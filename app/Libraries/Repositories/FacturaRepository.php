<?php 

namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\Factura;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FacturaRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
        return 'Hanzo\Models\Factura';
    }
}
