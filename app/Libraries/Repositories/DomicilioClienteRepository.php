<?php 

namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\DomicilioCliente;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DomicilioClienteRepository extends Repository
{
    /**
    * Configure the Model
    *
    **/
    public function model()
    {
        return 'Hanzo\Models\DomicilioCliente';
    }
}
