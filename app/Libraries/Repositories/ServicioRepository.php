<?php 

namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\Servicio;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ServicioRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
        return 'Hanzo\Models\Servicio';
    }
}
