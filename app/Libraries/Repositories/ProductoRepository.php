<?php 

namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\Producto;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductoRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
        return 'Hanzo\Models\Producto';
    }
}
