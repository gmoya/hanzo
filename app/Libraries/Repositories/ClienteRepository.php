<?php namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\Cliente;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ClienteRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'Hanzo\Models\Cliente';
    }

	public function search($input)
    {
        $query = Cliente::query();

        $columns = Schema::getColumnListing('clientes');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "Cliente not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "Cliente not found");
        }

        return $model->delete();
    }
}
