<?php 

namespace Hanzo\Libraries\Repositories;

use Hanzo\Models\Sucursal;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SucursalRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
        return 'Hanzo\Models\Sucursal';
    }
}
