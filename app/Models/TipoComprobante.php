<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class TipoComprobante extends Model
{
    public $table = "tipos_comprobante";

    protected $fillable = array('nombre', 'codigo_afip', 'codigo', 'letra', 'created_user_id');

    static public function retrieveByCodigoAndLetra($codigo, $letra)
    { 
        return TipoComprobante::where('codigo', $codigo)
                                ->where('letra', $letra)
                                ->first();
    }
}
