<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    public $table = "vendedores";
    
    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $vendedor = self::where('baciunas_id', $baciunas_id)->first();

        return $vendedor;
    }
}
