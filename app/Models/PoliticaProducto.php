<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class PoliticaProducto extends Model
{
    public $table = "politicas_producto";

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function politicas_precios()
    {
        return $this->hasMany(PoliticaPrecio::class, 'politicas_producto_id');
    }
}
