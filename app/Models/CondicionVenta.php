<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class CondicionVenta extends Model
{
    protected $table = 'condiciones_venta';
    public $timestamps = false;
    
    static public function retrieveByBaciunasId($baciunas_id)
    {
        $condicion = self::where('baciunas_id', $baciunas_id)->first();

        return $condicion;
    }
}