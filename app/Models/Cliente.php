<?php namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model as Model;
#use Hanzo\Models\Tipo;

class Cliente extends Model
{
    
	public $table = "clientes";
    

	public $fillable = [
	    "nro_cliente",
		"razon_social",
		"nombre_comercial",
		"cuit",
		"estado_id",
		"vendedor_id",
		"categoria_iva_id",
		"condicion_venta_id",
		"ingreso_brutos_id",
		"ingreso_brutos_bsas_id",
		"fecha_expiracion",
		"grupo_empresario_id",
		"web",
		"cuota_mensual",
        "tipo_cliente_id",
		"created_user_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nro_cliente" => "integer",
        "razon_social" => "string",
        "nombre_comercial" => "string",
        "cuit" => "string",
        "estado_id" => "integer",
        "vendedor_id" => "integer",
        "categoria_iva_id" => "integer",
        "condicion_venta_id" => "integer",
        "ingreso_brutos_id" => "integer",
        "ingreso_brutos_bsas_id" => "integer",
        "grupo_empresario_id" => "integer",
        "web" => "string",
        "cuota_mensual" => "double",
        "tipo_cliente_id" => "integer",
        "created_user_id" => "integer",
        "updated_user_id" => "integer",
        "deleted_user_id" => "integer"
    ];

	public static $rules = [
        "razon_social" => "required",
		"estado_id" => "required"
	];

    public function estado()
    {
        return $this->belongsTo(EstadosCliente::class);
    }

    public function categoria_iva()
    {
        return $this->belongsTo(CategoriaIva::class);
    }

    public function tipo_cliente()
    {
        return $this->belongsTo(TipoCliente::class);
    }
    
    public function ingresos_brutos()
    {
        return $this->belongsTo(Alicuota::class);
    }

    public function ingresos_brutos_bsas()
    {
        return $this->belongsTo(Alicuota::class);
    }

    public function vendedor()
    {
        return $this->belongsTo(Vendedor::class);
    }

    public function rubro_empresario()
    {
        return $this->belongsTo(RubroEmpresario::class);
    }

    public function politicas_productos()
    {
        return $this->hasMany(PoliticaProducto::class);
    }

    public function sucursales()
    {
        return $this->hasMany(Sucursal::class);
    }

    public function pendientes()
    {
        return $this->hasMany(Pendiente::class)->where('estado_id', 1);
    }

    public function historico()
    {
        return $this->hasMany(Historico::class);
    }

    public function domicilios_facturacion()
    {
        $tipo = TipoDomicilioCliente::where('nombre', 'ftc')->first();

        return $this->hasMany(DomicilioCliente::class)->where('tipo_domicilio_cliente_id', $tipo->id);
    }

    public function domicilios_cobro()
    {
        $tipo = TipoDomicilioCliente::where('nombre', 'cbz')->first();

        return $this->hasMany(DomicilioCliente::class)->where('tipo_domicilio_cliente_id', $tipo->id);
    }

    public function scopeCliente($query, $cliente)
    {
        if (trim($cliente) != '')
        {
            if ( is_numeric($cliente) )
            {
                if (strlen($cliente) < 10) 
                {
                    $query->where('id', $cliente);
                    $query->orWhere('cuit', $cliente);
                } else {
                    $query->where('cuit', $cliente);
                }
            } else {
                $query->where('razon_social', 'ilike', '%' . $cliente . '%');
            }
        }
    }

    public function scopeByEstado($query, $estado)
    {
        if (trim($estado) && is_numeric($estado))
            $query->where('estado_id', $estado);
    }

    public function getTotalPendientes()
    {
        $pendientes = $this->pendientes;
        $total = 0;

        if (!$pendientes)
            return $total;

        foreach ($pendientes as $pendiente) 
        {
            if ($pendiente->tipo_comprobante->codigo == 'NCRE')
                $total-= $pendiente->importe;
            else
                $total+= $pendiente->importe;
        }

        return $total;
    }
}
