<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCliente extends Model
{
    public $table = "tipos_cliente";

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $tipo = TipoCliente::where('baciunas_id', $baciunas_id)->first();

        return $tipo;
    }
}