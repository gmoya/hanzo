<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    public $table = "sucursales";

    public static $rules = [
        "nro_ceamse"   => "required|integer",
        "calle"        => "required",
        "nro"          => "required",
        "localidad_id" => "required|integer",
        "estado_id"    => "required|integer",
        "chofer_id"    => "integer",
        "email"        => "email",
    ];

    public $fillable = [
        "nombre",
        "nro_ceamse",
        "calle",
        "nro",
        "cliente_id",
        "localidad_id",
        "provincia_id",
        "codigo_postal",
        "entre_calle_a",
        "entre_calle_b",
        "hoja_filcar",
        "email",
        "estado_id",
        "telefono",
        "contacto",
        "horario_atencion",
        "chofer_id",
        "recorrido_id",
        "domicilio_factura_id",
        "domicilio_cobro_id",
        "created_user_id"
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function estado()
    {
        return $this->belongsTo(EstadosCliente::class);
    }

    public function abono()
    {
        return $this->hasOne(AbonoSucursal::class);
    }

    public function servicios()
    {
        return $this->hasMany(Servicio::class);
    }
    
    public function facturas()
    {
        return $this->hasMany(Factura::class);
    }

    public function politicas_productos()
    {
        return $this->hasMany(PoliticaProducto::class);
    }

    public function localidad()
    {
        return $this->belongsTo(Localidad::class);
    }

    public function provincia()
    {
        return $this->belongsTo(Provincia::class);
    }
    
    public function condicion_venta()
    {
        return $this->belongsTo(CondicionVenta::class);
    }

    static public function retrieveByNroCliente($baciunas_id)
    {
        $sucursal = Sucursal::where('sucursales.nro_cliente', $baciunas_id)->first();

        return $sucursal;
    }

    public function domicilio_facturacion()
    {
        return $this->belongsTo(DomicilioCliente::class, 'domicilio_factura_id', 'id');
    }

    public function getServiciosPendientes()
    {
        return $this->servicios()
                     ->where('servicios.estado_id', 1)
                     ->orderBy('fecha', 'desc')
                     ->get();
    }

    public function scopeSucursal($query, $sucursal)
    {
        if (trim($sucursal) != '')
        {
            if ( is_numeric($sucursal) )
            {
                $query->where('sucursales.id', $sucursal);
            } else {
                $query->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                      ->where('sucursales.nombre', 'ilike', '%' . $sucursal . '%')
                      ->where('clientes.razon_social', 'ilike', '%' . $sucursal . '%');
            }
        }
    }

    public function scopeByEstado($query, $estado)
    {
        if (trim($estado) && is_numeric($estado))
            $query->where('sucursales.estado_id', $estado);
    }
}