<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class DomicilioPersona extends Model
{
    public $table = "domicilios_persona";
}
