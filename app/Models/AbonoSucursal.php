<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;
use Hanzo\Models\AbonoFacturado;

class AbonoSucursal extends Model
{
    protected $table = 'abonos_sucursal';

    public function isFacturado()
    {
        return false;
        
        if ($abono_facturado = AbonoFacturado::getAbonoFacturadoUltimoMesBySucursal($this->sucursal_id))
            return $abono_facturado;
    }

    static public function getAbonosPendientes($desde, $hasta, $ciclo, $servicios_excluidos = null, $clientes_excluidos = null)
    {
        $abonos_facturados = AbonoFacturado::getSucursalesAbonosFacturadosUltimoMes();

        $sql = AbonoSucursal::select('abonos_sucursal.id', 'abonos_sucursal.sucursal_id')
                            ->join('sucursales', 'abonos_sucursal.sucursal_id', '=', 'sucursales.id')
                            ->where('abonos_sucursal.activo', true)
                            ->where('abonos_sucursal.monto', '<>', 0)
                            ->where('sucursales.estado_id', 1)
                            #->whereIn('sucursales.nro_cliente', array(2850, 2910, 3332, 4445, 4775, 4776, 4781, 4784, 4785, 4855, 4960, 5055, 5056, 5057, 5059, 5064, 5095, 5122, 5140, 5160, 5212, 5228, 5246, 5306, 5316))

                            ->where('sucursales.ciclo_facturacion_id', $ciclo);
                            #->whereNotIn('abonos_sucursal.sucursal_id', $abonos_facturados);

        if ($servicios_excluidos)
            $sql->whereNotIn('abonos_sucursal.sucursal_id', $servicios_excluidos);

        if ($clientes_excluidos)
            $sql->whereNotIn('sucursales.nro_cliente', $clientes_excluidos); # QUITAR Clientes excluídos

        return $sql->get();
    }
}