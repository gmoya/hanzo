<?php

namespace Hanzo\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TipoDomicilioCliente extends Model
{
    public $table = "tipos_domicilio_cliente";

    static public function getDomicilioByNombre($nombre)
    {
        #$tipo = TipoDomicilioCliente::where(DB::raw("UPPER(UNACCENT(descripcion))"), DB::raw("UPPER(UNACCENT('$nombre'))"))->first();
        $tipo = TipoDomicilioCliente::where('nombre', $nombre)->first();

        return $tipo;
    }

}
