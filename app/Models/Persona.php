<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    public $table = "personas";
}
