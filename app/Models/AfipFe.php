<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;
use Hanzo\Models\AfipWs;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
/*
  0 => "FECAESolicitarResponse FECAESolicitar(FECAESolicitar $parameters)"
  1 => "FECompTotXRequestResponse FECompTotXRequest(FECompTotXRequest $parameters)"
  2 => "FEDummyResponse FEDummy(FEDummy $parameters)"
  3 => "FECompUltimoAutorizadoResponse FECompUltimoAutorizado(FECompUltimoAutorizado $parameters)"
  4 => "FECompConsultarResponse FECompConsultar(FECompConsultar $parameters)"
  5 => "FECAEARegInformativoResponse FECAEARegInformativo(FECAEARegInformativo $parameters)"
  6 => "FECAEASolicitarResponse FECAEASolicitar(FECAEASolicitar $parameters)"
  7 => "FECAEASinMovimientoConsultarResponse FECAEASinMovimientoConsultar(FECAEASinMovimientoConsultar $parameters)"
  8 => "FECAEASinMovimientoInformarResponse FECAEASinMovimientoInformar(FECAEASinMovimientoInformar $parameters)"
  9 => "FECAEAConsultarResponse FECAEAConsultar(FECAEAConsultar $parameters)"
  10 => "FEParamGetCotizacionResponse FEParamGetCotizacion(FEParamGetCotizacion $parameters)"
  11 => "FEParamGetTiposTributosResponse FEParamGetTiposTributos(FEParamGetTiposTributos $parameters)"
  12 => "FEParamGetTiposMonedasResponse FEParamGetTiposMonedas(FEParamGetTiposMonedas $parameters)"
  13 => "FEParamGetTiposIvaResponse FEParamGetTiposIva(FEParamGetTiposIva $parameters)"
  14 => "FEParamGetTiposOpcionalResponse FEParamGetTiposOpcional(FEParamGetTiposOpcional $parameters)"
  15 => "FEParamGetTiposConceptoResponse FEParamGetTiposConcepto(FEParamGetTiposConcepto $parameters)"
  16 => "FEParamGetPtosVentaResponse FEParamGetPtosVenta(FEParamGetPtosVenta $parameters)"
  17 => "FEParamGetTiposCbteResponse FEParamGetTiposCbte(FEParamGetTiposCbte $parameters)"
  18 => "FEParamGetTiposDocResponse FEParamGetTiposDoc(FEParamGetTiposDoc $parameters)"
  19 => "FEParamGetTiposPaisesResponse FEParamGetTiposPaises(FEParamGetTiposPaises $parameters)"
  20 => "FECAESolicitarResponse FECAESolicitar(FECAESolicitar $parameters)"
  21 => "FECompTotXRequestResponse FECompTotXRequest(FECompTotXRequest $parameters)"
  22 => "FEDummyResponse FEDummy(FEDummy $parameters)"
  23 => "FECompUltimoAutorizadoResponse FECompUltimoAutorizado(FECompUltimoAutorizado $parameters)"
  24 => "FECompConsultarResponse FECompConsultar(FECompConsultar $parameters)"
  25 => "FECAEARegInformativoResponse FECAEARegInformativo(FECAEARegInformativo $parameters)"
  26 => "FECAEASolicitarResponse FECAEASolicitar(FECAEASolicitar $parameters)"
  27 => "FECAEASinMovimientoConsultarResponse FECAEASinMovimientoConsultar(FECAEASinMovimientoConsultar $parameters)"
  28 => "FECAEASinMovimientoInformarResponse FECAEASinMovimientoInformar(FECAEASinMovimientoInformar $parameters)"
  29 => "FECAEAConsultarResponse FECAEAConsultar(FECAEAConsultar $parameters)"
  30 => "FEParamGetCotizacionResponse FEParamGetCotizacion(FEParamGetCotizacion $parameters)"
  31 => "FEParamGetTiposTributosResponse FEParamGetTiposTributos(FEParamGetTiposTributos $parameters)"
  32 => "FEParamGetTiposMonedasResponse FEParamGetTiposMonedas(FEParamGetTiposMonedas $parameters)"
  33 => "FEParamGetTiposIvaResponse FEParamGetTiposIva(FEParamGetTiposIva $parameters)"
  34 => "FEParamGetTiposOpcionalResponse FEParamGetTiposOpcional(FEParamGetTiposOpcional $parameters)"
  35 => "FEParamGetTiposConceptoResponse FEParamGetTiposConcepto(FEParamGetTiposConcepto $parameters)"
  36 => "FEParamGetPtosVentaResponse FEParamGetPtosVenta(FEParamGetPtosVenta $parameters)"
  37 => "FEParamGetTiposCbteResponse FEParamGetTiposCbte(FEParamGetTiposCbte $parameters)"
  38 => "FEParamGetTiposDocResponse FEParamGetTiposDoc(FEParamGetTiposDoc $parameters)"
  39 => "FEParamGetTiposPaisesResponse FEParamGetTiposPaises(FEParamGetTiposPaises $parameters)"
*/
class AfipFe extends AfipWs
{
    protected $fe_tipos_comprobante = array();
    protected $fe_tipos_documento   = array();
    protected $fe_alicuotas_iva     = array();
    protected $fe_condiciones_iva   = array();
    protected $fe_monedas           = array();
    protected $fe_cotizacion_moneda;
    protected $fe_unidades_medida   = array();
    protected $fe_puntos_venta      = array();
    protected $fe_puntos_venta_cae  = array();
    protected $fe_tipos_tributo     = array();

    protected $cuit = '30689674751';

    protected $fe_url;
    protected $fe_wsdl;

    function __construct()
    {
        parent::__construct();

        $this->login('wsfe');

        $this->fe_url  = env('FE_URL');
        $this->fe_wsdl = env('FE_WSDL');

        $this->consultarTiposComprobante();
        $this->consultarTiposDocumento();
        $this->consultarAlicuotasIVA();
        $this->consultarCondicionesIVA();
        $this->consultarMonedas();
        $this->consultarCotizacionMoneda();
        $this->consultarUnidadesMedida();
        $this->consultarPuntosVenta();
        $this->consultarTiposTributo();
    }
/*
  0 => "FECAESolicitar(FECAESolicitar $parameters)"
  1 => "FECompTotXRequest(FECompTotXRequest $parameters)"
  2 => "FEDummy(FEDummy $parameters)"
  3 => "FECompUltimoAutorizado(FECompUltimoAutorizado $parameters)"
  4 => "FECompConsultar(FECompConsultar $parameters)"
*/    
    public function invocarClienteSoap($action, $parameters)
    {
        $opts = array(
            'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
        );

        $this->client = new \SoapClient($this->fe_wsdl, array('soap_version'   => SOAP_1_2,
                                                              'location'       => $this->fe_url,
                                                              'trace'          => 1,
                                                              'exceptions'     => 0,
                                                              'verifypeer'     => false, 
                                                              'verifyhost'     => false,
                                                              'stream_context' => stream_context_create($opts)
        ));

        return $this->client->$action($parameters);
    }

    /*  FEParamGetTiposCbte     */
    public function consultarTiposComprobante()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposCbte', 
                                                 array('Auth' => array(
                                                                                'Token' => $this->token,
                                                                                'Sign'  => $this->sign,
                                                                                'Cuit'  => $this->cuit
                                               )));

            $this->fe_tipos_comprobante = collect($results->arrayTiposComprobante->codigoDescripcion);
        */

        $this->fe_tipos_comprobante = collect([   ["codigo" => 1,  "descripcion" => "Factura A",         "FchDesde" => "20100917"],
                                                  ["codigo" => 2,  "descripcion" => "Nota de Débito A",  "FchDesde" => "20100917"],
                                                  ["codigo" => 3,  "descripcion" => "Nota de Crédito A", "FchDesde" => "20100917"],
                                                  ["codigo" => 6,  "descripcion" => "Factura B",         "FchDesde" => "20100917"],
                                                  ["codigo" => 7,  "descripcion" => "Nota de Débito B",  "FchDesde" => "20100917"],
                                                  ["codigo" => 8,  "descripcion" => "Nota de Crédito B", "FchDesde" => "20100917"],
                                                  ["codigo" => 51, "descripcion" => "Factura M",         "FchDesde" => "20150522"],
                                                  ["codigo" => 52, "descripcion" => "Nota de Débito M",  "FchDesde" => "20150522"],
                                                  ["codigo" => 53, "descripcion" => "Nota de Crédito M", "FchDesde" => "20150522"]
                                        ]);
    }

    /*  FEParamGetTiposDoc  */
    public function consultarTiposDocumento()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposDoc', 
                                                 array('Auth' => array(
                                                                                'Token' => $this->token,
                                                                                'Sign'  => $this->sign,
                                                                                'Cuit'  => $this->cuit
                                               )));

            $this->fe_tipos_documento = collect($results->arrayTiposDocumento->codigoDescripcion);
        */
        $this->fe_tipos_documento = collect([["codigo" => 0,  "descripcion" => "CI Policía Federal"],
                                                ["codigo" => 1,  "descripcion" => "CI Buenos Aires"],
                                                ["codigo" => 2,  "descripcion" => "CI Catamarca"],
                                                ["codigo" => 3,  "descripcion" => "CI Córdoba"],
                                                ["codigo" => 4,  "descripcion" => "CI Corrientes"],
                                                ["codigo" => 5,  "descripcion" => "CI Entre Ríos"],
                                                ["codigo" => 6,  "descripcion" => "CI Jujuy"],
                                                ["codigo" => 7,  "descripcion" => "CI Mendoza"],
                                                ["codigo" => 8,  "descripcion" => "CI La Rioja"],
                                                ["codigo" => 9,  "descripcion" => "CI Salta"],
                                                ["codigo" => 10, "descripcion" => "CI San Juan"],
                                                ["codigo" => 11, "descripcion" => "CI San Luis"],
                                                ["codigo" => 12, "descripcion" => "CI Santa Fe"],
                                                ["codigo" => 13, "descripcion" => "CI Santiago del Estero"],
                                                ["codigo" => 14, "descripcion" => "CI Tucumán"],
                                                ["codigo" => 16, "descripcion" => "CI Chaco"],
                                                ["codigo" => 17, "descripcion" => "CI Chubut"],
                                                ["codigo" => 18, "descripcion" => "CI Formosa"],
                                                ["codigo" => 19, "descripcion" => "CI Misiones"],
                                                ["codigo" => 20, "descripcion" => "CI Neuquén"],
                                                ["codigo" => 21, "descripcion" => "CI La Pampa"],
                                                ["codigo" => 22, "descripcion" => "CI Río Negro"],
                                                ["codigo" => 23, "descripcion" => "CI Santa Cruz"],
                                                ["codigo" => 24, "descripcion" => "CI Tierra del Fuego"],
                                                ["codigo" => 30, "descripcion" => "Certificado de Migración"],
                                                ["codigo" => 80, "descripcion" => "CUIT"],
                                                ["codigo" => 86, "descripcion" => "CUIL"],
                                                ["codigo" => 87, "descripcion" => "CDI"],
                                                ["codigo" => 89, "descripcion" => "LE"],
                                                ["codigo" => 90, "descripcion" => "LC"],
                                                ["codigo" => 91, "descripcion" => "CI Extranjera"],
                                                ["codigo" => 92, "descripcion" => "en trámite"],
                                                ["codigo" => 93, "descripcion" => "Acta Nacimiento"],
                                                ["codigo" => 95, "descripcion" => "CI Bs. As. RNP"],
                                                ["codigo" => 96, "descripcion" => "DNI"],
                                                ["codigo" => 94, "descripcion" => "Pasaporte"]
                                        ]);
    }

    /*  FEParamGetTiposIva  */
    public function consultarAlicuotasIVA()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposIva', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                               )));

            $this->fe_alicuotas_iva = collect($results->arrayAlicuotasIVA->codigoDescripcion);
        */
        $this->fe_alicuotas_iva = collect([   ["codigo" => 3, "descripcion" => "0%"],
                                              ["codigo" => 4, "descripcion" => "10.5%"],
                                              ["codigo" => 5, "descripcion" => "21%"],
                                              ["codigo" => 6, "descripcion" => "27%"]
                                        ]);
    }

    public function consultarCondicionesIVA()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposConcepto', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                               )));
            $this->fe_condiciones_iva = collect($results->arrayCondicionesIVA->codigoDescripcion);
        */

        $this->fe_condiciones_iva = collect([["codigo" => 1, "descripcion" => "No gravado"],
                                                ["codigo" => 2, "descripcion" => "Exento"],
                                                ["codigo" => 3, "descripcion" => "0%"],
                                                ["codigo" => 4, "descripcion" => "10.5%"],
                                                ["codigo" => 5, "descripcion" => "21%"],
                                                ["codigo" => 6, "descripcion" => "27%"]
                                        ]);
    }

    /*  FEParamGetTiposMonedas  */
    public function consultarMonedas()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposMonedas', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                               )));

            $this->fe_monedas = collect($results->arrayMonedas->codigoDescripcion);
        */

        $this->fe_monedas = collect([["codigo" => "PES", "descripcion" => "Pesos Argentinos"],
                                        ["codigo" => "DOL", "descripcion" => "Dólar Estadounidense"],
                                        ["codigo" => "002", "descripcion" => "Dólar Libre EEUU"],
                                        ["codigo" => "007", "descripcion" => "Florines Holandeses"],
                                        ["codigo" => "009", "descripcion" => "Franco Suizo"],
                                        ["codigo" => "010", "descripcion" => "Pesos Mejicanos"],
                                        ["codigo" => "011", "descripcion" => "Pesos Uruguayos"],
                                        ["codigo" => "012", "descripcion" => "Real"],
                                        ["codigo" => "014", "descripcion" => "Coronas Danesas"],
                                        ["codigo" => "015", "descripcion" => "Coronas Noruegas"],
                                        ["codigo" => "016", "descripcion" => "Coronas Suecas"],
                                        ["codigo" => "018", "descripcion" => "Dólar Canadiense"],
                                        ["codigo" => "019", "descripcion" => "Yens"],
                                        ["codigo" => "021", "descripcion" => "Libra Esterlina"],
                                        ["codigo" => "023", "descripcion" => "Bolívar Venezolano"],
                                        ["codigo" => "024", "descripcion" => "Corona Checa"],
                                        ["codigo" => "025", "descripcion" => "Dinar Yugoslavo"],
                                        ["codigo" => "026", "descripcion" => "Dólar Australiano"],
                                        ["codigo" => "027", "descripcion" => "Dracma Griego"],
                                        ["codigo" => "028", "descripcion" => "Florín (Antillas Holandesas)"],
                                        ["codigo" => "029", "descripcion" => "Güaraní"],
                                        ["codigo" => "030", "descripcion" => "Shekel (Israel)"],
                                        ["codigo" => "031", "descripcion" => "Peso Boliviano"],
                                        ["codigo" => "032", "descripcion" => "Peso Colombiano"],
                                        ["codigo" => "033", "descripcion" => "Peso Chileno"],
                                        ["codigo" => "034", "descripcion" => "Rand Sudafricano"],
                                        ["codigo" => "035", "descripcion" => "Nuevo Sol Peruano"],
                                        ["codigo" => "036", "descripcion" => "Sucre Ecuatoriano"],
                                        ["codigo" => "040", "descripcion" => "Lei Rumano"],
                                        ["codigo" => "041", "descripcion" => "Derechos Especiales de Giro"],
                                        ["codigo" => "042", "descripcion" => "Peso Dominicano"],
                                        ["codigo" => "043", "descripcion" => "Balboas Panameñas"],
                                        ["codigo" => "044", "descripcion" => "Córdoba Nicaragüense"],
                                        ["codigo" => "045", "descripcion" => "Dirham Marroquí"],
                                        ["codigo" => "046", "descripcion" => "Libra Egipcia"],
                                        ["codigo" => "047", "descripcion" => "Riyal Saudita"],
                                        ["codigo" => "049", "descripcion" => "Gramos de Oro Fino"],
                                        ["codigo" => "051", "descripcion" => "Dólar de Hong Kong"],
                                        ["codigo" => "052", "descripcion" => "Dólar de Singapur"],
                                        ["codigo" => "053", "descripcion" => "Dólar de Jamaica"],
                                        ["codigo" => "054", "descripcion" => "Dólar de Taiwan"],
                                        ["codigo" => "055", "descripcion" => "Quetzal Guatemalteco"],
                                        ["codigo" => "056", "descripcion" => "Forint (Hungría)"],
                                        ["codigo" => "057", "descripcion" => "Baht (Tailandia)"],
                                        ["codigo" => "059", "descripcion" => "Dinar Kuwaiti"],
                                        ["codigo" => "060", "descripcion" => "Euro"],
                                        ["codigo" => "063", "descripcion" => "Lempira Hondureña"],
                                        ["codigo" => "062", "descripcion" => "Rupia Hindú"],
                                        ["codigo" => "061", "descripcion" => "Zloty Polaco"],
                                        ["codigo" => "064", "descripcion" => "Yuan (Rep. Pop. China)"]
                                    ]);
    }

    /*  FEParamGetCotizacion    */
    public function consultarCotizacionMoneda()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetCotizacion', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                                                        ),
                                                        'MonId' => $this->fe_monedas->where('descripcion', 'Pesos Argentinos')->first()['codigo']
                                                 ));
        
            $this->fe_cotizacion_moneda = empty((array)$results) ? 1 : collect($results->cotizacionMoneda);
        */

        $this->fe_cotizacion_moneda = 1;
    }

    public function consultarUnidadesMedida()
    {
        /*
            return $results = $this->invocarClienteSoap('consultarUnidadesMedida', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        )));

            $this->fe_unidades_medida = collect($results->arrayUnidadesMedida->codigoDescripcion);
        */
        $this->fe_unidades_medida = collect([["codigo" => 0,  "descripcion" => " "],
                                                ["codigo" => 1,  "descripcion" => "kilogramos"],
                                                ["codigo" => 2,  "descripcion" => "metros"],
                                                ["codigo" => 3,  "descripcion" => "metros cuadrados"],
                                                ["codigo" => 4,  "descripcion" => "metros cúbicos"],
                                                ["codigo" => 5,  "descripcion" => "litros"],
                                                ["codigo" => 6,  "descripcion" => "1000 kWh"],
                                                ["codigo" => 7,  "descripcion" => "unidades"],
                                                ["codigo" => 8,  "descripcion" => "pares"],
                                                ["codigo" => 9,  "descripcion" => "docenas"],
                                                ["codigo" => 10, "descripcion" => "quilates"],
                                                ["codigo" => 11, "descripcion" => "millares"],
                                                ["codigo" => 14, "descripcion" => "gramos"],
                                                ["codigo" => 15, "descripcion" => "milimetros"],
                                                ["codigo" => 16, "descripcion" => "mm cúbicos"],
                                                ["codigo" => 17, "descripcion" => "kilómetros"],
                                                ["codigo" => 18, "descripcion" => "hectolitros"],
                                                ["codigo" => 20, "descripcion" => "centímetros"],
                                                ["codigo" => 25, "descripcion" => "jgo. pqt. mazo naipes"],
                                                ["codigo" => 27, "descripcion" => "cm cúbicos"],
                                                ["codigo" => 29, "descripcion" => "toneladas"],
                                                ["codigo" => 30, "descripcion" => "dam cúbicos"],
                                                ["codigo" => 31, "descripcion" => "hm cúbicos"],
                                                ["codigo" => 32, "descripcion" => "km cúbicos"],
                                                ["codigo" => 33, "descripcion" => "microgramos"],
                                                ["codigo" => 34, "descripcion" => "nanogramos"],
                                                ["codigo" => 35, "descripcion" => "picogramos"],
                                                ["codigo" => 41, "descripcion" => "miligramos"],
                                                ["codigo" => 47, "descripcion" => "mililitros"],
                                                ["codigo" => 48, "descripcion" => "curie"],
                                                ["codigo" => 49, "descripcion" => "milicurie"],
                                                ["codigo" => 50, "descripcion" => "microcurie"],
                                                ["codigo" => 51, "descripcion" => "uiacthor"],
                                                ["codigo" => 52, "descripcion" => "muiacthor"],
                                                ["codigo" => 53, "descripcion" => "kg base"],
                                                ["codigo" => 54, "descripcion" => "gruesa"],
                                                ["codigo" => 61, "descripcion" => "kg bruto"],
                                                ["codigo" => 62, "descripcion" => "uiactant"],
                                                ["codigo" => 63, "descripcion" => "muiactant"],
                                                ["codigo" => 64, "descripcion" => "uiactig"],
                                                ["codigo" => 65, "descripcion" => "muiactig"],
                                                ["codigo" => 66, "descripcion" => "kg activo"],
                                                ["codigo" => 67, "descripcion" => "gramo activo"],
                                                ["codigo" => 68, "descripcion" => "gramo base"],
                                                ["codigo" => 95, "descripcion" => "anulación/devolución"],
                                                ["codigo" => 96, "descripcion" => "packs"],
                                                ["codigo" => 97, "descripcion" => "seña/anticipo"],
                                                ["codigo" => 98, "descripcion" => "otras unidades"],
                                                ["codigo" => 99, "descripcion" => "bonificación"]
                                        ]);
    }

    /*  FEParamGetPtosVenta     */
    public function consultarPuntosVenta()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetPtosVenta', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                                                        )));
            $this->fe_puntos_venta = collect($results->arrayPuntosVenta); # HARDCODE
        */

        $this->fe_puntos_venta = collect([  [ "Nro" => 4, "EmisionTipo" => "CAE", "Bloqueado" => "N", "FchBaja" => "NULL" ],
                                            [ "Nro" => 5, "EmisionTipo" => "CAE", "Bloqueado" => "N", "FchBaja" => "NULL" ],
                                            [ "Nro" => 6, "EmisionTipo" => "CAE", "Bloqueado" => "N", "FchBaja" => "NULL" ]
                                ]);
    }

    /*  FEParamGetTiposTributos     */
    public function consultarTiposTributo()
    {
        /*
            $results = $this->invocarClienteSoap('FEParamGetTiposTributos', 
                                                 array('Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                                                    )));

            $this->fe_tipos_tributo = collect($results->arrayTiposTributo->codigoDescripcion);
        */
        $this->fe_tipos_tributo = collect([["codigo" => 1,  "descripcion" => "Impuestos Nacionales"],
                                              ["codigo" => 2,  "descripcion" => "Impuestos Provinciales"],
                                              ["codigo" => 3,  "descripcion" => "Impuestos Municipales"],
                                              ["codigo" => 4,  "descripcion" => "Impuestos Internos"],
                                              ["codigo" => 99, "descripcion" => "Otros"]
                                    ]);
    }

    /*  FECompUltimoAutorizado  */
    public function consultarUltimoComprobanteAutorizado($codigo_comprobante, $punto_venta)
    {
        if ($codigo_comprobante && $punto_venta)
        {
            $results = $this->invocarClienteSoap('FECompUltimoAutorizado', 
                                                 array( 
                                                       'Auth' => array(
                                                                        'Token' => $this->token,
                                                                        'Sign'  => $this->sign,
                                                                        'Cuit'  => $this->cuit
                                                                    ),
                                                        'CbteTipo' => $codigo_comprobante,
                                                        'PtoVta'   => $punto_venta
                                                ));
            return $results;
        }
    }

    public function autorizarComprobanteCAE(Factura $factura, $items = array())
    {
        $comprobante = $this->generarComprobanteFactura($factura, $items);

        $results = $this->invocarClienteSoap('FECAESolicitar', $comprobante);

        $view_log = new Logger('Afip Fe Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/afip_fe.log', Logger::INFO) );
        $view_log->addInfo($this->client->__getLastRequest());
        $view_log->addInfo($this->client->__getLastResponse());
        
        if ( !is_soap_fault($results) )
            $view_log->addInfo(serialize($results));

        if (is_soap_fault($results) || (($results->FECAESolicitarResult->FeCabResp != null) && $results->FECAESolicitarResult->FeCabResp->Resultado != 'A'))
        {
            $view_log = new Logger('Afip Fe Errores Log');
            $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/afip_fe_errors.log', Logger::INFO) );
            $view_log->addInfo($this->client->__getLastRequest());
            $view_log->addInfo($this->client->__getLastResponse());
            
            if ( !is_soap_fault($results) )
                $view_log->addInfo(serialize($results));
        }

        return $results;
    }

    public function generarComprobanteFactura(Factura $factura, $items = array())
    {
        $codigo_concepto = 2; # HARDCODE Código de Servicios
        $format = (strlen($factura->fecha) == 10) ? 'd/m/Y' : 'Y-m-d H:i:s';

        $date = new \DateTime();
        $vencimiento = $date->createFromFormat($format, $factura->fecha)
                            ->add(new \DateInterval('P'.$factura->sucursal->condicion_venta->dias_1.'D'));
        $emision = $date->createFromFormat($format, $factura->fecha);
        $desde   = $date->createFromFormat($format, $factura->periodo_facturacion_desde);
        $hasta   = $date->createFromFormat($format, $factura->periodo_facturacion_hasta);

        $comprobante = array('Auth' => array(), 'FeCAEReq' => array());

        $comprobante['Auth'] = array(
                                        'Token' => $this->token,
                                        'Sign'  => $this->sign,
                                        'Cuit'  => $this->cuit
                               );

        $comprobante['FeCAEReq']['FeCabReq'] = array('CantReg'  => 1,
                                                     'PtoVta'   => $factura->punto_venta->nro,
                                                     'CbteTipo' => $this->fe_tipos_comprobante->where('codigo', $factura->tipo_comprobante->codigo_afip)->first()['codigo'],

                                                );

        $comprobante['FeCAEReq']['FeDetReq'] = array();

        $comprobante['FeCAEReq']['FeDetReq']['FECAEDetRequest'] = array('Concepto'     => $codigo_concepto,
                                                                        'DocTipo'      => $this->fe_tipos_documento->where('descripcion', 'CUIT')->first()['codigo'],
                                                                        'DocNro'       => $factura->cuit,
                                                                        'CbteDesde'    => $factura->numero_comprobante,
                                                                        'CbteHasta'    => $factura->numero_comprobante,
                                                                        'CbteFch'      => $emision->format('Ymd'),
                                                                        'ImpTotal'     => number_format($factura->total, 2 , '.', ''),
                                                                        'ImpTotConc'   => '0.00',
                                                                        'ImpNeto'      => number_format($factura->neto_gravado, 2 , '.', ''),
                                                                        'ImpOpEx'      => '0.00',
                                                                        'ImpTrib'      => '0.00',
                                                                        'ImpIVA'       => number_format($factura->impuestos, 2, '.', ''),
                                                                        'FchServDesde' => $desde->format('Ymd'),
                                                                        'FchServHasta' => $hasta->format('Ymd'),
                                                                        'FchVtoPago'   => $vencimiento->format('Ymd'),
                                                                        'MonId'        => $this->fe_monedas->where('descripcion', 'Pesos Argentinos')->first()['codigo'],
                                                                        'MonCotiz'     => $this->fe_cotizacion_moneda
                                                                    );

        #$comprobante['comprobanteCAERequest']['arrayItems'] = array();
        
        $items = $items ? $items : $factura->items;
        $iva = array();

        foreach ($items as $key => $item) 
        {
            $cantidad        = ( ($item->contenedores > 0) ? $item->contenedores : ( ($item->kilos > 0) ? $item->kilos : 1 ) );
            $tipo_unidad     = $this->fe_unidades_medida->where('descripcion', ( !is_null($item->kilos) ? 'toneladas' : 'otras unidades' ))->first()['codigo'];
            $alicuota        = $item->producto->alicuota;
            $descripcion_iva = ( $alicuota ? (($alicuota->abreviatura == 'IVA10.5') ? '10.5%' : ( ($alicuota->abreviatura == 'IVA21') ? '21%' : '0%')) : '0%' );
            $condicion_iva   = $this->fe_condiciones_iva->where('descripcion', $descripcion_iva)->first()['codigo'];
            $importe_iva     = $alicuota ? $item->neto * "0.$alicuota->porcentaje" : 0;
            $importe_item    = $alicuota ? $item->neto * "1.$alicuota->porcentaje" : $item->neto;

            $item->precio_unitario = ($factura->letra == 'B' && $alicuota) ? $item->precio_unitario * "1.$alicuota->porcentaje" : $item->precio_unitario;

            $valores = 
                array('codigo'              => $item->producto->codigo,
                      'unidadesMtx'         => $tipo_unidad,
                      'codigoMtx'           => $item->producto->codigo,
                      'descripcion'         => $item->producto->nombre,
                      'cantidad'            => $cantidad,
                      'codigoUnidadMedida'  => $tipo_unidad,
                      'precioUnitario'      => number_format($item->precio_unitario, 6, '.', ''),
                      'codigoCondicionIVA'  => $condicion_iva,
                      'importeItem'         => number_format($importe_item, 2, '.', '')
                );

            if ($factura->letra == 'A')
                $valores['importeIVA'] = number_format($importe_iva, 2, '.', '');

            if ($alicuota->porcentaje > 0)
            {
                if ( isset($iva[$alicuota->porcentaje]) )
                {
                    $iva[$alicuota->porcentaje]['imponible'] += $item->neto;
                    $iva[$alicuota->porcentaje]['importe']   += number_format($importe_iva, 2, '.', '');
                } else {
                    $iva[$alicuota->porcentaje] = array();
                    $iva[$alicuota->porcentaje]['id']        = $condicion_iva;
                    $iva[$alicuota->porcentaje]['imponible'] = $item->neto;
                    $iva[$alicuota->porcentaje]['importe']   = $importe_iva;
                }

            }
            #$comprobante['comprobanteCAERequest']['arrayItems'][] = $valores;
        }
        
        $comprobante['FeCAEReq']['FeDetReq']['FECAEDetRequest']['Iva'] = array();

        foreach ($iva as $porcentaje => $impuesto) 
        {
            $comprobante['FeCAEReq']['FeDetReq']['FECAEDetRequest']['Iva'][] = array('Id'      => $impuesto['id'],
                                                                                     'BaseImp' => number_format($impuesto['imponible'], 2, '.', ''),
                                                                                     'Importe' => number_format($impuesto['importe'], 2, '.', ''));
        }

        return $comprobante;
    }
}