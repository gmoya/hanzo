<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    static public function retrieveByPk($baciunas_id)
    {
        $provincia = self::where('id', $baciunas_id)->first();

        return $provincia;
    }
}