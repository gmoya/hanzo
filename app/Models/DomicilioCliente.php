<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class DomicilioCliente extends Model
{
    public $table = "domicilios_cliente";

    public static $rules = [
        "calle"        => "required",
        "nro"          => "required",
        "localidad_id" => "required|integer",
        "email"        => "email"
    ];

    public $fillable = [
        "calle",
        "nro",
        "cliente_id",
        "localidad_id",
        "provincia_id",
        "codigo_postal",
        "entre_calle_a",
        "entre_calle_b",
        "hoja_filcar",
        "email",
        "telefono",
        "contacto",
        "horario_atencion",
        "tipo_domicilio_cliente_id",
        "created_user_id"
    ];

    public function tipo_domicilio()
    {
        return $this->belongsTo(TipoDomicilioCliente::class, 'tipo_domicilio_cliente_id');
    }

    public function localidad()
    {
        return $this->belongsTo(Localidad::class);
    }

    public function provincia()
    {
        return $this->belongsTo(Provincia::class);
    }
}
