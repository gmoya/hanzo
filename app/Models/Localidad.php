<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'localidades';
    //
    static public function retrieveByBaciunasId($baciunas_id)
    {
        $localidad = Localidad::where('localidades.baciunas_id', $baciunas_id)->first();

        return $localidad;
    }

}
