<?php

namespace Hanzo\Models;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Database\Eloquent\Model;
use Hanzo\Models\ItemFactura;
use Hanzo\Models\AfipMtxca;
use Hanzo\Models\FacturaFallida;
use Hanzo\Models\TipoComprobante;

class Factura extends Model
{
    protected $table = 'facturas';
    protected $cuit_hugo = '30689674751';

    public function tipo_comprobante()
    {
        return $this->belongsTo(TipoComprobante::class);
    }

    public function punto_venta()
    {
        return $this->belongsTo(PuntoVenta::class);
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }
 
    public function condicion_venta()
    {
        return $this->belongsTo(CondicionVenta::class);
    }

    public function localidad()
    {
        return $this->belongsTo(Localidad::class);
    }

    public function provincia()
    {
        return $this->belongsTo(Provincia::class);
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function items()
    {
        return $this->hasMany(ItemFactura::class);
    }

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $alicuota = self::where('baciunas_id', $baciunas_id)->first();

        return $alicuota;
    }
    
    public function getNumeroCompleto()
    {
        return str_pad($this->punto_venta->nro, 4, "0", STR_PAD_LEFT) . ' - ' . str_pad($this->numero_comprobante, 8, "0", STR_PAD_LEFT);
    }

    static public function getUltimaFechaFacturaByPuntoVentaId($punto_venta_id, $letra = null)
    {
        $sql = Factura::select('fecha')->where('punto_venta_id', $punto_venta_id)->orderBy('fecha', 'DESC');

        if ($letra)
            $sql->where('letra', $letra);

        return ($factura = $sql->first()) ? $factura->fecha : null;
    }

    public function getFechaEspaniol()
    {
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = strtotime($this->fecha);

        return date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " del ".date('Y', $fecha) ;
    }

    public function getCuitFormateado()
    {
        return substr($this->cuit, 0, 2) . '-'. substr($this->cuit, 2, -1) . '-'. substr($this->cuit, -1);
    }

    public function getBarcodeImage($codigo)
    {
        $file = storage_path('app/barcodes/'.$codigo.'.png');

        if ( !file_exists($file) )
        {
            // Loading Font
            $font = new \BCGFontFile('assets/fonts/Arial.ttf', 18);

            $text = $codigo;
            // The arguments are R, G, B for color.
            $color_black = new \BCGColor(0, 0, 0);
            $color_white = new \BCGColor(255, 255, 255);

            $drawException = null;
            
            try {
                $code = new \BCGcode39();
                $code->setScale(2); // Resolution
                $code->setThickness(30); // Thickness
                $code->setForegroundColor($color_black); // Color of bars
                $code->setBackgroundColor($color_white); // Color of spaces
                $code->setFont($font); // Font (or 0)
                $code->parse($text); // Text
            } catch(Exception $exception) {
                $drawException = $exception;
            }

            /* Here is the list of the arguments
            1 - Filename (empty : display on screen)
            2 - Background color */
            $drawing = new \BCGDrawing($file, $color_white);
            
            if ($drawException) 
            {
                $drawing->drawException($drawException);
            } else {
                $drawing->setBarcode($code);
                $drawing->draw();
            }

            // Header that says it is an image (remove it if you save the barcode to a file)
            #header('Content-Type: image/png');
            #header('Content-Disposition: inline; filename="barcode.png"');

            $drawing->finish(\BCGDrawing::IMG_FORMAT_PNG);
        }

        return base64_encode(file_get_contents($file));
    }

    public function getCodigoBarras($image = false)
    {
        $codigo = $this->cuit_hugo;
        $codigo.= str_pad($this->tipo_comprobante->codigo_afip, 2, "0", STR_PAD_LEFT);
        $codigo.= str_pad($this->punto_venta->nro, 4, "0", STR_PAD_LEFT);
        $codigo.= $this->cae;
        $codigo.= str_replace('-', '',$this->vencimiento_cae);
        $codigo.= $this->getDigitoVerificador($codigo);

        return ($image) ? $this->getBarcodeImage($codigo) : $codigo;
    }

    public function getDigitoVerificador($codigo)
    {
        $dividido = str_split($codigo);
        $suma_pares   = 0;
        $suma_impares = 0;

        for ($i = 0; $i < count($dividido); $i+=2)
        {
            $suma_pares += $dividido[$i];
        }

        for ($i = 1; $i < count($dividido); $i+=2)
        {
            $suma_impares += $dividido[$i];
        }

        $total  = ($suma_pares * 3) + $suma_impares;
        $digito = 10 - ($total - (( (int)($total / 10) ) * 10) );

        return $digito;
    }

    public function getVencimiento()
    {
        $format = (strlen($this->fecha) == 10) ? 'd/m/Y' : ( (strlen($this->fecha) == 8) ? 'Ymd' : 'Y-m-d H:i:s');

        $vencimiento = \DateTime::createFromFormat($format, $this->fecha)
                            ->add(new \DateInterval('P'.$this->condicion_venta->dias_1.'D'));
    
        return $vencimiento->format('Y-m-d H:i:s');
    }

    static public function facturarBySucursal($sucursal, $servicios, $ciclo, $fecha, $periodo_desde, $periodo_hasta, $usuario_id)
    {
        $usuario = Usuario::find($usuario_id);
        $tipo_comprobante = TipoComprobante::retrieveByCodigoAndLetra('FAC', $sucursal->cliente->categoria_iva->tipo_factura);
        $numerado         = NumeradoComprobante::retrieveByTipoComprobanteIdAndPuntoVentaId($tipo_comprobante->id, $usuario->punto_venta_id);
        $letra            = $sucursal->cliente->categoria_iva->tipo_factura;
        $fecha_minima     = Factura::getUltimaFechaFacturaByPuntoVentaId($usuario->punto_venta_id, $letra);

        if ($fecha_minima)
        { 
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $fecha_minima);            
            $date_validar = \DateTime::createFromFormat('d/m/Y H:i:s', $fecha . ' 00:00:00');

            $diferencia = $date->diff($date_validar)->format('%r%a');
        }

        $factura = new Factura();

        $factura->sucursal_id               = $sucursal->id;
        $factura->cliente_id                = $sucursal->cliente_id;
        $factura->letra                     = $letra;
        $factura->tipo_comprobante_id       = $tipo_comprobante->id;
        $factura->numero_comprobante        = $numerado->nro_comprobante_siguiente;
        $factura->punto_venta_id            = $usuario->punto_venta_id;
        
        $factura->razon                     = $sucursal->cliente->razon_social;
        $factura->direccion                 = $sucursal->domicilio_facturacion->calle;
        $factura->codigo_postal             = $sucursal->domicilio_facturacion->codigo_postal;
        $factura->localidad_id              = $sucursal->domicilio_facturacion->localidad_id;
        $factura->provincia_id              = $sucursal->domicilio_facturacion->provincia_id;
        $factura->categoria_iva_id          = $sucursal->cliente->categoria_iva_id;
        $factura->cuit                      = $sucursal->cliente->cuit;
        $factura->vendedor_id               = $sucursal->vendedor_id;
        $factura->condicion_venta_id        = $sucursal->condicion_venta_id;
        
        $factura->activa                    = true;
        $factura->manual                    = false;

        $factura->fecha                     = ($fecha_minima && $diferencia < 0) ? $fecha_minima : $fecha;
        $factura->periodo_facturacion_desde = $periodo_desde;
        $factura->periodo_facturacion_hasta = $periodo_hasta;
        $factura->estado_id                 = 1; # Estado Pendiente
        $factura->created_user_id           = $usuario->id;

        $neto = 0;
        $renglon = 1;
        $items = array();

        $abono = $sucursal->abono;
        $politicas = $sucursal->politicas_productos->keyBy('producto_id');
        
        if ($abono && $abono->activo && !$abono->isFacturado())
        {
            $producto = Producto::where('codigo', 'CUOTA')->first();

            $item = new ItemFactura();

            $item->producto_id     = $producto->id;
            $item->alicuota_id     = $producto->alicuota_id;
            $item->renglon         = $renglon;
            $item->contenedores    = 1;
            $item->precio_unitario = $abono->monto;
            $item->neto            = $abono->monto;
            $item->created_user_id = $usuario->id;

            $neto += $item->neto;

            $items[] = $item;
            $renglon++;

            $abono_facturado = new AbonoFacturado();

            $abono_facturado->monto             = $abono->monto;
            $abono_facturado->sucursal_id       = $sucursal->id;
            $abono_facturado->abono_sucursal_id = $abono->id;
            $abono_facturado->created_user_id   = $usuario->id;
        }

        if ($servicios)
        {
            foreach ($servicios->groupBy('producto_id') as $producto_id => $servicios_prod) 
            {
                $politica_producto = PoliticaProducto::where('producto_id', $producto_id)
                                                     ->where('sucursal_id', $sucursal->id)
                                                     ->first();
                
                if (!$politica_producto)
                    $factura->logError('Sucursal ['.$sucursal->id.'] No se encontró política del producto id '. $producto_id);

                if ( $politica_producto )
                {
                    $abono_producto = $politica_producto->cantidad_mensual;
                    $precios        = $politica_producto->politicas_precios->groupBy('tipo_cantidad_id');
                    
                    $contenedores          = $servicios_prod->sum('contenedores');
                    $kilos_facturar        = $servicios_prod->sum('kilos');            
                    $contenedores_facturar = ($abono && $abono->activo) ? $contenedores - $abono_producto : $contenedores;

                    if ( $contenedores_facturar > 0 && isset($precios[1]))
                    {
                        $index = 0;
                        $desde = 0;

                        $precios_contenedores = $precios[1]->sortBy('cantidad_hasta');

                        while ( $desde < $contenedores_facturar )
                        {
                            $item = new ItemFactura();
                                            
                            $item->producto_id     = $producto_id;
                            $item->alicuota_id     = Producto::find($producto_id)->alicuota_id;
                            $item->renglon         = $renglon;
                            $item->contenedores    = ($contenedores_facturar > $precios_contenedores[$index]->cantidad_hasta) ? ($precios_contenedores[$index]->cantidad_hasta - $desde) : ($contenedores_facturar - $desde);
                            $item->precio_unitario = $precios_contenedores[$index]->precio;
                            $item->neto            = $item->precio_unitario * $item->contenedores;
                            $item->created_user_id = $usuario->is;

                            $neto += $item->neto;

                            $items[] = $item;

                            $desde = $precios_contenedores[$index]->cantidad_hasta;
                            $renglon++;
                            $index++; 
                        }
                    }

                    if ( $kilos_facturar > 0 && isset($precios[2]))
                    {
                        $index = 0;
                        $desde = 0;

                        $producto_tn = Producto::where('codigo', 'TN')->first();
                        $precios_toneladas = $precios[2]->sortBy('cantidad_hasta');
                        $toneladas_facturar = $kilos_facturar / 1000;

                        while ( $desde < $toneladas_facturar )
                        {
                            $item = new ItemFactura();
                                            
                            $item->producto_id     = $producto_tn->id;
                            $item->alicuota_id     = $producto_tn->alicuota_id;
                            $item->renglon         = $renglon;
                            $item->kilos           = ( $toneladas_facturar > $precios_toneladas[$index]->cantidad_hasta ) ? ($precios_toneladas[$index]->cantidad_hasta - $desde) : ($toneladas_facturar - $desde);
                            $item->precio_unitario = $precios_toneladas[$index]->precio;
                            $item->neto            = $item->precio_unitario * $item->kilos;
                            $item->created_user_id = $usuario->id;

                            $neto += $item->neto;

                            $items[] = $item;

                            $desde = $precios_toneladas[$index]->cantidad_hasta;
                            $renglon++;
                            $index++; 
                        }
                    }

                    if (!isset($precios[1]) && $contenedores_facturar > 0)
                        $factura->logError('Sucursal ['.$sucursal->id.'] No se facturaron '. $contenedores_facturar . ' contenedores en Factura Nro.' . $factura->id);

                    if (!isset($precios[2]) && $kilos_facturar > 0)
                        $factura->logError('Sucursal ['.$sucursal->id.'] No se facturaron '. $kilos_facturar . ' kilos en Factura Nro.' . $factura->id);
                }
            }
        }

        $iva = 0;

        foreach ($items as $key => $item)
        {
            # $cantidad = ( $item->kilos > 0 ) ? ($item->kilo / 1000) : $item->contenedores;
            # $iva += $cantidad * $item->precio_unitario * ( $item->producto->alicuota->porcentaje / 100 );
            $iva += $item->neto * ( $item->producto->alicuota->porcentaje / 100 );
        }

        $factura->neto_gravado = $neto;
        $factura->impuestos    = $iva;
        $factura->total        = $factura->neto_gravado + $factura->impuestos;
        
        if ($factura->impuestos > 0.00)
        {
            #   MTXCA
            #   $mtxca = new AfipMtxca();
            #   $results = $mtxca->autorizarComprobanteCAE($factura, $items);
        
            #   WSFE
            $afip_fe = new AfipFe();
            $results = $afip_fe->autorizarComprobanteCAE($factura, $items);

            #   A: Aprobado, O: Observado, R: Rechazado
            if ($results->FECAESolicitarResult->FeCabResp->Resultado == 'R')
            {
                $factura->resultado_cae = $results->FECAESolicitarResult->FeCabResp->Resultado;

                $fallida = new FacturaFallida();
                $fallida->clonar($factura, $items, $results);
            
                return $fallida;
            }

            $numerado->nro_comprobante_siguiente = $numerado->nro_comprobante_siguiente + 1;
            $numerado->fecha_ultima_factura = $factura->fecha;
            $numerado->save();

            if ( isset($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) )
            {
                $observaciones = is_array($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) ? $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones : $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones; 

                foreach ($observaciones as $key => $observacion) 
                    $factura->mtxca_observaciones.= $observacion->Code . ': ' . $observacion->Msg . "\n";
            }

            $factura->resultado_cae   = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Resultado;
            $factura->cae             = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE;                 #   "CAE": 66091002660590
            $factura->vencimiento_cae = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto; #   "fechaVencimientoCAE": "2016-03-03"
            $factura->estado_id = 1; # Estado Autorizado

            $factura->save();

            # Ingreso el comprobante a los pendientes
            Pendiente::ingresarComprobante($factura);
            #Historico::ingresarComprobante($factura);
            #SubdiarioIvaVentas::ingresarComprobante($factura);

            foreach ($items as $key => $item) 
            {
                $item->factura_id = $factura->id;
                $item->save();
            }

            # Update Servicios
            foreach ($servicios as $servicio)
            {
                $servicio->estado_id     = 2;
                $servicio->factura_id    = $factura->id;
                $servicio->fecha_factura = date('d/m/Y H:i:s');

                $servicio->save();
            }

            if (isset($abono_facturado))
            {
                $abono_facturado->factura_id = $factura->id;
                $abono_facturado->save();
            }
        }

        return $factura;
    }

    public function crearFormaManual($input)
    {
        $usuario          = Usuario::find( $input['created_user_id'] );
        $sucursal         = Sucursal::find( $input['sucursal_id'] );
        $tipo_comprobante = TipoComprobante::retrieveByCodigoAndLetra('FAC', $sucursal->cliente->categoria_iva->tipo_factura);
        $numerado         = NumeradoComprobante::retrieveByTipoComprobanteIdAndPuntoVentaId($tipo_comprobante->id, $usuario->punto_venta_id);

        $abono = $sucursal->abono;

        $this->punto_venta_id            = $usuario->punto_venta_id;
        $this->tipo_comprobante_id       = $tipo_comprobante->id;
        $this->numero_comprobante        = $numerado->nro_comprobante_siguiente;

        $this->fecha                     = $input['fecha_facturacion'];
        $this->periodo_facturacion_desde = $input['fecha_desde'];
        $this->periodo_facturacion_hasta = $input['fecha_hasta'];
        $this->observaciones             = $input['observaciones'];

        $this->sucursal_id               = $sucursal->id;
        $this->cliente_id                = $sucursal->cliente_id;
        $this->letra                     = $sucursal->cliente->categoria_iva->tipo_factura;
        $this->razon                     = $sucursal->cliente->razon_social;
        $this->direccion                 = $sucursal->domicilio_facturacion->calle;
        $this->codigo_postal             = $sucursal->domicilio_facturacion->codigo_postal;
        $this->localidad_id              = $sucursal->domicilio_facturacion->localidad_id;
        $this->provincia_id              = $sucursal->domicilio_facturacion->provincia_id;
        $this->categoria_iva_id          = $sucursal->cliente->categoria_iva_id;
        $this->cuit                      = $sucursal->cliente->cuit;
        $this->vendedor_id               = $sucursal->vendedor_id;
        $this->condicion_venta_id        = $sucursal->condicion_venta_id;
        $this->activa                    = true;
        $this->manual                    = true;        
        $this->estado_id                 = 1; # Estado Pendiente
        $this->neto_gravado              = $this->generarNetoManual($input['items']);
        $this->impuestos                 = $this->generarIvaManual($input['items']);
        $this->total                     = $this->neto_gravado + $this->impuestos;
        $this->created_user_id           = $usuario->id;
        
        $items = $this->crearItemsFormaManual($input['items']);
        
        #   MTXCA
        #   $mtxca = new AfipMtxca();
        #   $results = $mtxca->autorizarComprobanteCAE($factura, $items);
    
        #   WSFE
        $afip_fe = new AfipFe();
        $results = $afip_fe->autorizarComprobanteCAE($this, $items);    

        #   A: Aprobado, O: Observado, R: Rechazado
        if ($results->FECAESolicitarResult->FeCabResp->Resultado == 'R')
        {
            $this->resultado_cae = $results->FECAESolicitarResult->FeCabResp->Resultado;

            $fallida = new FacturaFallida();
            $fallida->clonar($this, $items, $results);
        
            return $fallida;
        }
        
        $numerado->nro_comprobante_siguiente = $numerado->nro_comprobante_siguiente + 1;
        $numerado->fecha_ultima_factura = $this->fecha;
        $numerado->save();

        if ( isset($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) )
        {
            $observaciones = is_array($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) ? $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones : $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones; 

            foreach ($observaciones as $key => $observacion) 
                $this->mtxca_observaciones.= $observacion->Code . ': ' . $observacion->Msg . "\n";
        }

        $this->resultado_cae   = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Resultado;
        $this->cae             = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE;                 #   "CAE": 66091002660590
        $this->vencimiento_cae = $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto; #   "fechaVencimientoCAE": "2016-03-03"
        $this->estado_id       = 1; # Estado Autorizado

        $this->save();

        $abono_producto_id = Producto::where('codigo', 'CUOTA')->first()->id;

        foreach ($items as $key => $item) 
        {
            $item->factura_id = $this->id;
            $item->save();

            if ( $abono_producto_id == $item->producto_id )
                $this->registrarAbonoFormaManual($item);
        }

        # Ingreso el comprobante a los pendientes
        Pendiente::ingresarComprobante($this);
        #Historico::ingresarComprobante($this);
        #SubdiarioIvaVentas::ingresarComprobante($this);

        return $this;
    }

    public function generarNetoManual($items)
    {
        $neto = 0;

        foreach ($items as $key => $item_data) 
        {
            $cantidad = ( $item_data['tipo_unidad'] == 2 ) ? ($item_data['cantidad'] / 1000) : $item_data['cantidad']; 
           
            $neto += $cantidad * $item_data['precio_unitario'];
        }

        return $neto;
    }

    public function generarIvaManual($items)
    {
        $iva = 0;

        foreach ($items as $key => $item_data) 
        {
            $cantidad = ( $item_data['tipo_unidad'] == 2 ) ? ($item_data['cantidad'] / 1000) : $item_data['cantidad']; 
            $producto = Producto::find($item_data['producto_id']);

            $iva += $cantidad * $item_data['precio_unitario'] * ( $producto->alicuota->porcentaje / 100 );
        }

        return $iva;
    }

    public function crearItemsFormaManual($items)
    {
        $renglon = 1;
        $items_factura = array(); 

        foreach ($items as $key => $item_data) 
        {
            $cantidad = ( $item_data['tipo_unidad'] == 2 ) ? ($item_data['cantidad'] / 1000) : $item_data['cantidad']; 
            $neto = $cantidad * $item_data['precio_unitario'];
            $item_data['cantidad'] = $cantidad;

            $item = new ItemFactura();
                                            
            $item->producto_id     = $item_data['producto_id'];
            $item->alicuota_id     = 1;
            $item->renglon         = $renglon;

            $item->contenedores    = ( $item_data['tipo_unidad'] == 1 ) ? $cantidad : 0;
            $item->kilos           = ( $item_data['tipo_unidad'] == 2 ) ? $cantidad : 0;
            
            $item->precio_unitario = $item_data['precio_unitario'];
            $item->neto            = $neto;
            $item->created_user_id = $this->created_user_id;

            $items_factura[] = $item;

            $renglon++;
        }

        return $items_factura;
    }

    public function registrarAbonoFormaManual(ItemFactura $item)
    {
        $abono = $this->sucursal->abono;

        $abono_facturado = new AbonoFacturado();

        $abono_facturado->monto             = $item->cantidad() * $item->precio_unitario;
        $abono_facturado->sucursal_id       = $this->sucursal_id;
        $abono_facturado->abono_sucursal_id = $abono->id;
        $abono_facturado->factura_id        = $this->id;
        $abono_facturado->created_user_id   = $this->created_user_id;

        $abono_facturado->save();
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Facturación Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/facturacion.log', Logger::INFO) );
        $view_log->addInfo($contenido);
    }
}