<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class NumeradoComprobante extends Model
{
    public $table = "numerado_comprobantes";

    static public function retrieveByTipoComprobanteIdAndPuntoVentaId($tipo_comprobante_id, $punto_venta_id)
    { 
        return NumeradoComprobante::where('tipo_comprobante_id', $tipo_comprobante_id)
                                  ->where('punto_venta_id', $punto_venta_id)
                                  ->first();
    }
}
