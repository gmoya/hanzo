<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;
use Hanzo\Models\AfipWs;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AfipMtxca extends AfipWs
{
    protected $mtxca_tipos_comprobante = array();
    protected $mtxca_tipos_documento   = array();
    protected $mtxca_alicuotas_iva     = array();
    protected $mtxca_condiciones_iva   = array();
    protected $mtxca_monedas           = array();
    protected $mtxca_cotizacion_moneda;
    protected $mtxca_unidades_medida   = array();
    protected $mtxca_puntos_venta      = array();
    protected $mtxca_puntos_venta_cae  = array();
    protected $mtxca_tipos_tributo     = array();

    protected $cuit = '30689674751';

    protected $mtxca_url  = env('MTXCA_URL');
    protected $mtxca_wsdl = env('MTXCA_WSDL');

    function __construct()
    {
        parent::__construct();

        $this->login('wsmtxca');

        $this->consultarTiposComprobante();
        $this->consultarTiposDocumento();
        $this->consultarAlicuotasIVA();
        $this->consultarCondicionesIVA();
        $this->consultarMonedas();
        $this->consultarCotizacionMoneda();
        $this->consultarUnidadesMedida();
        $this->consultarPuntosVenta();
        $this->consultarPuntosVentaCAE();
        $this->consultarTiposTributo();
    }
    
    public function invocarClienteSoap($action, $parameters)
    {
        $this->client = new \SoapClient($this->mtxca_wsdl, array('soap_version' => SOAP_1_2,
                                                           'location'     => $this->mtxca_url,
                                                           'trace'        => 1,
                                                           'exceptions'   => 0
        ));

        return $this->client->$action($parameters);
    }

    /**
     *  Consulta en AFIP los tipos de comprobantes disponibles
     *  1 – Factura A
     *  2 – Nota de Débito A
     *  3 – Nota de Crédito A
     *  6 – Factura B
     *  7 – Nota de Débito B
     *  8 – Nota de Crédito B
     *  51: Factura M
     *  52: Nota de Débito M
     *  53: Nota de Crédito M
     */
    public function consultarTiposComprobante()
    {
        /*
            $results = $this->invocarClienteSoap('consultarTiposComprobante', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                               )));

            $this->mtxca_tipos_comprobante = collect($results->arrayTiposComprobante->codigoDescripcion);
        */

        $this->mtxca_tipos_comprobante = collect([["codigo" => 1,  "descripcion" => "Factura A"],
                                                  ["codigo" => 2,  "descripcion" => "Nota de Débito A"],
                                                  ["codigo" => 3,  "descripcion" => "Nota de Crédito A"],
                                                  ["codigo" => 6,  "descripcion" => "Factura B"],
                                                  ["codigo" => 7,  "descripcion" => "Nota de Débito B"],
                                                  ["codigo" => 8,  "descripcion" => "Nota de Crédito B"],
                                                  ["codigo" => 51, "descripcion" => "Factura M"],
                                                  ["codigo" => 52, "descripcion" => "Nota de Débito M"],
                                                  ["codigo" => 53, "descripcion" => "Nota de Crédito M"]
                                        ]);
    }

    public function consultarTiposDocumento()
    {
        /*
            $results = $this->invocarClienteSoap('consultarTiposDocumento', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                               )));

            $this->mtxca_tipos_documento = collect($results->arrayTiposDocumento->codigoDescripcion);
        */

        $this->mtxca_tipos_documento = collect([["codigo" => 0,  "descripcion" => "CI Policía Federal"],
                                                ["codigo" => 1,  "descripcion" => "CI Buenos Aires"],
                                                ["codigo" => 2,  "descripcion" => "CI Catamarca"],
                                                ["codigo" => 3,  "descripcion" => "CI Córdoba"],
                                                ["codigo" => 4,  "descripcion" => "CI Corrientes"],
                                                ["codigo" => 5,  "descripcion" => "CI Entre Ríos"],
                                                ["codigo" => 6,  "descripcion" => "CI Jujuy"],
                                                ["codigo" => 7,  "descripcion" => "CI Mendoza"],
                                                ["codigo" => 8,  "descripcion" => "CI La Rioja"],
                                                ["codigo" => 9,  "descripcion" => "CI Salta"],
                                                ["codigo" => 10, "descripcion" => "CI San Juan"],
                                                ["codigo" => 11, "descripcion" => "CI San Luis"],
                                                ["codigo" => 12, "descripcion" => "CI Santa Fe"],
                                                ["codigo" => 13, "descripcion" => "CI Santiago del Estero"],
                                                ["codigo" => 14, "descripcion" => "CI Tucumán"],
                                                ["codigo" => 16, "descripcion" => "CI Chaco"],
                                                ["codigo" => 17, "descripcion" => "CI Chubut"],
                                                ["codigo" => 18, "descripcion" => "CI Formosa"],
                                                ["codigo" => 19, "descripcion" => "CI Misiones"],
                                                ["codigo" => 20, "descripcion" => "CI Neuquén"],
                                                ["codigo" => 21, "descripcion" => "CI La Pampa"],
                                                ["codigo" => 22, "descripcion" => "CI Río Negro"],
                                                ["codigo" => 23, "descripcion" => "CI Santa Cruz"],
                                                ["codigo" => 24, "descripcion" => "CI Tierra del Fuego"],
                                                ["codigo" => 30, "descripcion" => "Certificado de Migración"],
                                                ["codigo" => 80, "descripcion" => "CUIT"],
                                                ["codigo" => 86, "descripcion" => "CUIL"],
                                                ["codigo" => 87, "descripcion" => "CDI"],
                                                ["codigo" => 89, "descripcion" => "LE"],
                                                ["codigo" => 90, "descripcion" => "LC"],
                                                ["codigo" => 91, "descripcion" => "CI Extranjera"],
                                                ["codigo" => 92, "descripcion" => "en trámite"],
                                                ["codigo" => 93, "descripcion" => "Acta Nacimiento"],
                                                ["codigo" => 95, "descripcion" => "CI Bs. As. RNP"],
                                                ["codigo" => 96, "descripcion" => "DNI"],
                                                ["codigo" => 94, "descripcion" => "Pasaporte"]
                                        ]);
    }

    public function consultarAlicuotasIVA()
    {
        /*
            $results = $this->invocarClienteSoap('consultarAlicuotasIVA', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                               )));

            $this->mtxca_alicuotas_iva = collect($results->arrayAlicuotasIVA->codigoDescripcion);
        */
        $this->mtxca_alicuotas_iva = collect([["codigo" => 3, "descripcion" => "0%"],
                                              ["codigo" => 4, "descripcion" => "10.5%"],
                                              ["codigo" => 5, "descripcion" => "21%"],
                                              ["codigo" => 6, "descripcion" => "27%"]
                                        ]);
    }

    /**
     *  0 => 
     *      "codigo": 1
     *      "descripcion": "No gravado"
     *  1 => 
     *      "codigo": 2
     *      "descripcion": "Exento"
     *  2 => 
     *      "codigo": 3
     *      "descripcion": "0%"
     *  3 => 
     *      "codigo": 4
     *      "descripcion": "10.5%"
     *  4 => 
     *      "codigo": 5
     *      "descripcion": "21%"
     *  5 => 
     *      "codigo": 6
     *      "descripcion": "27%"
     */
    public function consultarCondicionesIVA()
    {
        /*
            $results = $this->invocarClienteSoap('consultarCondicionesIVA', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                               )));

            $this->mtxca_condiciones_iva = collect($results->arrayCondicionesIVA->codigoDescripcion);
        */

        $this->mtxca_condiciones_iva = collect([["codigo" => 1, "descripcion" => "No gravado"],
                                                ["codigo" => 2, "descripcion" => "Exento"],
                                                ["codigo" => 3, "descripcion" => "0%"],
                                                ["codigo" => 4, "descripcion" => "10.5%"],
                                                ["codigo" => 5, "descripcion" => "21%"],
                                                ["codigo" => 6, "descripcion" => "27%"]
                                        ]);
    }

    public function consultarMonedas()
    {
        /*
            $results = $this->invocarClienteSoap('consultarMonedas', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                               )));

            $this->mtxca_monedas = collect($results->arrayMonedas->codigoDescripcion);
        */

        $this->mtxca_monedas = collect([["codigo" => "PES", "descripcion" => "Pesos Argentinos"],
                                        ["codigo" => "DOL", "descripcion" => "Dólar Estadounidense"],
                                        ["codigo" => "002", "descripcion" => "Dólar Libre EEUU"],
                                        ["codigo" => "007", "descripcion" => "Florines Holandeses"],
                                        ["codigo" => "009", "descripcion" => "Franco Suizo"],
                                        ["codigo" => "010", "descripcion" => "Pesos Mejicanos"],
                                        ["codigo" => "011", "descripcion" => "Pesos Uruguayos"],
                                        ["codigo" => "012", "descripcion" => "Real"],
                                        ["codigo" => "014", "descripcion" => "Coronas Danesas"],
                                        ["codigo" => "015", "descripcion" => "Coronas Noruegas"],
                                        ["codigo" => "016", "descripcion" => "Coronas Suecas"],
                                        ["codigo" => "018", "descripcion" => "Dólar Canadiense"],
                                        ["codigo" => "019", "descripcion" => "Yens"],
                                        ["codigo" => "021", "descripcion" => "Libra Esterlina"],
                                        ["codigo" => "023", "descripcion" => "Bolívar Venezolano"],
                                        ["codigo" => "024", "descripcion" => "Corona Checa"],
                                        ["codigo" => "025", "descripcion" => "Dinar Yugoslavo"],
                                        ["codigo" => "026", "descripcion" => "Dólar Australiano"],
                                        ["codigo" => "027", "descripcion" => "Dracma Griego"],
                                        ["codigo" => "028", "descripcion" => "Florín (Antillas Holandesas)"],
                                        ["codigo" => "029", "descripcion" => "Güaraní"],
                                        ["codigo" => "030", "descripcion" => "Shekel (Israel)"],
                                        ["codigo" => "031", "descripcion" => "Peso Boliviano"],
                                        ["codigo" => "032", "descripcion" => "Peso Colombiano"],
                                        ["codigo" => "033", "descripcion" => "Peso Chileno"],
                                        ["codigo" => "034", "descripcion" => "Rand Sudafricano"],
                                        ["codigo" => "035", "descripcion" => "Nuevo Sol Peruano"],
                                        ["codigo" => "036", "descripcion" => "Sucre Ecuatoriano"],
                                        ["codigo" => "040", "descripcion" => "Lei Rumano"],
                                        ["codigo" => "041", "descripcion" => "Derechos Especiales de Giro"],
                                        ["codigo" => "042", "descripcion" => "Peso Dominicano"],
                                        ["codigo" => "043", "descripcion" => "Balboas Panameñas"],
                                        ["codigo" => "044", "descripcion" => "Córdoba Nicaragüense"],
                                        ["codigo" => "045", "descripcion" => "Dirham Marroquí"],
                                        ["codigo" => "046", "descripcion" => "Libra Egipcia"],
                                        ["codigo" => "047", "descripcion" => "Riyal Saudita"],
                                        ["codigo" => "049", "descripcion" => "Gramos de Oro Fino"],
                                        ["codigo" => "051", "descripcion" => "Dólar de Hong Kong"],
                                        ["codigo" => "052", "descripcion" => "Dólar de Singapur"],
                                        ["codigo" => "053", "descripcion" => "Dólar de Jamaica"],
                                        ["codigo" => "054", "descripcion" => "Dólar de Taiwan"],
                                        ["codigo" => "055", "descripcion" => "Quetzal Guatemalteco"],
                                        ["codigo" => "056", "descripcion" => "Forint (Hungría)"],
                                        ["codigo" => "057", "descripcion" => "Baht (Tailandia)"],
                                        ["codigo" => "059", "descripcion" => "Dinar Kuwaiti"],
                                        ["codigo" => "060", "descripcion" => "Euro"],
                                        ["codigo" => "063", "descripcion" => "Lempira Hondureña"],
                                        ["codigo" => "062", "descripcion" => "Rupia Hindú"],
                                        ["codigo" => "061", "descripcion" => "Zloty Polaco"],
                                        ["codigo" => "064", "descripcion" => "Yuan (Rep. Pop. China)"]
                                    ]);
    }

    public function consultarCotizacionMoneda()
    {
        /*
            $results = $this->invocarClienteSoap('consultarCotizacionMoneda', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        ),
                                                        'codigoMoneda' => $this->mtxca_monedas->where('descripcion', 'Pesos Argentinos')->first()->codigo
                                                 ));

            $this->mtxca_cotizacion_moneda = empty((array)$results) ? 1 : collect($results->cotizacionMoneda);
        */

        $this->mtxca_cotizacion_moneda = 1;
    }

    public function consultarUnidadesMedida()
    {
        /*
            $results = $this->invocarClienteSoap('consultarUnidadesMedida', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        )));

            $this->mtxca_unidades_medida = collect($results->arrayUnidadesMedida->codigoDescripcion);
        */

        $this->mtxca_unidades_medida = collect([["codigo" => 0,  "descripcion" => " "],
                                                ["codigo" => 1,  "descripcion" => "kilogramos"],
                                                ["codigo" => 2,  "descripcion" => "metros"],
                                                ["codigo" => 3,  "descripcion" => "metros cuadrados"],
                                                ["codigo" => 4,  "descripcion" => "metros cúbicos"],
                                                ["codigo" => 5,  "descripcion" => "litros"],
                                                ["codigo" => 6,  "descripcion" => "1000 kWh"],
                                                ["codigo" => 7,  "descripcion" => "unidades"],
                                                ["codigo" => 8,  "descripcion" => "pares"],
                                                ["codigo" => 9,  "descripcion" => "docenas"],
                                                ["codigo" => 10, "descripcion" => "quilates"],
                                                ["codigo" => 11, "descripcion" => "millares"],
                                                ["codigo" => 14, "descripcion" => "gramos"],
                                                ["codigo" => 15, "descripcion" => "milimetros"],
                                                ["codigo" => 16, "descripcion" => "mm cúbicos"],
                                                ["codigo" => 17, "descripcion" => "kilómetros"],
                                                ["codigo" => 18, "descripcion" => "hectolitros"],
                                                ["codigo" => 20, "descripcion" => "centímetros"],
                                                ["codigo" => 25, "descripcion" => "jgo. pqt. mazo naipes"],
                                                ["codigo" => 27, "descripcion" => "cm cúbicos"],
                                                ["codigo" => 29, "descripcion" => "toneladas"],
                                                ["codigo" => 30, "descripcion" => "dam cúbicos"],
                                                ["codigo" => 31, "descripcion" => "hm cúbicos"],
                                                ["codigo" => 32, "descripcion" => "km cúbicos"],
                                                ["codigo" => 33, "descripcion" => "microgramos"],
                                                ["codigo" => 34, "descripcion" => "nanogramos"],
                                                ["codigo" => 35, "descripcion" => "picogramos"],
                                                ["codigo" => 41, "descripcion" => "miligramos"],
                                                ["codigo" => 47, "descripcion" => "mililitros"],
                                                ["codigo" => 48, "descripcion" => "curie"],
                                                ["codigo" => 49, "descripcion" => "milicurie"],
                                                ["codigo" => 50, "descripcion" => "microcurie"],
                                                ["codigo" => 51, "descripcion" => "uiacthor"],
                                                ["codigo" => 52, "descripcion" => "muiacthor"],
                                                ["codigo" => 53, "descripcion" => "kg base"],
                                                ["codigo" => 54, "descripcion" => "gruesa"],
                                                ["codigo" => 61, "descripcion" => "kg bruto"],
                                                ["codigo" => 62, "descripcion" => "uiactant"],
                                                ["codigo" => 63, "descripcion" => "muiactant"],
                                                ["codigo" => 64, "descripcion" => "uiactig"],
                                                ["codigo" => 65, "descripcion" => "muiactig"],
                                                ["codigo" => 66, "descripcion" => "kg activo"],
                                                ["codigo" => 67, "descripcion" => "gramo activo"],
                                                ["codigo" => 68, "descripcion" => "gramo base"],
                                                ["codigo" => 95, "descripcion" => "anulación/devolución"],
                                                ["codigo" => 96, "descripcion" => "packs"],
                                                ["codigo" => 97, "descripcion" => "seña/anticipo"],
                                                ["codigo" => 98, "descripcion" => "otras unidades"],
                                                ["codigo" => 99, "descripcion" => "bonificación"]
                                        ]);
    }

    public function consultarPuntosVenta()
    {
        /*
            $results = $this->invocarClienteSoap('consultarPuntosVenta', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        )));

            $this->mtxca_puntos_venta = collect($results->arrayPuntosVenta); # HARDCODE
        */

        $this->mtxca_puntos_venta = collect();
    }

    public function consultarPuntosVentaCAE()
    {
        /*
            $results = $this->invocarClienteSoap('consultarPuntosVentaCAE', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        )));

            $this->mtxca_puntos_venta_cae = collect($results->arrayPuntosVenta); # HARDCODE
        */

        $this->mtxca_puntos_venta_cae = collect();
    }

    public function consultarTiposTributo()
    {
        /*
            $results = $this->invocarClienteSoap('consultarTiposTributo', 
                                                 array('authRequest' => array(
                                                                                'token'            => $this->token,
                                                                                'sign'             => $this->sign,
                                                                                'cuitRepresentada' => $this->cuit
                                                                        )));

            $this->mtxca_tipos_tributo = collect($results->arrayTiposTributo->codigoDescripcion);
        */

        $this->mtxca_tipos_tributo = collect([["codigo" => 1,  "descripcion" => "Impuestos Nacionales"],
                                              ["codigo" => 2,  "descripcion" => "Impuestos Provinciales"],
                                              ["codigo" => 3,  "descripcion" => "Impuestos Municipales"],
                                              ["codigo" => 4,  "descripcion" => "Impuestos Internos"],
                                              ["codigo" => 99, "descripcion" => "Otros"]
                                    ]);
    }

    public function consultarUltimoComprobanteAutorizado($codigo_comprobante, $punto_venta)
    {
        $results = $this->invocarClienteSoap('consultarUltimoComprobanteAutorizado', 
                                             array( 
                                                   'authRequest' => array(
                                                                            'token'            => $this->token,
                                                                            'sign'             => $this->sign,
                                                                            'cuitRepresentada' => $this->cuit
                                                                    ),
                                                    'consultaUltimoComprobanteAutorizadoRequest' => array(
                                                                                                            'codigoTipoComprobante' => $codigo_comprobante,
                                                                                                            'numeroPuntoVenta'      => $punto_venta)
                                                                                                          ));

        return $results;
    }

    public function autorizarComprobanteCAE(Factura $factura, $items = array())
    {
        $comprobante = $this->generarComprobanteFactura($factura, $items);

        $results = $this->invocarClienteSoap('autorizarComprobante', $comprobante);

        if (is_soap_fault($results) || $results->resultado != 'A')
        {
            $view_log = new Logger('Afip Mtxca Log');
            $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/afip_mtxca.log', Logger::INFO) );
            $view_log->addInfo($this->client->__getLastRequest());
            $view_log->addInfo($this->client->__getLastResponse());
            
            if ( !is_soap_fault($results) )
                $view_log->addInfo(serialize($results));
        }

        return $results;
    }

    public function generarComprobanteFactura(Factura $factura, $items = array())
    {
        $codigo_concepto = 2; # HARDCODE Código de Servicios
        $format = (strlen($factura->fecha) == 10) ? 'd/m/Y' : 'Y-m-d H:i:s';

        $date = new \DateTime();
        $vencimiento = $date->createFromFormat($format, $factura->fecha)
                            ->add(new \DateInterval('P'.$factura->sucursal->condicion_venta->dias_1.'D'));
        $emision = $date->createFromFormat($format, $factura->fecha);
        $desde   = $date->createFromFormat($format, $factura->periodo_facturacion_desde);
        $hasta   = $date->createFromFormat($format, $factura->periodo_facturacion_hasta);

        $comprobante = array('authRequest' => array(), 'comprobanteCAERequest' => array());

        $comprobante['authRequest'] = 
            array(
                    'token'            => $this->token,
                    'sign'             => $this->sign,
                    'cuitRepresentada' => $this->cuit
           );

        $comprobante['comprobanteCAERequest'] = 
            array(
                    'codigoTipoComprobante'  => $this->mtxca_tipos_comprobante->where('codigo', $factura->tipo_comprobante->codigo_afip)->first()['codigo'],
                    'numeroPuntoVenta'       => $factura->punto_venta->nro,
                    'numeroComprobante'      => $factura->numero_comprobante,
                    'fechaEmision'           => $emision->format('Y-m-d'),
                    #'codigoTipoAutorizacion' => '',
                    #'codigoAutorizacion'     => '',
                    #'fechaVencimiento'       => '',
                    'codigoTipoDocumento'    => $this->mtxca_tipos_documento->where('descripcion', 'CUIT')->first()['codigo'],
                    'numeroDocumento'        => $factura->cuit,
                    'importeGravado'         => number_format($factura->neto_gravado, 2 , '.', ''),
                    'importeNoGravado'       => '0.00',
                    'importeExento'          => '0.00',
                    'importeSubtotal'        => number_format($factura->neto_gravado, 2 , '.', ''),
                    #'importeOtrosTributos'   => '0.00',
                    'importeTotal'           => number_format($factura->total, 2 , '.', ''),
                    'codigoMoneda'           => $this->mtxca_monedas->where('descripcion', 'Pesos Argentinos')->first()['codigo'],
                    'cotizacionMoneda'       => $this->mtxca_cotizacion_moneda,
                    'observaciones'          => '',
                    'codigoConcepto'         => $codigo_concepto,
                    'fechaServicioDesde'     => $desde->format('Y-m-d'),
                    'fechaServicioHasta'     => $hasta->format('Y-m-d'),
                    'fechaVencimientoPago'   => $vencimiento->format('Y-m-d'),
            );
        #        dd($comprobante['comprobanteCAERequest']['numeroComprobante']);
        /*
        $comprobante['autorizarComprobanteRequest']['comprobanteCAERequest']['comprobantesAsociados'] = array();
        $comprobante['autorizarComprobanteRequest']['comprobanteCAERequest']['comprobantesAsociados'][] = 
            array(
                  'codigoTipoComprobante' => '',
                  'numeroPuntoVenta'      => '',
                  'numeroComprobante'     => ''
            );
                                                                                     
        $comprobante['autorizarComprobanteRequest']['comprobanteCAERequest']['otrosTributos'] = array();
        $comprobante['autorizarComprobanteRequest']['comprobanteCAERequest']['otrosTributos'][] = 
            array(
                  'codigo'        => '',
                  'descripcion'   => '',
                  'baseImponible' => '',
                  'importe'       => ''
            );
        */

        $comprobante['comprobanteCAERequest']['arrayItems'] = array();
        
        $items = $items ? $items : $factura->items;

        foreach ($items as $key => $item) 
        {
            $cantidad        = ( ($item->contenedores > 0) ? $item->contenedores : ( ($item->kilos > 0) ? $item->kilos : 1 ) );
            $tipo_unidad     = $this->mtxca_unidades_medida->where('descripcion', ( !is_null($item->kilos) ? 'toneladas' : 'otras unidades' ))->first()['codigo'];
            $alicuota        = $item->producto->alicuota;
            $descripcion_iva = ( $alicuota ? (($alicuota->abreviatura == 'IVA10.5') ? '10.5%' : ( ($alicuota->abreviatura == 'IVA21') ? '21%' : '0%')) : '0%' );
            $condicion_iva   = $this->mtxca_condiciones_iva->where('descripcion', $descripcion_iva)->first()['codigo'];
            $importe_iva     = $alicuota ? $item->neto * "0.$alicuota->porcentaje" : 0;
            $importe_item    = $alicuota ? $item->neto * "1.$alicuota->porcentaje" : $item->neto;

            $item->precio_unitario = ($factura->letra == 'B' && $alicuota) ? $item->precio_unitario * "1.$alicuota->porcentaje" : $item->precio_unitario;

            $valores = 
                array('codigo'              => $item->producto->codigo,
                      'unidadesMtx'         => $tipo_unidad,
                      'codigoMtx'           => $item->producto->codigo,
                      'descripcion'         => $item->producto->nombre,
                      'cantidad'            => $cantidad,
                      'codigoUnidadMedida'  => $tipo_unidad,
                      'precioUnitario'      => number_format($item->precio_unitario, 6, '.', ''),
                    # 'importeBonificacion' => '',
                      'codigoCondicionIVA'  => $condicion_iva,
                      'importeItem'         => number_format($importe_item, 2, '.', '')
                );

            if ($factura->letra == 'A')
                $valores['importeIVA'] = number_format($importe_iva, 2, '.', '');


            $comprobante['comprobanteCAERequest']['arrayItems'][] = $valores;
        }
        
        $comprobante['comprobanteCAERequest']['arraySubtotalesIVA'] = array();

        $comprobante['comprobanteCAERequest']['arraySubtotalesIVA'][] = array('codigo'  => $condicion_iva,
                                                                              'importe' => number_format($factura->impuestos, 2, '.', '') );

        return $comprobante;
    }
}