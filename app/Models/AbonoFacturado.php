<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class AbonoFacturado extends Model
{
    protected $table = 'abonos_facturados';

    static public function getSucursalesAbonosFacturadosUltimoMes()
    {
        $sql = AbonoFacturado::select('sucursal_id')
                             ->whereRaw("(NOW() - created_at) < '28 days'");

        return $sql->get();
    }

    static public function getAbonoFacturadoUltimoMesBySucursal($sucursal_id)
    {
        $sql = AbonoFacturado::where('sucursal_id', $sucursal_id)
                             ->whereRaw("(NOW() - created_at) < '28 days'");

        return $sql->first();
    }
}
