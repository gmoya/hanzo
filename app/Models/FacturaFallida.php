<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;
use Hanzo\Models\ItemFacturaFallida;

class FacturaFallida extends Model
{
    protected $table = 'facturas_fallidas';

    public function clonar(Factura $factura, $items, $results)
    {
        $this->talonario_id              = $factura->talonario_id;
        $this->sucursal_id               = $factura->sucursal_id;
        $this->cliente_id                = $factura->cliente_id;
        $this->tipo_comprobante_baciunas = $factura->tipo_comprobante_baciunas;
        $this->letra                     = $factura->letra;
        $this->punto_venta_baciunas      = $factura->punto_venta_baciunas;
        $this->numero                    = $factura->numero;
        $this->fecha                     = $factura->fecha;
        $this->razon                     = $factura->razon;
        $this->direccion                 = $factura->direccion;
        $this->codigo_postal             = $factura->codigo_postal;
        $this->localidad_id              = $factura->localidad_id;
        $this->provincia_id              = $factura->provincia_id;
        $this->categoria_iva_id          = $factura->categoria_iva_id;
        $this->cuit                      = $factura->cuit;
        $this->vendedor_id               = $factura->vendedor_id;
        $this->condicion_venta_id        = $factura->condicion_venta_id;
        $this->activa                    = $factura->activa;
        $this->manual                    = $factura->manual;
        $this->punto_venta_id            = $factura->punto_venta_id;
        $this->tipo_comprobante_id       = $factura->tipo_comprobante_id;
        $this->periodo_facturacion_desde = $factura->periodo_facturacion_desde;
        $this->periodo_facturacion_hasta = $factura->periodo_facturacion_hasta;
        $this->observaciones             = $factura->observaciones;
        $this->mtxca_observaciones       = $factura->mtxca_observaciones;
        $this->mtxca_errores             = $factura->mtxca_errores;
        $this->cae                       = $factura->cae;
        $this->vencimiento_cae           = $factura->vencimiento_cae;
        $this->estado_id                 = $factura->estado_id;
        $this->fecha_anulacion           = $factura->fecha_anulacion;
        $this->neto_gravado              = $factura->neto_gravado;
        $this->neto_no_gravado           = $factura->neto_no_gravado;
        $this->impuestos                 = $factura->impuestos;
        $this->total                     = $factura->total;
        $this->cuenta_corriente_id       = $factura->cuenta_corriente_id;
        $this->created_user_id           = $factura->created_user_id;
        
        if ( isset($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) )
        {
            $observaciones = is_array($results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) ? $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones : $results->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones; 

            foreach ($observaciones as $key => $observacion) 
                $this->mtxca_observaciones.= $observacion->Code . ': ' . $observacion->Msg . "\n";
        }

        if ( isset($results->FECAESolicitarResult->Errors) )
        {
            $errores = is_array($results->FECAESolicitarResult->Errors) ? $results->FECAESolicitarResult->Errors : $results->FECAESolicitarResult->Errors; 

            foreach ($errores as $key => $error) 
                $this->mtxca_observaciones.= $error->Code . ': ' . $error->Msg . "\n";
        }

        $this->save();
    
        foreach ($items as $key => $item) 
        {
            $item_failed = new ItemFacturaFallida();

            $item_failed->producto_id        = $item->producto_id;
            $item_failed->factura_fallida_id = $this->id;
            $item_failed->alicuota_id        = $item->alicuota_id;
            $item_failed->renglon            = $item->renglon;
            $item_failed->contenedores       = $item->contenedores;
            $item_failed->kilos              = $item->kilos;
            $item_failed->precio_unitario    = $item->precio_unitario;
            $item_failed->impuestos          = $item->impuestos;
            $item_failed->neto               = $item->neto;
            $item_failed->nota_factura       = $item->nota_factura;
            $item_failed->created_user_id    = $item->created_user_id;

            $item_failed->save();
        }
    }
}
