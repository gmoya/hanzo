<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class PuntoVenta extends Model
{
    public $table = "puntos_venta";
}
