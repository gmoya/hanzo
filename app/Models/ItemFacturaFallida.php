<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class ItemFacturaFallida extends Model
{
    protected $table = 'items_factura_fallida';
}
