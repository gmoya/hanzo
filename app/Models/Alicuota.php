<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Alicuota extends Model
{
    protected $table = 'alicuotas';
    
    static public function retrieveByBaciunasId($baciunas_id)
    {
        $alicuota = self::where('baciunas_id', $baciunas_id)->first();

        return $alicuota;
    }
}
