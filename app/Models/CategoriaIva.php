<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaIva extends Model
{
    protected $table = 'categorias_iva';

    static public function retrieveByPk($baciunas_id)
    {
        $categoria = self::where('id', $baciunas_id)->first();

        return $categoria;
    }
}
