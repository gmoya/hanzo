<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $table = "productos";

    public static $rules = [
        "codigo"    => "required",
        "nombre"    => "required"
    ];

    public $fillable = [
        "codigo",
        "nombre",
        "alicuota_id",
        "created_user_id"
    ];

    public function alicuota()
    {
        return $this->belongsTo(Alicuota::class);
    }

    public function scopeProducto($query, $producto)
    {
        if (trim($producto) != '')
        {
            $query->where('codigo',   'ilike', '%' . $producto . '%')
                  ->orWhere('nombre', 'ilike', '%' . $producto . '%');
        }
    }

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $producto = self::where('baciunas_id', $baciunas_id)->first();

        return $producto;
    }
}
