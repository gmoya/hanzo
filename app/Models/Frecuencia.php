<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Frecuencia extends Model
{
    protected $table = 'frecuencias';

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $alicuota = self::where('baciunas_id', $baciunas_id)->first();

        return $alicuota;
    }
}
