<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class ItemFactura extends Model
{
    protected $table = 'items_factura';
    
    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
    
    public function alicuota()
    {
        return $this->belongsTo(Alicuota::class);
    }

    public function cantidad()
    {
        return $this->kilos ? $this->kilos : $this->contenedores; 
    }
}
