<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class RubroEmpresario extends Model
{
    public $table = "rubros_empresarios";

    static public function retrieveByBaciunasId($baciunas_id)
    {
        $rubro = self::where('baciunas_id', $baciunas_id)->first();

        return $rubro;
    }
}
