<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

ini_set('soap.wsdl_cache_enabled', '0');

class AfipWs extends Model
{
    protected $cms;
    protected $sign;
    protected $token;
    protected $expiration_time;
    protected $wsdl;                # The WSDL corresponding to WSAA
    protected $cert;                # The X.509 certificate in PEM format
    protected $private_key;         # The private key correspoding to CERT (PEM)
    protected $passphrase = 'hugo'; # The passphrase (if any) to sign
    protected $login_url;

    function __construct()
    {
        $this->login_url   = env('LOGIN_URL');
        $this->wsdl        = storage_path('app/afipws/wsaa.wsdl');
        $this->cert        = storage_path(env('CERT'));
        $this->private_key = storage_path(env('PRIVATE_KEY'));
    }

    public function createTra($service)
    {
        $tra = new \SimpleXMLElement(    '<?xml version="1.0" encoding="UTF-8"?>' .
                                        '<loginTicketRequest version="1.0">'.
                                        '</loginTicketRequest>'     );
        
        $tra->addChild('header');
        $tra->header->addChild('uniqueId', date('U'));
        $tra->header->addChild('generationTime', date('c',date('U')-60));
        $tra->header->addChild('expirationTime', date('c',date('U')+60));
        $tra->addChild('service', $service);

        $tra->asXML(storage_path('app/afipws/tra.xml'));
    }

    /**
     *  Firma el pedido de autorizacióncon PKCS#7 utilizando el TRA xml, CERT y
     *  PRIVATEKEY.
     */
    public function signTra()
    {
        $status = openssl_pkcs7_sign(   storage_path('app/afipws/tra.xml'), storage_path('app/afipws/tra.tmp'), 
                                        "file://" . $this->cert, array("file://" . $this->private_key, $this->passphrase), 
                                        array(), !PKCS7_DETACHED   );

        if (!$status) 
            exit("ERROR generando firma PKCS#7 \n");
      
        $inf = fopen(storage_path('app/afipws/tra.tmp'), 'r');
        $i = 0;
        $this->cms = '';
      
        while (!feof($inf)) 
        { 
            $buffer = fgets($inf);

            if ( $i++ >= 4 )
                $this->cms.= $buffer;
        }

        fclose($inf);
        #  unlink("TRA.xml");
        unlink(storage_path('app/afipws/tra.tmp'));
    }

    public function callWsaa()
    {
        $client = new \SoapClient($this->wsdl, array(   'soap_version'    => SOAP_1_2,
                                                        'location'       => $this->login_url,
                                                        'trace'          => 1,
                                                        'exceptions'     => 0           )); 
      
        $results = $client->loginCms(array('in0' => $this->cms));

        file_put_contents(storage_path('app/afipws/request-loginCms.xml'), $client->__getLastRequest());
        file_put_contents(storage_path('app/afipws/response-loginCms.xml'), $client->__getLastResponse());
      
        if (is_soap_fault($results)) 
            exit('SOAP Fault: ' . $results->faultcode . "\n" . $results->faultstring . "\n");
      
        file_put_contents(storage_path('app/afipws/ta.xml'), $results->loginCmsReturn);

        $response = new \SimpleXMLElement($results->loginCmsReturn);

        $this->sign            = $response->credentials->sign->__toString();
        $this->token           = $response->credentials->token->__toString();
        $this->expiration_time = $response->header->expirationTime->__toString();
    }

    public function verificarTicketAcceso()
    {
        $file = storage_path('app/afipws/ta.xml');

        if (file_exists($file))
        {
            $ta = new \SimpleXMLElement(file_get_contents($file));

            $this->sign            = $ta->credentials->sign->__toString();
            $this->token           = $ta->credentials->token->__toString();
            $this->expiration_time = $ta->header->expirationTime->__toString();
        }
    }

    public function login($service)
    {
        if (!file_exists($this->cert)) { exit("Error al abrir " . $this->cert . "\n"); }
        if (!file_exists($this->private_key)) { exit("Error al abrir " . $this->private_key . "\n"); }
        if (!file_exists($this->wsdl)) { exit("Error al abrir " . $this->wsdl . "\n"); }

        $this->verificarTicketAcceso();

        if ( !$this->expiration_time || $this->expiration_time < date('c',date('U')) )
        {
            $this->createTra($service);
            $this->signTra();
            $this->callWsaa();
        }
    }
}

