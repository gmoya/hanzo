<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Talonario extends Model
{
    public $table = "talonarios";
    
    static public function retrieveByBaciunasId($baciunas_id)
    {
        $talonario = self::where('baciunas_id', $baciunas_id)->first();

        return $talonario;
    }
}
