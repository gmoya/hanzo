<?php

namespace Hanzo\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';

    public static $rules = [
        "sucursal_id"  => "required",
        "fecha"        => "required",
        "producto_id"  => "required",
        "contenedores" => "required"
    ];

    public $fillable = [
        "cliente_id",
        "sucursal_id",
        "estado_id",
        "fecha",
        "producto_id",
        "contenedores",
        "kilos",
        "direccion",
        "nro_manifiesto",
        "nro_cr",
        "observaciones",
        "created_user_id"
    ];
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
    
    public function scopeSucursalCliente($query, $nombre)
    {
        if (trim($nombre) != '')
        {
            $query->select('servicios.*')
                  ->join('sucursales', 'servicios.sucursal_id', '=', 'sucursales.id')
                  ->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                  ->where('sucursales.nombre',     'ilike', '%' . $nombre . '%')
                  ->where('clientes.razon_social', 'ilike', '%' . $nombre . '%');
        }
    }

    static public function getServiciosPendientes($desde, $hasta, $ciclo, $clientes_excluidos = null)
    {
        $sql = Servicio::select('servicios.id', 'servicios.sucursal_id')
                        ->join('sucursales', 'servicios.sucursal_id', '=', 'sucursales.id')
                        ->where('servicios.estado_id', 1) # Estado Pendiente
                        ->where('servicios.fecha', '>=', $desde.' 00:00:00')
                        ->where('servicios.fecha', '<=', $hasta.' 23:59:59')
                        #->whereIn('sucursales.nro_cliente', array(2850, 2910, 3332, 4445, 4775, 4776, 4781, 4784, 4785, 4855, 4960, 5055, 5056, 5057, 5059, 5064, 5095, 5122, 5140, 5160, 5212, 5228, 5246, 5306, 5316))
                        
                        ->where('sucursales.ciclo_facturacion_id', $ciclo);
        
        if ($clientes_excluidos)
            $sql->whereNotIn('sucursales.nro_cliente', $clientes_excluidos); # QUITAR Clientes excluídos

        #dd($sql->toSql(), $sql->getBindings(), $sql->get());
        # QUITAR
        #->whereNotIn('servicios.baciunas_id', [1275215, 1276731, 1278588, 1278602, 1283415, 1287537, 1290685, 1290836, 1292237, 1294796, 1298196])
        #->whereNotIn('sucursales.nro_cliente', [564, 1923, 2634]);

        return $sql->get();
    }

    static public function getServiciosPendientesBySucursal($sucursal_id, $desde, $hasta)
    {
        return Servicio::where('sucursal_id', $sucursal_id)
                        ->where('estado_id', 1)
                        ->where('fecha', '>=', $desde)
                        ->where('fecha', '<=', $hasta)
                        ->get();
    }

    static public function getSucursalesIdServiciosPendientes($desde, $hasta)
    {
        return Servicio::select(DB::raw('DISTINCT(sucursal_id) AS sucursal_id'))
                        ->where('estado_id', 1)
                        ->where('fecha', '>=', $desde)
                        ->where('fecha', '<=', $hasta)
                        ->orderBy('sucursal_id', 'ASC')
                        ->get();
    }
}
