<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Pendiente extends Model
{
    public $table = "pendientes";

    public function tipo_comprobante()
    {
        return $this->belongsTo(TipoComprobante::class);
    }

    public function punto_venta()
    {
        return $this->belongsTo(PuntoVenta::class);
    }

    public function getEstado()
    {
        $estado = '';

        switch ($this->estado_id) 
        {
            case 2:
                $estado = 'Saldo';
                break;
            
            case 3:
                $estado = 'Pagado';
                break;
            
            default:
                $estado = 'Pendiente';
                break;
        }

        return $estado;
    }

    static public function ingresarComprobante($factura)
    {
        $pendiente = new Pendiente();

        $pendiente->cliente_id          = $factura->cliente_id;
        $pendiente->razon_social        = $factura->razon;
        $pendiente->cuit                = $factura->cuit;
        $pendiente->comprobante_id      = $factura->id;
        $pendiente->fecha               = $factura->fecha;
        $pendiente->vencimiento         = $factura->getVencimiento();
        $pendiente->letra               = $factura->letra;
        $pendiente->numero_comprobante  = $factura->numero_comprobante;
        $pendiente->punto_venta_id      = $factura->punto_venta_id;
        $pendiente->tipo_comprobante_id = $factura->tipo_comprobante_id;
        $pendiente->importe             = $factura->total;
        $pendiente->estado_id           = 1;    
        $pendiente->created_user_id     = $factura->created_user_id;

        $pendiente->save();
    }
}
