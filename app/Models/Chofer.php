<?php

namespace Hanzo\Models;

use Illuminate\Database\Eloquent\Model;

class Chofer extends Model
{
    public $table = "choferes";
    
    static public function retrieveByBaciunasId($baciunas_id)
    {
        $condicion = self::where('baciunas_id', $baciunas_id)->first();

        return $condicion;
    }
}
