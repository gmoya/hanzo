<?php

namespace Hanzo\Http\Controllers;

use Flash;

use Illuminate\Http\Request;
use Hanzo\Http\Requests;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Hanzo\Models\CondicionVenta;
use Hanzo\Models\Factura;
use Hanzo\Models\FacturaFallida;
use Hanzo\Models\Servicio;
use Hanzo\Models\Pendiente;
use Hanzo\Models\Historico;
use Hanzo\Models\SubdiarioIvaVentas;
use Hanzo\Models\Sucursal;
use Hanzo\Models\AbonoSucursal;
use Hanzo\Models\AfipWs;
use Hanzo\Models\AfipFe;
use Hanzo\Models\AfipMtxca;

use Hanzo\Http\Controllers\Controller;
use Hanzo\Libraries\Repositories\FacturaRepository;

class FacturaController extends Controller
{
    /** @var  FacturaRepository */
    private $FacturaRepository;

    function __construct(FacturaRepository $facturaRepo)
    {
        $this->middleware('auth');
        $this->facturaRepository = $facturaRepo;
    }

    public function getPendientesFacturacion(Request $request)
    {
        $totales = array();
        $inputs = $request->input();
        $servicios = Servicio::getServiciosPendientes($inputs['fecha_desde'], $inputs['fecha_hasta'], $inputs['ciclo_facturacion_id']);
        
        $totales['servicios'] = count($servicios);

        $servicios = $servicios->sortBy('sucursal_id')->groupBy('sucursal_id');

        #$abonos = AbonoSucursal::getAbonosPendientes($inputs['fecha_desde'], $inputs['fecha_hasta'], $inputs['ciclo_facturacion_id'], $servicios->keys()->toArray(), [564, 1923, 2634]);
        $abonos = AbonoSucursal::getAbonosPendientes($inputs['fecha_desde'], $inputs['fecha_hasta'], $inputs['ciclo_facturacion_id'], $servicios->keys()->toArray());

        $totales['abonos'] = count($abonos);
        
        $view_log = new Logger('Facturación Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/facturacion.log', Logger::INFO) );
        $view_log->addInfo('Se van a facturar ' . $totales['servicios'] . ' servicios pendientes.');
        $view_log->addInfo('Se van a facturar ' . $totales['abonos'] . ' abonos pendientes.');
        
        echo collect(['servicios' => $servicios, 'abonos' => $abonos, 'totales' => $totales ])->toJson();
        die;
    }

    public function imprimir(Request $request)
    {
        $facturas = collect();
        $inputs   = $request->input();

        if ($inputs)
        {
            if ($inputs['desde'] > $inputs['hasta']) 
                Flash::error('El Nro. Comprobante Desde debe ser menor al Nro. Comprobante Hasta.');
            else {
                $facturas = Factura::where('numero_comprobante', '>=', $inputs['desde'])
                                   ->where('numero_comprobante', '<=', $inputs['hasta'])
                                   ->where('letra', $inputs['letra'])
                                   ->where('punto_venta_id', $request->user()->punto_venta_id)
                                   ->orderBy('id', 'ASC')
                                   ->get();

                if ( $facturas->isEmpty() )
                    Flash::error('No se encontraron facturas con los datos ingresados.');
            }
        }

        $options = array('tipos_factura' => array('A' => 'Factura A', 'B' => 'Factura B'));
        $options['copias'] = array('original', 'duplicado', 'triplicado');

        return view('facturas.impresiones')
                    ->with('facturas', $facturas)
                    ->with('options', $options);
    }

    public function facturarBySucursal(Request $request)
    {
        $inputs       = $request->input();
        $sucursal_id  = $inputs['sucursal_id'];
        $servicios_id = array();
        $sucursal     = Sucursal::where('id', $sucursal_id)->first();

        if (isset($inputs['servicios']))
        {
            foreach ($inputs['servicios'] as $servicio)
            {
                $servicios_id[] = $servicio['id'];
            }

            $servicios = Servicio::select('servicios.id', 
                                          'servicios.contenedores', 
                                          'servicios.kilos', 
                                          'servicios.sucursal_id', 
                                          'servicios.producto_id')
                                    ->whereIn('id', $servicios_id)
                                    ->get();
        }

        $servicios = (isset($servicios)) ? $servicios : collect([]);


        $resultado  = Factura::facturarBySucursal($sucursal, $servicios, 
                                                $inputs['ciclo_facturacion_id'], 
                                                $inputs['fecha_facturacion'],
                                                $inputs['fecha_desde'],
                                                $inputs['fecha_hasta'],
                                                $request->user()->id
                    );
     
        if (get_class($resultado) == 'Hanzo\Models\Factura')
        {
            echo($resultado->toJson()); die;
        }
        elseif (get_class($resultado) == 'Hanzo\Models\FacturaFallida')
        {
            $mensaje = 'Factura Sucursal ID '. $resultado->sucursal_id. ' Error ->';

            if ($resultado->mtxca_errores)
                $mensaje.= $resultado->mtxca_errores;

            if ($resultado->mtxca_observaciones)
                $mensaje.= $resultado->mtxca_observaciones;
            
            Flash::error($mensaje);
            //echo($resultado->toJson()); die;
        }
    }
    
    # ["codigo" => 1,  "descripcion" => "Factura A"],
    # ["codigo" => 6,  "descripcion" => "Factura B"],
    public function facturar(Request $request)
    {
        # $afip_fe = new AfipFe();
        # dd($afip_fe->consultarUltimoComprobanteAutorizado(1, 6));
        $fecha_minima = Factura::getUltimaFechaFacturaByPuntoVentaId($request->user()->punto_venta_id);

        if ($fecha_minima)
        { 
            $hoy = new \DateTime();
            $fecha = \DateTime::createFromFormat('Y-m-d H:i:s', $fecha_minima);            
            $fecha_minima = $hoy->diff($fecha)->format('%r%a');
        }

        $fecha_minima = $fecha_minima ? $fecha_minima : '-5';

        $options = array('ciclos' => ['' => '', '1' => 'Mensual', '2' => 'Fin', '3' => 'Factura B'], 'fecha_min' => $fecha_minima);
        
        $scripts = array('facturar_form.js');

        return view('facturas.facturar')
            ->with('scripts', $scripts)
            ->with('options', $options);
    }

    public function facturacion(Request $request)
    {
        $facturas = Factura::get();

        return view('facturas.facturacion')
            ->with('facturas', $facturas);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas = Factura::orderBy('id', 'DESC')->paginate();
        $query = array();
        
        return view('facturas.index')
            ->with('query', $query)
            ->with('facturas', $facturas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $options = array();
        $options['condiciones_venta'] = ['' => ''] + CondicionVenta::lists('condicion', 'id')->all();
        
        $scripts = array('factura_create_form.js');

        return view('facturas.create')
            ->with('scripts', $scripts)
            ->with('options', $options);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $input['created_user_id'] = $request->user()->id;

        $factura = new Factura();
        $resultado = $factura->crearFormaManual($input);

        if (get_class($resultado) == 'Hanzo\Models\Factura')
        {
            Flash::success('La factura fue generada correctamente.');
        }
        elseif (get_class($resultado) == 'Hanzo\Models\FacturaFallida')
        {
            $mensaje = 'Factura Sucursal ID '. $resultado->sucursal_id. ' Error ->';

            if ($resultado->mtxca_errores)
                $mensaje.= $resultado->mtxca_errores;

            if ($resultado->mtxca_observaciones)
                $mensaje.= $resultado->mtxca_observaciones;
            
            Flash::error($mensaje);
        
            return redirect(route('facturas.index'));
        }

        return redirect(route('facturas.show', array('id' => $factura->id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factura = $this->facturaRepository->find($id);

        if (empty($factura))
        {
            Flash::error('Factura no encontrado');

            return redirect(route('facturas.index'));
        }

        $options = array();
        $options['copias'] = array('original', 'duplicado', 'triplicado');
        
        return view('facturas.show')
                    ->with('options', $options)
                    ->with('factura', $factura);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
