<?php

namespace Hanzo\Http\Controllers;

use Illuminate\Http\Request;
use Hanzo\Http\Requests;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Models\Producto;
use Hanzo\Models\PoliticaProducto;

class PoliticaProductoController extends Controller
{
    public function getProductosBySucursalAutocomplete(Request $request)
    {
        $productos = PoliticaProducto::select('productos.id', 'productos.nombre')
                                ->join('productos', 'politicas_producto.producto_id', '=', 'productos.id')
                                ->where('politicas_producto.sucursal_id', $request->get('sucursal'))
                                ->where('productos.nombre', 'ilike', "%" . $request->get('term') . "%")
                                ->limit(10)
                                ->get();

        echo json_encode($productos);
    }
    
    public function getProductosBySucursal(Request $request)
    {
        $productos = PoliticaProducto::select('productos.id', 'productos.nombre')
                                ->join('productos', 'politicas_producto.producto_id', '=', 'productos.id')
                                ->where('politicas_producto.sucursal_id', $request->get('sucursal'))
                                ->get();

        echo json_encode($productos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $options = array();

        $options['cliente'] = $request->get('cliente');
        $options['productos'] = ['' => ''] + Producto::orderBy('nombre', 'ASC')->lists('nombre', 'id')->all();
        
        return view('politicas_producto.create')->with('options', $options);    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
