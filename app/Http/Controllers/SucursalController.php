<?php

namespace Hanzo\Http\Controllers;

use DB;
use Flash;
use Hanzo\Http\Requests;
use Illuminate\Http\Request;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Models\Localidad;
use Hanzo\Models\EstadosCliente;
use Hanzo\Models\DomicilioCliente;
use Hanzo\Models\TipoDomicilioCliente;
use Hanzo\Models\Sucursal;
use Hanzo\Models\CondicionVenta;
use Hanzo\Http\Requests\CreateSucursalRequest;
use Hanzo\Http\Requests\UpdateDomicilioClienteRequest;
use Hanzo\Libraries\Repositories\SucursalRepository;

class SucursalController extends Controller
{
    private $sucursalRepository;
    
    function __construct(SucursalRepository $sucursalRepo)
    {
        $this->middleware('auth');
        $this->sucursalRepository = $sucursalRepo;
    }

    public function getSucursalesAutocomplete(Request $request)
    {
        $sucursales = Sucursal::select('sucursales.id', DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre) as nombre"))
                                ->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                                ->where(DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre)"), 'ilike', "%" . $request->get('term') . "%")
                                ->limit(10)
                                ->get();

        echo json_encode($sucursales);
    }

    public function getSucursalesActivasByNroClienteAutocomplete(Request $request)
    {
        $sucursales = Sucursal::select('sucursales.id', DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre) as nombre"))
                                ->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                                #->where('clientes.nro_cliente', 'ilike', "%" . $request->get('term') . "%")
                                ->where('sucursales.nro_cliente', $request->get('term'))
                                ->where('sucursales.estado_id', 1)
                                ->limit(10)
                                ->get();

        echo json_encode($sucursales);
    }

    public function getSucursalesActivasAutocomplete(Request $request)
    {
        $sucursales = Sucursal::select('sucursales.id', DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre) as nombre"))
                                ->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                                ->where(DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre)"), 'ilike', "%" . $request->get('term') . "%")
                                ->where('sucursales.estado_id', 1)
                                ->limit(10)
                                ->get();

        echo json_encode($sucursales);
    }

    public function getDatosClienteFactura(Request $request)
    {/*
        $sucursal = Sucursal::select('sucursales.id', DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre) as nombre"))
                                ->join('clientes', 'sucursales.cliente_id', '=', 'clientes.id')
                                ->where(DB::raw("CONCAT(clientes.razon_social, ' - ', sucursales.nombre)"), 'ilike', "%" . $request->get('term') . "%")
                                ->where('sucursales.id', $request->sucursal)
                                ->first();

        */
        $sucursal  = Sucursal::find($request->sucursal);
        $cliente   = $sucursal->cliente;
        $domicilio = $sucursal->domicilio_facturacion;

        $datos = array('razon_social'     => $cliente->razon_social,
                       'nombre_comercial' => $cliente->nombre_comercial,
                       'cuit'             => $cliente->cuit,
                       'estado'           => $cliente->estado->nombre,
                       'categoria_iva'    => $cliente->categoria_iva ? $cliente->categoria_iva->categoria : '',
                       'tipo_factura'     => $cliente->categoria_iva ? $cliente->categoria_iva->tipo_factura : '',
                       'nro_orden_compra' => $sucursal->nro_orden_compra,
                       'condicion_venta'  => $sucursal->condicion_venta->condicion,
                       'domicilios_facturacion' => $domicilio->calle .' '. $domicilio->nro .' - '. $domicilio->localidad->nombre .' - '. $domicilio->provincia->nombre
                       );

        echo json_encode($datos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sucursales = Sucursal::sucursal($request->get('sucursal'))->byEstado($request->get('estado'))->orderBy('id', 'ASC')->paginate();
        $options = array('estados' => ['' => ''] + EstadosCliente::lists('nombre', 'id')->all());
        $query = array();
        
        if ($request->get('sucursal') != '')
          $query['sucursal'] = $request->get('sucursal');

        if ($request->get('estado') != '')
          $query['estado'] = $request->get('estado');

        return view('sucursales.index')
            ->with('query', $query)
            ->with('options', $options)
            ->with('sucursales', $sucursales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $options = array();
        $cliente_id = $request->get('cliente');
        $tipo_cbz = TipoDomicilioCliente::getDomicilioByNombre('cbz');
        $tipo_ftc = TipoDomicilioCliente::getDomicilioByNombre('ftc');

        $options['cliente']                = $cliente_id;
        $options['estados']                = ['' => ''] + EstadosCliente::lists('nombre', 'id')->all();
        $options['condiciones_venta']      = ['' => ''] + CondicionVenta::lists('condicion', 'id')->all();
        $options['domicilios_cobro']       = ['' => ''] + DomicilioCliente::where('cliente_id', $cliente_id)->where('tipo_domicilio_cliente_id', $tipo_cbz->id)->lists('calle', 'id')->all();
        $options['domicilios_facturacion'] = ['' => ''] + DomicilioCliente::where('cliente_id', $cliente_id)->where('tipo_domicilio_cliente_id', $tipo_ftc->id)->lists('calle', 'id')->all();

        $scripts = array('sucursales_form.js');

        return view('sucursales.create')->with('options', $options)->with('scripts', $scripts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSucursalRequest $request)
    {
        $input = $request->all();
        $input['created_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $localidad = Localidad::where('id', $input['localidad_id'])->first();
        $input['provincia_id'] = $localidad->provincia_id;
        
        $sucursal = $this->sucursalRepository->create($input);

        Flash::success('La sucursal fue guardada correctamente.');

        return redirect(route('clientes.show', array('id' => $sucursal->cliente_id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sucursal = Sucursal::find($id);

        if(empty($sucursal))
        {
            Flash::error('Sucursal no encontrada');

            return redirect(route('sucursales.index'));
        }

        return view('sucursales.show')->with('sucursal', $sucursal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if ( empty($sucursal) )
        {
            Flash::error('Sucursal no encontrada');

            return redirect(route('clientes.index'));
        }
        
        $tipo_cbz = TipoDomicilioCliente::getDomicilioByNombre('cbz');
        $tipo_ftc = TipoDomicilioCliente::getDomicilioByNombre('ftc');

        $options = array();
        $options['cliente']                = $sucursal->cliente_id;
        $options['estados']                = ['' => ''] + EstadosCliente::lists('nombre', 'id')->all();
        $options['domicilios_cobro']       = ['' => ''] + DomicilioCliente::where('cliente_id', $options['cliente'])->where('tipo_domicilio_cliente_id', $tipo_cbz->id)->lists('calle', 'id')->all();
        $options['domicilios_facturacion'] = ['' => ''] + DomicilioCliente::where('cliente_id', $options['cliente'])->where('tipo_domicilio_cliente_id', $tipo_ftc->id)->lists('calle', 'id')->all();

        $scripts = array('sucursales_form.js');

        return view('sucursales.edit')
                    ->with('sucursal', $sucursal)
                    ->with('options', $options)
                    ->with('scripts', $scripts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateDomicilioClienteRequest $request)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if(empty($sucursal))
        {
            Flash::error('Sucursal no encontrada');

            return redirect(route('clientes.index'));
        }

        $input = $request->all();
        $input['updated_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $this->sucursalRepository->updateRich($input, $id);

        Flash::success('Sucursal actualizada correctamente.');

        return redirect(route('clientes.show', array('id' => $sucursal->cliente_id)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
