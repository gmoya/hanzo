<?php namespace Hanzo\Http\Controllers;

use Hanzo\Http\Requests;
use Illuminate\Http\Request;
use Hanzo\Models\Cliente;
use Hanzo\Models\EstadosCliente;
use Hanzo\Models\CategoriaIva;
use Hanzo\Models\CondicionVenta;
use Hanzo\Models\Alicuota;
use Hanzo\Models\TipoCliente;
use Hanzo\Http\Requests\CreateClienteRequest;
use Hanzo\Http\Requests\UpdateClienteRequest;
use Hanzo\Libraries\Repositories\ClienteRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClienteController extends AppBaseController
{

	/** @var  ClienteRepository */
	private $clienteRepository;

	function __construct(ClienteRepository $clienteRepo)
	{
        $this->middleware('auth');
		$this->clienteRepository = $clienteRepo;
	}

	/**
	 * Display a listing of the Cliente.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $clientes = Cliente::cliente($request->get('cliente'))->byEstado($request->get('estado'))->orderBy('id', 'ASC')->paginate();
        $options = array('estados' => ['' => ''] + EstadosCliente::lists('nombre', 'id')->all());
        $query = array();
        
        if ($request->get('cliente') != '')
		  $query['cliente'] = $request->get('cliente');

        if ($request->get('estado') != '')
          $query['estado'] = $request->get('estado');

        return view('clientes.index')
            ->with('clientes', $clientes)
            ->with('options', $options)
			->with('query', $query);
	}

	/**
	 * Show the form for creating a new Cliente.
	 *
	 * @return Response
	 */
	public function create()
	{
        $options = array();
        $options['estados'] = ['' => ''] + EstadosCliente::lists('nombre', 'id')->all();
        $options['categorias_iva'] = ['' => ''] + CategoriaIva::lists('categoria', 'id')->all();
        $options['condiciones_venta'] = ['' => ''] + CondicionVenta::lists('condicion', 'id')->all();
        $options['tipos_cliente'] = ['' => ''] + TipoCliente::lists('nombre', 'id')->all();
        $options['ingresos_brutos'] = ['' => ''] + Alicuota::where('codigo', '>', '20')->lists('alicuota', 'id')->all();

        return view('clientes.create')->with('options', $options);
	}

	/**
	 * Store a newly created Cliente in storage.
	 *
	 * @param CreateClienteRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateClienteRequest $request)
	{
        $input = $request->all();
        $input['created_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $cliente = $this->clienteRepository->create($input);

        Flash::success('El Cliente fue guardado correctamente.');

        return redirect(route('clientes.show', array('id' => $cliente->id)));
	}

	/**
	 * Display the specified Cliente.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$cliente = $this->clienteRepository->find($id);

		if(empty($cliente))
		{
			Flash::error('Cliente no encontrado');

			return redirect(route('clientes.index'));
		}

        $scripts = array('clientes_show.js');

		return view('clientes.show')
                    ->with('scripts', $scripts)
                    ->with('cliente', $cliente);
	}

	/**
	 * Show the form for editing the specified Cliente.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$cliente = $this->clienteRepository->find($id);

		if(empty($cliente))
		{
			Flash::error('Cliente no encontrado');

			return redirect(route('clientes.index'));
		}

        $options = array();
        $options['estados'] = ['' => ''] + EstadosCliente::lists('nombre', 'id')->all();
        $options['categorias_iva'] = ['' => ''] + CategoriaIva::lists('categoria', 'id')->all();
        $options['condiciones_venta'] = ['' => ''] + CondicionVenta::lists('condicion', 'id')->all();
        $options['ingresos_brutos'] = ['' => ''] + Alicuota::where('codigo', '>', '20')->lists('alicuota', 'id')->all();
        $options['tipos_cliente'] = ['' => ''] + TipoCliente::lists('nombre', 'id')->all();
        
        return view('clientes.edit')->with('cliente', $cliente)->with('options', $options);
	}

	/**
	 * Update the specified Cliente in storage.
	 *
	 * @param  int              $id
	 * @param UpdateClienteRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateClienteRequest $request)
	{
		$cliente = $this->clienteRepository->find($id);

		if(empty($cliente))
		{
			Flash::error('Cliente no encontrado');

			return redirect(route('clientes.index'));
		}

        $input = $request->all();
        $input['updated_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

		$cliente = $this->clienteRepository->updateRich($input, $id);

		Flash::success('Cliente actualizado correctamente.');

        return redirect(route('clientes.show', array('id' => $id)));
	}

	/**
	 * Remove the specified Cliente from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$cliente = $this->clienteRepository->find($id);

		if(empty($cliente))
		{
			Flash::error('Cliente no encontrado');

			return redirect(route('clientes.index'));
		}

		$this->clienteRepository->delete($id);

		Flash::success('Cliente eliminado correctamente.');

		return redirect(route('clientes.index'));
	}
}