<?php

namespace Hanzo\Http\Controllers;

use Flash;
use Illuminate\Http\Request;
use Hanzo\Models\DomicilioCliente;
use Hanzo\Models\TipoDomicilioCliente;
use Hanzo\Models\Provincia;
use Hanzo\Models\Localidad;
use Hanzo\Http\Requests;
use Hanzo\Http\Requests\CreateDomicilioClienteRequest;
use Hanzo\Http\Requests\UpdateDomicilioClienteRequest;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Libraries\Repositories\DomicilioClienteRepository;

class DomicilioClienteController extends Controller
{
    private $domicilioClienteRepository;
    
    function __construct(DomicilioClienteRepository $domicilioClienteRepo)
    {
        $this->middleware('auth');
        $this->domicilioClienteRepository = $domicilioClienteRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $options = array();

        $tipo_domicilio = TipoDomicilioCliente::getDomicilioByNombre($request->get('tipo'));

        $options['cliente'] = $request->get('cliente');
        $options['tipo'] = $tipo_domicilio->nombre;
        $options['tipo_id'] = $tipo_domicilio->id;

        $scripts = array('domicilios_cliente_form.js');

        return view('domicilios_cliente.create')->with('options', $options)->with('scripts', $scripts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDomicilioClienteRequest $request)
    {
        $input = $request->all();
        $input['created_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $localidad = Localidad::where('id', $input['localidad_id'])->first();
        $input['provincia_id'] = $localidad->provincia_id;
        
        $domicilio = $this->domicilioClienteRepository->create($input);

        Flash::success('El domicilio fue guardado correctamente.');

        return redirect(route('clientes.show', array('id' => $domicilio->cliente_id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $domicilio = $this->domicilioClienteRepository->find($id);

        if ( empty($domicilio) )
        {
            Flash::error('Domicilio no encontrado');

            return redirect(route('clientes.index'));
        }

        $options = array();
        $options['tipo'] = $domicilio->tipo_domicilio->nombre;

        $scripts = array('domicilios_cliente_form.js');

        return view('domicilios_cliente.edit')
                    ->with('domicilio', $domicilio)
                    ->with('options', $options)
                    ->with('scripts', $scripts);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateDomicilioClienteRequest $request)
    {
        $domicilio = $this->domicilioClienteRepository->find($id);

        if(empty($domicilio))
        {
            Flash::error('Domicilio no encontrado');

            return redirect(route('clientes.index'));
        }

        $input = $request->all();
        $input['updated_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $this->domicilioClienteRepository->updateRich($input, $id);

        Flash::success('Domicilio actualizado correctamente.');

        return redirect(route('clientes.show', array('id' => $domicilio->cliente_id)));    //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
