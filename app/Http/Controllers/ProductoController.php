<?php

namespace Hanzo\Http\Controllers;

use Illuminate\Http\Request;
use Hanzo\Http\Requests;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Models\Producto;
use Hanzo\Models\Alicuota;

use Hanzo\Libraries\Repositories\ProductoRepository;
use Hanzo\Http\Requests\CreateProductoRequest;
use Hanzo\Http\Requests\UpdateProductoRequest;
use Flash;

class ProductoController extends Controller
{
    /** @var  ProductoRepository */
    private $productoRepository;

    function __construct(ProductoRepository $productoRepo)
    {
        $this->middleware('auth');
        $this->productoRepository = $productoRepo;
    }

    public function getProductosAutocomplete(Request $request)
    {
        $productos = Producto::select('productos.id', 'productos.nombre', 'alicuotas.porcentaje')
                                ->join('alicuotas', 'productos.alicuota_id', '=', 'alicuotas.id')
                                ->where('productos.nombre', 'ilike', "%" . $request->get('term') . "%")
                                ->limit(10)
                                ->get();

        echo json_encode($productos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productos = Producto::producto($request->get('producto'))->orderBy('id', 'DESC')->paginate();
        $query = array();
        
        if ($request->get('producto') != '')
          $query['producto'] = $request->get('producto');

        return view('productos.index')
            ->with('query', $query)
            ->with('productos', $productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $options = array();
        $options['alicuotas'] = ['' => ''] + Alicuota::where('codigo', '<', '22')->lists('alicuota', 'id')->all();

        return view('productos.create')->with('options', $options);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductoRequest $request)
    {
        $input = $request->all();
        $input['created_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $cliente = $this->productoRepository->create($input);

        Flash::success('El Producto fue guardado correctamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = $this->productoRepository->find($id);

        if(empty($producto))
        {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        $options = array();
        $options['alicuotas'] = ['' => ''] + Alicuota::where('codigo', '<', '22')->lists('alicuota', 'id')->all();
        
        return view('productos.edit')->with('producto', $producto)->with('options', $options);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateProductoRequest $request)
    {
        $producto = $this->productoRepository->find($id);

        if(empty($producto))
        {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        $input = $request->all();
        $input['updated_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $producto = $this->productoRepository->updateRich($input, $id);

        Flash::success('Producto actualizado correctamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = $this->productoRepository->find($id);

        if(empty($producto))
        {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        $this->productoRepository->delete($id);

        Flash::success('Producto eliminado correctamente.');

        return redirect(route('productos.index'));
    }
}
