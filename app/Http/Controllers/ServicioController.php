<?php

namespace Hanzo\Http\Controllers;

use Flash;
use Response;

use Illuminate\Http\Request;

use Hanzo\Libraries\Repositories\ServicioRepository;

use Hanzo\Http\Requests;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Http\Requests\CreateServicioRequest;
use Hanzo\Http\Requests\UpdateServicioRequest;

use Hanzo\Models\Servicio;
use Hanzo\Models\Sucursal;


class ServicioController extends Controller
{
    private $servicioRepository;

    function __construct(ServicioRepository $servicioRepo)
    {
        $this->middleware('auth');
        $this->servicioRepository = $servicioRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $servicios = Servicio::sucursalCliente($request->get('sucursal_cliente'))->orderBy('servicios.id', 'DESC')->paginate();
        $query = array();

        if ($request->get('sucursal_cliente') != '')
          $query['sucursal_cliente'] = $request->get('sucursal_cliente');

        return view('servicios.index')
            ->with('query', $query)
            ->with('servicios', $servicios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $options = array();
        $scripts = array('servicios_form.js');

        return view('servicios.create')
                    ->with('options', $options)
                    ->with('scripts', $scripts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateServicioRequest $request)
    {
        $input = $request->all();

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }
        
        $sucursal = Sucursal::where('id', $input['sucursal_id'])->first();

        $input['cliente_id'] = $sucursal->cliente_id;
        $input['estado_id'] = 1;
        $input['created_user_id'] = $request->user()->id;

        $servicio = $this->servicioRepository->create($input);

        Flash::success('El Servicio fue guardado correctamente.');

        return redirect(route( isset($input['save_add']) ? 'servicios.create' : 'servicios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = $this->servicioRepository->find($id);

        if(empty($servicio))
        {
            Flash::error('Servicio no encontrado');

            return redirect(route('servicios.index'));
        }

        $scripts = array('servicios_form.js');

        return view('servicios.edit')
                    ->with('scripts', $scripts)
                    ->with('servicio', $servicio)
                    ->with('sucursal', $servicio->sucursal)
                    ->with('producto', $servicio->producto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateServicioRequest $request)
    {
        $servicio = $this->servicioRepository->find($id);

        if ( empty($servicio) )
        {
            Flash::error('Servicio no encontrado');

            return redirect(route('servicios.index'));
        }

        $input = $request->all();
        $input['updated_user_id'] = $request->user()->id;

        foreach ($input as $clave => $valor) 
        {
            if (trim($valor) == '')
                unset($input[$clave]);
        }

        $this->servicioRepository->updateRich($input, $id);

        Flash::success('Servicio actualizado correctamente.');

        return redirect(route('servicios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
