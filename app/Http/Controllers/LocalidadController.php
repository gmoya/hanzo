<?php

namespace Hanzo\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Hanzo\Models\Provincia;
use Hanzo\Models\Localidad;
use Hanzo\Http\Requests;
use Hanzo\Http\Controllers\Controller;

class LocalidadController extends Controller
{
    public function getLocalidadesAutocomplete(Request $request)
    {
        $localidades = Localidad::select('localidades.id', DB::raw("CONCAT(provincias.nombre, ' - ', localidades.nombre) as nombre"))
                                ->join('provincias', 'localidades.provincia_id', '=', 'provincias.id')
                                ->where(DB::raw("CONCAT(provincias.nombre, ' - ', localidades.nombre)"), 'ilike', "%" . $request->get('term') . "%")
                                ->limit(10)
                                ->get();

        echo json_encode($localidades);
    }
}
