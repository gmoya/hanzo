<?php

namespace Hanzo\Http\Controllers;

use Illuminate\Http\Request;
use Hanzo\Http\Requests;
use Hanzo\Http\Controllers\Controller;
use Hanzo\Models\PoliticaPrecio;

class PoliticaPrecioController extends Controller
{
    public function getPrecioForItem(Request $request)
    {
        $cantidad    = $request->get('cantidad');
        $tipo_unidad = $request->get('tipo_unidad');
        $cantidad = ( $tipo_unidad == 2 ? (int)($cantidad / 1000) : $cantidad );

        $precio = PoliticaPrecio::select('politicas_precios.precio')
                                ->join('politicas_producto', 'politicas_precios.politicas_producto_id', '=', 'politicas_producto.id')
                                ->where('politicas_producto.sucursal_id', $request->get('sucursal'))
                                ->where('politicas_precios.producto_id', $request->get('producto'))
                                ->where('politicas_precios.cantidad_hasta', '>=', $cantidad)
                                ->where('politicas_precios.tipo_cantidad_id', $tipo_unidad)
                                ->orderBy('cantidad_hasta', 'ASC')
                                ->limit(1)
                                ->first();

        echo json_encode($precio);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
