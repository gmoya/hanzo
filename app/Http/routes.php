<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
 
// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

Route::get('/', 'ClienteController@index');
#Route::get('/', function () {
#    return view('app');
#});

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function ()
{
	Route::group(['prefix' => ''], function ()
	{
        require Config::get('generator.path_api_routes');
	});
});

Route::get('clientes/{id}/delete', [
    'as' => 'clientes.delete',
    'uses' => 'ClienteController@destroy',
]);
Route::resource('clientes', 'ClienteController');

Route::get('facturas/facturar', [
            'as'   => 'facturas.facturar',
            'uses' => 'FacturaController@facturar'
]);
Route::post('facturas/facturar', [
            'as'   => 'facturas.facturar',
            'uses' => 'FacturaController@facturar'
]);

Route::get('facturas/imprimir', 'FacturaController@imprimir');
Route::post('facturas/imprimir', [
            'as'   => 'facturas.imprimir',
            'uses' => 'FacturaController@imprimir'
]);

Route::post('facturas/getPendientesFacturacion', 'FacturaController@getPendientesFacturacion');
Route::post('facturas/facturarBySucursal', 'FacturaController@facturarBySucursal');
Route::get('facturas/facturarAfip', 'FacturaController@facturarAfip');

Route::get('facturas/facturacion', [
            'as'   => 'facturas.facturacion',
            'uses' => 'FacturaController@facturacion'
]);
Route::resource('facturas', 'FacturaController');

Route::resource('servicios', 'ServicioController');

Route::get('localidad/getLocalidadesAutocomplete', 'LocalidadController@getLocalidadesAutocomplete');

Route::resource('domicilios_cliente', 'DomicilioClienteController');

Route::get('sucursales/getDatosClienteFactura', 'SucursalController@getDatosClienteFactura');
Route::get('sucursales/getSucursalesAutocomplete', 'SucursalController@getSucursalesAutocomplete');
Route::get('sucursales/getSucursalesActivasAutocomplete', 'SucursalController@getSucursalesActivasAutocomplete');
Route::get('sucursales/getSucursalesActivasByNroClienteAutocomplete', 'SucursalController@getSucursalesActivasByNroClienteAutocomplete');
Route::resource('sucursales', 'SucursalController');


Route::get('productos/getProductosAutocomplete', 'ProductoController@getProductosAutocomplete');
Route::resource('productos', 'ProductoController');

Route::get('productos/{id}/delete', [
    'as' => 'productos.delete',
    'uses' => 'ProductoController@destroy',
]);

Route::get('politicas_producto/getProductosBySucursal', 'PoliticaProductoController@getProductosBySucursal');
Route::get('politicas_producto/getProductosBySucursalAutocomplete', 'PoliticaProductoController@getProductosBySucursalAutocomplete');
Route::resource('politicas_producto', 'PoliticaProductoController');

Route::get('politicas_producto/{id}/delete', [
    'as' => 'politicas_producto.delete',
    'uses' => 'PoliticaProductoController@destroy',
]);

Route::get('politicas_precios/getPrecioForItem', 'PoliticaPrecioController@getPrecioForItem');

/*
Route::get('clientes/{id}/delete', [
    'as' => 'clientes.delete',
    'uses' => 'ClienteController@destroy',
]);
*/