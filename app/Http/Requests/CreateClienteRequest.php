<?php namespace Hanzo\Http\Requests;

use Hanzo\Http\Requests\Request;
use Hanzo\Models\Cliente;

class CreateClienteRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return Cliente::$rules;
	}

}
