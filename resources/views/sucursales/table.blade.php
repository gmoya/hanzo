<table class="table">
    <thead>
        <th>Id</th>
        <th>Sucursal</th>
        <th>Cliente</th>
        <th>Estado</th>
        <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($sucursales as $sucursal)
        <tr>
            <td>{!! $sucursal->id !!}</td>
            <td>{!! $sucursal->nombre !!}</td>
            <td>{!! '[' . $sucursal->cliente->id . '] ' . $sucursal->cliente->razon_social !!}</td>
            <td>{!! $sucursal->estado->nombre !!}</td>
            <td class="col-xs-2">
                <a class="action-list" href="{!! route('sucursales.show', [$sucursal->id]) !!}"><i class="glyphicon glyphicon-list-alt"> </i></a>
                <a class="action-list" href="{!! route('sucursales.edit', [$sucursal->id]) !!}"><i class="glyphicon glyphicon-edit"> </i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
