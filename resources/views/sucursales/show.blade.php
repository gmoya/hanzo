@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 lead">
                            Sucursal <a class="btn btn-default pull-right" href="{!! route('sucursales.edit', [$sucursal->id]) !!}"><i class="glyphicon glyphicon-pencil"> </i> Editar</a>
                            <hr>
                        </div>
                            
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="img-circle avatar avatar-original" style="-webkit-user-select:none; display:block; margin:auto;" src="http://robohash.org/sitsequiquia.png?size=120x120">
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="only-bottom-margin">{!! $sucursal->razon_social !!}</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="text-muted">Nro. Cliente:</span> {!! $sucursal->nro_cliente !!}<br>
                                    <span class="text-muted">Razón social:</span> {!! $sucursal->razon_social !!}<br>
                                    <span class="text-muted">Nombre Comercial:</span> {!! $sucursal->nombre_comercial !!}<br>
                                    <span class="text-muted">CUIT:</span> {!! $sucursal->cuit !!}<br>
                                    <span class="text-muted">Estado:</span> {!! $sucursal->estado->nombre !!}<br>
                                    <span class="text-muted">Vendedor:</span> <!-- $sucursal->vendedor->persona->nombre . ' ' . $sucursal->vendedor->persona->apellido !!}--><br>
                                    <span class="text-muted">Categoría IVA:</span> {!! $sucursal->categoria_iva ? $sucursal->categoria_iva->categoria : '' !!}<br>
                                    <span class="text-muted">Condición de Venta:</span> {!! $sucursal->condicion_venta ? $sucursal->condicion_venta->condicion : '' !!}<br>
                                    <span class="text-muted">IIBB:</span> {!! $sucursal->ingresos_brutos ? $sucursal->ingresos_brutos->alicuota : '' !!}<br>
                                    <span class="text-muted">IIBB CABA:</span> {!! $sucursal->ingresos_brutos_bsas ? $sucursal->ingresos_brutos_bsas->alicuota : '' !!}<br>
                                    <span class="text-muted">Fecha Expiracion:</span> {!! $sucursal->fecha_expiracion !!}<br>
                                    <span class="text-muted">Nro Orden Compra:</span> {!! $sucursal->nro_orden_compra !!}<br>
                                </div>
                                <div>
                                    <!--<div class="activity-mini">
                                        <i class="glyphicon glyphicon-comment text-muted"></i> 500
                                    </div>
                                    <div class="activity-mini">
                                        <i class="glyphicon glyphicon-thumbs-up text-muted"></i> 1500
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="col-lg-6">
    <!--        include('sucursales.show_facturacion')
            include('sucursales.show_cobranza')
        -->
            @include('sucursales.politicas')
        </div>

    </div>
</div>
@endsection
