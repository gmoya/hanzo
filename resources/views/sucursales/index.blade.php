@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Sucursales</h1>
            <a class="btn btn-default pull-right" style="margin-top: 25px; color:rgb(0, 112, 210)" href="{!! route('sucursales.create') !!}">Nuevo</a>
        </div>

        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'sucursales.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) !!}
                <div class="form-group">
                    {!! Form::text('sucursal', null, ['class' => 'form-control', 'placeholder' => 'ID, Sucursal, Razón Social']) !!}
                    {!! Form::select('estado', $options['estados'], null, ['class' => 'form-control']) !!}
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"> </span></button>
            {!! Form::close() !!}
        </div>

        <div class="row">
            @if($sucursales->isEmpty())
                <div class="well text-center">No se encotraron sucursales.</div>
            @else
                @include('sucursales.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $sucursales])

        <div class="row">
            <p>Resultados {{ $sucursales->total() }} sucursales</p>
        </div>

    </div>
@endsection