@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($sucursal, ['route' => ['sucursales.update', $sucursal->id], 'method' => 'patch']) !!}

        @include('sucursales.fields')

    {!! Form::close() !!}
</div>
@endsection