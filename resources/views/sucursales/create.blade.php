@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'sucursales.store']) !!}

        @include('sucursales.fields')

    {!! Form::close() !!}
</div>
@endsection
