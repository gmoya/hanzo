<div class="col-sm-6 col-lg-6 form-horizontal">

    @if (!isset($sucursal))
        {!! Form::hidden('cliente_id', $options['cliente'], ['class' => 'form-control']) !!}
    @endif
    
    {!! Form::hidden('localidad_id', null, ['id' => 'localidad_id', 'class' => 'form-control']) !!}

    <div class="form-group">
        {!! Form::label('nombre', 'Nombre', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nro_ceamse', 'Nro. Ceamse', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nro_ceamse', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('calle', 'Calle', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('calle', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nro', 'Nro', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-3">
           {!! Form::text('nro', null, ['class' => 'form-control']) !!}
        </div>
        {!! Form::label('codigo_postal', 'Código Postal', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-3">
           {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('entre_calle_a', 'Entre las calles', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-4">
           {!! Form::text('entre_calle_a', null, ['class' => 'form-control']) !!}
        </div>
       
        {!! Form::label('entre_calle_b', 'Y', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-4">
           {!! Form::text('entre_calle_b', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('localidad', 'Localidad', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            @if ( isset ($sucursal) )
                {!! Form::text('localidad', $sucursal->provincia->nombre . ' - ' . $sucursal->localidad->nombre, ['id' => 'localidad', 'class' => 'form-control']) !!}
            @else
                {!! Form::text('localidad', null, ['id' => 'localidad', 'class' => 'form-control']) !!}
            @endif
        </div>
    </div>
    
    <div class="form-group">
        {!! Form::label('hoja_filcar', 'Hoja Filcar', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('hoja_filcar', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('chofer_id', 'Chofer', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('chofer_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('condicion_venta_id', 'Condición de Venta', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('condicion_venta_id', $options['condiciones_venta'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('vendedor_id', 'Vendedor', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('vendedor_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('estado_id', 'Estado', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('estado_id', $options['estados'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('telefono', 'Telefono', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('contacto', 'Contacto', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('contacto', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('horario_atencion', 'Horario de atención', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('horario_atencion', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('recorrido_id', 'Recorrido', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('recorrido_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('domicilio_factura_id', 'Domicilio de Facturaci&oacute;n', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('domicilio_factura_id', $options['domicilios_facturacion'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('domicilio_cobro_id', 'Domicilio de Cobro', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('domicilio_cobro_id', $options['domicilios_cobro'], null, ['class' => 'form-control']) !!}
        </div>
    </div>    

    <div class="form-group">
        {!! Form::label('cobrador_id', 'Cobrador', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('cobrador_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group text-right">
        @if ( isset ($sucursal) )
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('sucursales.show', ['id' => $sucursal->id]) !!}">
        @else
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('clientes.show', ['cliente' => $options['cliente']]) !!}">Cancelar</a>
        @endif
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>