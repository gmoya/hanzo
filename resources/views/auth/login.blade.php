@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div id="logo_wrapper" class="standard_logo_wrapper mb24">
                    <h1 style="height: 100%; display: table-cell; vertical-align: bottom;">
                        <img id="logo" class="standard_logo" src="/assets/images/logo_hugo.jpg" alt="Hugo Contenedores" border="0" name="logo">
                    </h1>
                </div>
                <div class="panel panel-default">
                    <!--<div class="panel-heading">Login</div>-->
                    <div class="panel-body">
                        {!! Form::open(['route' => 'auth/login', 'class' => 'form']) !!}
                            <div class="form-group">
                                <label>Email</label>
                                {!! Form::email('email', '', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                {!! Form::password('password', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="checkbox">
                                <label><input name="remember" type="checkbox"> Recordarme</label>
                            </div>
                            <div>                            
                                {!! Form::submit('Ingresar',['class' => 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection