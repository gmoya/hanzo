@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($cliente, ['route' => ['clientes.update', $cliente->id], 'method' => 'patch']) !!}

        @include('clientes.fields')

    {!! Form::close() !!}
</div>
@endsection
