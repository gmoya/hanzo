<div class="box panel-default">
    <div class="panel-body">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12 lead">
                    Pol&iacute;tica de Precios 
                    <a class="btn btn-default pull-right small-font" href="{!! route('politicas_producto.create', ['cliente' => $cliente->id]) !!}">
                        <i class="glyphicon glyphicon-plus"> </i> Agregar
                    </a>
                </div>
            </div>
        </div>

        <div class="box-content">
        @if ($cliente->politicas_productos->isEmpty())
            <div class="text-center">No se ha cargado la pol&iacute;tica de precios.</div>
        @else
            <table class="table bootstrap-datatable datatable small-font">
                <tbody>
                    @foreach ($cliente->politicas_productos as $politica_producto)
                    <tr>
                        <td>
                            <div class="col-md-4">
                                <div class="col-md-8">{{ $politica_producto->producto->nombre }}</div>
                                <div class="col-md-2 pull-right">
                                    <a href="{!! route('domicilios_cliente.edit', ['domicilio' => $politica_producto->id]) !!}">
                                        <i class="glyphicon glyphicon-pencil"> </i>
                                    </a>
                                </div><br><br>
                                <div class="col-md-12"><b>Mensual:</b> {{ $politica_producto->cantidad_mensual }}</div>
                            </div>
                            <div class="col-md-8">
                                <table class="table bootstrap-datatable datatable small-font">
                                    <thead>
                                        <tr>
                                            <th>Hasta</th>
                                            <th>Precio</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($politica_producto->politicas_precios as $politica_precio)
                                        <tr>
                                            <td>
                                                {{ $politica_precio->cantidad_hasta }} 
                                                {{ $politica_precio->tipo_cantidad_id == 1 ?  'unidades' : 'toneladas' }}
                                            </td>
                                            <td>$ {{ $politica_precio->precio }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
        </div>
    </div>
</div>