<div class="box panel-default">
    <div class="panel-body">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12 lead">
                    Sucursales 
                    <a class="btn btn-default pull-right small-font" href="{!! route('sucursales.create', ['cliente' => $cliente->id]) !!}">
                        <i class="glyphicon glyphicon-plus"> </i> Agregar
                    </a>
                </div>
            </div>
        </div>

        <div class="box-content">
        @if($cliente->sucursales->isEmpty())
            <div class="text-center">No se encontraron sucursales.</div>
        @else
            <div id="tabs-sucursales">
                <ul class="col-lg-6 pre-scrollable">
                    @foreach ($cliente->sucursales as $sucursal)
                    <li>
                        <a href="#tabs-suc-{{ $sucursal->id }}" class="col-md-12">
                            {{ $sucursal->nombre }}
                        </a>
                    </li>
                    @endforeach
                </ul>

                <div class="col-lg-6">
                @foreach ($cliente->sucursales as $sucursal)
                    <div id="tabs-suc-{{ $sucursal->id }}" class="col-md-12">
                        <div class="row lead">
                            {{ $sucursal->nombre }} 
                            <a class="pull-right" href=" {{ route('sucursales.edit', ['sucursal' => $sucursal->id] ) }}">
                                <i class="glyphicon glyphicon-pencil"> </i>
                            </a> 
                        </div>

                        <div class="tabs-info-sucursal ui-tabs-horizontal">
                            <ul>
                                <li><a class="col-lg-12" href="#tabs-suc-{{ $sucursal->id }}-1">Políticas de Precios</a>
                                <li><a class="col-lg-12" href="#tabs-suc-{{ $sucursal->id }}-2">
                                    Servicios {{ $sucursal->getServiciosPendientes()->count() > 0 ? '(' . $sucursal->getServiciosPendientes()->count() . ')' : '' }}  
                                </a></li>
                                <li><a class="col-lg-12" href="#tabs-suc-{{ $sucursal->id }}-3">Facturas</a></li>
                            </ul>

                            <div id="tabs-suc-{{ $sucursal->id }}-1" class="col-lg-12 pre-scrollable">
                            @if ( $sucursal->politicas_productos->isEmpty() )
                                <div class="text-center">No se definieron políticas.</div>
                            @else
                                <table class="table bootstrap-datatable datatable small-font">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Mensual</th>
                                            <th>Hasta</th>
                                            <th>Precio</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($sucursal->politicas_productos as $politica_producto)
                                        @if ($politica_producto->politicas_precios->isEmpty())
                                        <tr>
                                            <th rowspan="{{ count($politica_producto->politicas_precios) }}">
                                                <div class="col-md-8">{{ $politica_producto->producto->nombre }}</div>
                                                <div class="col-md-2 pull-right">
                                                    <a href="{!! route('domicilios_cliente.edit', ['domicilio' => $politica_producto->id]) !!}">
                                                        <i class="glyphicon glyphicon-pencil"> </i>
                                                    </a>
                                                </div>
                                            </th>
                                            <td rowspan="{{ count($politica_producto->politicas_precios) }}">
                                                {{ $politica_producto->cantidad_mensual ? $politica_producto->cantidad_mensual : 0 }} uni.
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @else
                                        @foreach ($politica_producto->politicas_precios as $key => $politica_precio)
                                            <tr>
                                            @if ( $key == 0 )
                                                <th rowspan="{{ count($politica_producto->politicas_precios) }}">
                                                    <div class="col-md-8">{{ $politica_producto->producto->nombre }}</div>
                                                    <div class="col-md-2 pull-right">
                                                        <a href="{!! route('domicilios_cliente.edit', ['domicilio' => $politica_producto->id]) !!}">
                                                            <i class="glyphicon glyphicon-pencil"> </i>
                                                        </a>
                                                    </div>
                                                </th>
                                                <td rowspan="{{ count($politica_producto->politicas_precios) }}">
                                                    {{ $politica_producto->cantidad_mensual ? $politica_producto->cantidad_mensual : 0 }} uni.
                                                </td>
                                            @endif
                                                <td>
                                                    {{ $politica_precio->cantidad_hasta }} 
                                                    {{ $politica_precio->tipo_cantidad_id == 1 ?  'uni' : 'ton' }}
                                                </td>
                                                <td>$ {{ $politica_precio->precio }}</td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif

                            </div>

                            <!-- Servicios pendientes-->
                            <div id="tabs-suc-{{ $sucursal->id }}-2" class="col-lg-12 pre-scrollable">
                            @if ( $sucursal->getServiciosPendientes()->isEmpty() )
                                <div class="text-center">No se tiene servicios pendientes.</div>
                            @else<table class="table bootstrap-datatable datatable small-font">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Producto</th>
                                            <th>Contenido</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($sucursal->getServiciosPendientes() as $servicio)
                                        <tr>
                                            <td>{{ date('d-m-Y', strtotime($servicio->fecha)) }}</td>
                                            <td>{{ $servicio->producto->nombre }}</td>
                                            <td>{{ $servicio->contenedores }} cont./ {{ $servicio->kilos }} kg</td>
                                            <td>{!! $servicio->estado_id == 1 ? 'Pendiente' : ($servicio->estado_id == 2 ? 'Facturado' : 'ERR' ) !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                            </div>

                            <!-- Facturas pendientes
                            <div id="tabs-suc-{{ $sucursal->id }}-3" class="col-lg-12">
                                <table class="table bootstrap-datatable datatable small-font">
                                    <thead>
                                        <tr>
                                            <th>Numero</th>
                                            <th>Tipo</th>
                                            <th>Total</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($sucursal->facturas->sortByDesc('numero') as $factura)
                                        <tr>
                                            <td>{{ $factura->numero }}</td>
                                            <td>{{ $factura->letra }}</td>
                                            <td class="text-right">$ {!! number_format($factura->total, 2, '.', ',') !!}</td>
                                            <td>{{ date('d-m-Y', strtotime($factura->fecha)) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            -->
                        </div>

                    </div>
                @endforeach
                </div>
            </div>
        @endif
        </div>
    </div>
</div>