<table class="table">
    <thead>
        <th>Id</th>
		<th>Razon Social</th>
		<th>Cuit</th>
		<th>Estado</th>
        <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($clientes as $cliente)
        <tr>
            <td>{!! $cliente->id !!}</td>
			<td>{!! $cliente->razon_social !!}</td>
			<td>{!! $cliente->cuit !!}</td>
			<td>{!! $cliente->estado->nombre !!}</td>
            <td class="col-xs-2">
                <a class="action-list" href="{!! route('clientes.show', [$cliente->id]) !!}"><i class="glyphicon glyphicon-list-alt"> </i></a>
                <a class="action-list" href="{!! route('clientes.edit', [$cliente->id]) !!}"><i class="glyphicon glyphicon-edit"> </i></a>
                <a class="action-list" href="{!! route('clientes.delete', [$cliente->id]) !!}" onclick="return confirm('¿Está seguro que desea borrar este cliente?')"><i class="glyphicon glyphicon-remove"> </i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
