<div class="col-sm-6 col-lg-6 form-horizontal">

    <!-- Razon Social Field -->
    <div class="form-group">
        {!! Form::label('razon_social', 'Raz&oacute;n Social', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
    	   {!! Form::text('razon_social', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Nombre Comercial Field -->
    <div class="form-group">
        {!! Form::label('nombre_comercial', 'Nombre Comercial', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nombre_comercial', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <!-- Categoria Iva Id Field -->
        {!! Form::label('categoria_iva_id', 'Categor&iacute;a Iva', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('categoria_iva_id', $options['categorias_iva'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    
    <div class="form-group">    
        <!-- Cuit Field -->
        {!! Form::label('cuit', 'Cuit', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('cuit', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Ingreso Brutos Id Field -->
    <div class="form-group">
        {!! Form::label('ingreso_brutos_id', 'Ingresos Brutos', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
    	   {!! Form::select('ingreso_brutos_id', $options['ingresos_brutos'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Ingreso Brutos Bsas Id Field -->
    <div class="form-group">
        {!! Form::label('ingreso_brutos_bsas_id', 'Ingresos Brutos Bs As', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('ingreso_brutos_bsas_id', $options['ingresos_brutos'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Estado Id Field -->
    <div class="form-group">
        {!! Form::label('estado_id', 'Estado', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('estado_id', $options['estados'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Tipo Cliente Id Field -->
    <div class="form-group">
        {!! Form::label('tipo_cliente_id', 'Tipo Cliente', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('tipo_cliente_id', $options['tipos_cliente'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    
    <!-- Web Field -->
    <div class="form-group">
        {!! Form::label('web', 'Web', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('web', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group"> </div>
    
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('clientes.index') !!}">Cancelar</a>
    </div>
</div>