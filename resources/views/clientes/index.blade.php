@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Clientes</h1>
            <a class="btn btn-default pull-right" style="margin-top: 25px; color:rgb(0, 112, 210)" href="{!! route('clientes.create') !!}">Nuevo</a>
        </div>

        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'clientes.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) !!}
                <div class="form-group">
                    {!! Form::text('cliente', null, ['class' => 'form-control', 'placeholder' => 'ID, CUIT, Razón Social']) !!}
                    {!! Form::select('estado', $options['estados'], null, ['class' => 'form-control']) !!}
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"> </span></button>
            {!! Form::close() !!}
        </div>

        <div class="row">
            @if($clientes->isEmpty())
                <div class="well text-center">No se encotraron clientes.</div>
            @else
                @include('clientes.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $clientes])

        <div class="row">
            <p>Resultados {{ $clientes->total() }} clientes</p>
        </div>

    </div>
@endsection