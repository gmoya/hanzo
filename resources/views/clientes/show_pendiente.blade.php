<div class="box panel-default">
    <div class="panel-body">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12 lead">Pendiente</div>
            </div>
        </div>

        <div class="box-content small-font pre-scrollable">
        @if( $cliente->pendientes->isEmpty() )
            <div class="text-center">No se encontraron comprobantes pendientes de cobro.</div>
        @else
        <?php $acumulado = 0; ?>

            <table class="table bootstrap-datatable datatable small-font">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Nro. Comprobante</th>
                        <th>Tipo Comprobante</th>
                        <th>Punto de Venta</th>
                        <th class="text-right">Importe</th>
                        <th class="text-right">Acumulado</th>
                        <th class="text-right">Vencimiento</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-right" colspan="5">Total</th>
                        <th class="text-right">$ {{ number_format($cliente->getTotalPendientes(), 2, '.', '') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($cliente->pendientes as $pendiente)

                    @if( $pendiente->tipo_comprobante->codigo == 'NCRE' )
                        <?php $acumulado-= $pendiente->importe ?>
                    @else
                        <?php $acumulado+= $pendiente->importe ?>
                    @endif
                    
                    <tr>
                        <td>{{ date('d-m-Y', strtotime($pendiente->fecha)) }}</td>
                        <td>
                            {{ str_pad($pendiente->punto_venta->nro, 4, "0", STR_PAD_LEFT) . ' - ' . 
                               str_pad($pendiente->numero_comprobante, 8, "0", STR_PAD_LEFT) }}</td>
                        <td>{{ $pendiente->tipo_comprobante->nombre }}</td>
                        <td>{{ $pendiente->punto_venta->nombre}} </td>
                        <td class="text-right">$ {{ number_format($pendiente->importe, 2, '.', ' ') }}</td>
                        <td class="text-right">$ {{ number_format($acumulado, 2, '.', '') }}</td>
                        <td class="text-right">{{ date('d-m-Y', strtotime($pendiente->vencimiento)) }}</td>
                        <td>{{ $pendiente->getEstado() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endif

        </div>
    </div>
</div>