<div class="box panel-default">
    <div class="panel-body">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12 lead">
                    Domicilio de cobro 

                    @if( !$cliente->domicilio_cobro )
                    <a class="btn btn-default pull-right small-font" href="{!! route('domicilios_cliente.create', ['cliente' => $cliente->id, 'tipo' => 'cbz']) !!}">
                        <i class="glyphicon glyphicon-plus"> </i> Agregar
                    </a>
                    @else
                    <a class="btn btn-default pull-right small-font" href="{!! route('domicilios_cliente.edit', ['domicilio' => $cliente->domicilio_cobro->id]) !!}">
                        <i class="glyphicon glyphicon-pencil"> </i> Editar
                    </a>
                    @endif
                </div>
            </div>
        </div>

        <div class="box-content small-font pre-scrollable">
        @if( $cliente->domicilios_cobro->isEmpty() )
            <div class="text-center">No se encotraron domicilios de cobro.</div>
        @else
            <table class="table bootstrap-datatable datatable small-font">
                <thead>
                    <tr>
                        <th>Domicilio</th>
                        <th>Localidad</th>
                        <th>Teléfono</th>
                        <th></th>
                    </tr>
                </thead>   
                <tbody>
                    @foreach ($cliente->domicilios_cobro as $domicilio)
                    <tr>
                        <td>{{ $domicilio->calle }} {{ $domicilio->nro }}</td>
                        <td>{{ $domicilio->localidad->nombre }} - {{ $domicilio->provincia->nombre }}</td>
                        <td>{{ $domicilio->telefono }}</td>
                        <td>
                            <a class="pull-right" href="{!! route('domicilios_cliente.edit', ['domicilio' => $domicilio->id]) !!}">
                                <i class="glyphicon glyphicon-pencil"> </i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endif

        </div>
    </div>
</div>