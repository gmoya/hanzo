<div class="row">
    <div class="row">
        <h1 class="lead text-center">{!! $cliente->razon_social !!}</h1>
    </div>

    <div id="tabs" class="ui-tabs-horizontal">
        <ul>
            <li><a href="#tabs-1" class="col-md-12">Perfil del Cliente</a></li>
            <li><a href="#tabs-2" class="col-md-12">Sucursales</a></li>
            <li><a href="#tabs-3" class="col-md-12">Domicilios de Cobranza</a></li>
            <li><a href="#tabs-4" class="col-md-12">Domicilios de Facturación</a></li>
            <li><a href="#tabs-5" class="col-md-12">Pendiente de Cobro</a></li>
        </ul>
    
        <div id="tabs-1" class="row">
            <div class="col-md-12">
                <div class="panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 lead">
                                Perfil del Cliente <a class="btn btn-default pull-right" href="{!! route('clientes.edit', [$cliente->id]) !!}"><i class="glyphicon glyphicon-pencil"> </i> Editar</a>
                                <hr>
                            </div>
                                
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <img class="img-circle avatar avatar-original" style="-webkit-user-select:none; display:block; margin:auto;" src="/assets/images/perfil-azul.jpg">
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="text-muted">Razón social:</span> {!! $cliente->razon_social !!}<br>
                                        <span class="text-muted">Nombre Comercial:</span> {!! $cliente->nombre_comercial !!}<br>
                                        <span class="text-muted">CUIT:</span> {!! $cliente->cuit !!}<br>
                                        <span class="text-muted">Estado:</span> {!! $cliente->estado->nombre !!}<br>
                                        <span class="text-muted">Categoría IVA:</span> {!! $cliente->categoria_iva ? $cliente->categoria_iva->categoria : '' !!}<br>
                                        <span class="text-muted">IIBB:</span> {!! $cliente->ingresos_brutos ? $cliente->ingresos_brutos->alicuota : '' !!}<br>
                                        <span class="text-muted">IIBB CABA:</span> {!! $cliente->ingresos_brutos_bsas ? $cliente->ingresos_brutos_bsas->alicuota : '' !!}<br>
                                        <span class="text-muted">Fecha Expiracion:</span> {!! $cliente->fecha_expiracion !!}<br>
                                        <span class="text-muted">Rubro Empresario:</span> {!! $cliente->rubro_empresario->rubro !!}<br>
                                        <span class="text-muted">Cuota Mensual:</span> {!! $cliente->cuota_mensual !!}<br>
                                        <span class="text-muted">Tipo Cliente:</span> {!! $cliente->tipo_cliente->nombre !!}<br>
                                        <span class="text-muted">Web:</span> {!! $cliente->web !!}<br><br>
                                        <small class="text-muted">Creado: {!! $cliente->created_at !!}</small>
                                    </div>
                                    <div>
                                        <!--<div class="activity-mini">
                                            <i class="glyphicon glyphicon-comment text-muted"></i> 500
                                        </div>
                                        <div class="activity-mini">
                                            <i class="glyphicon glyphicon-thumbs-up text-muted"></i> 1500
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <div id="tabs-2" class="row">
            <div class="col-lg-12">
                @include('clientes.show_sucursales')
            </div>
        </div>

        <div id="tabs-3" class="row">
            <div class="col-lg-12">
                @include('clientes.show_cobranza')
            </div>
        </div>
    
        <div id="tabs-4" class="row">
            <div class="col-lg-12">
                @include('clientes.show_facturacion')
            </div>
        </div>

        <div id="tabs-5" class="row">
            <div class="col-lg-12">
                @include('clientes.show_pendiente')
            </div>
        </div>
    </div>

</div>