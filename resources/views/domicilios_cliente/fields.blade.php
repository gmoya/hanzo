<div class="col-sm-6 col-lg-6 form-horizontal">

    @if (!isset($domicilio))
        {!! Form::hidden('cliente_id', $options['cliente'], ['class' => 'form-control']) !!}
        {!! Form::hidden('tipo_domicilio_cliente_id', $options['tipo_id'], ['class' => 'form-control']) !!}
        {!! Form::hidden('tipo_domicilio', $options['tipo'], ['class' => 'form-control']) !!}
    @endif
    
    {!! Form::hidden('localidad_id', null, ['id' => 'localidad_id', 'class' => 'form-control']) !!}

    <div class="form-group">
        {!! Form::label('calle', 'Calle', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('calle', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nro', 'Nro', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-3">
           {!! Form::text('nro', null, ['class' => 'form-control']) !!}
        </div>
        {!! Form::label('codigo_postal', 'Código Postal', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-3">
           {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('entre_calle_a', 'Entre las calles', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-4">
           {!! Form::text('entre_calle_a', null, ['class' => 'form-control']) !!}
        </div>
       
        {!! Form::label('entre_calle_b', 'Y', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-4">
           {!! Form::text('entre_calle_b', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('localidad', 'Localidad', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            @if ( isset ($domicilio) )
                {!! Form::text('localidad', $domicilio->provincia->nombre . ' - ' . $domicilio->localidad->nombre, ['id' => 'localidad', 'class' => 'form-control']) !!}
            @else
                {!! Form::text('localidad', null, ['id' => 'localidad', 'class' => 'form-control']) !!}
            @endif
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('hoja_filcar', 'Hoja Filcar', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('hoja_filcar', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('telefono', 'Telefono', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('contacto', 'Contacto', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('contacto', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('horario_atencion', 'Horario de atención', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('horario_atencion', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>