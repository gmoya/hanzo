@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($domicilio, ['route' => ['domicilios_cliente.update', $domicilio->id], 'method' => 'patch']) !!}

        @include('domicilios_cliente.fields')

    {!! Form::close() !!}
</div>
@endsection
