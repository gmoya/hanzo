@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'domicilios_cliente.store']) !!}

        @include('domicilios_cliente.fields')

    {!! Form::close() !!}
</div>
@endsection
