@include('flash::message')

<div class="col-sm-6 col-lg-6 form-horizontal">

    {!! Form::hidden('sucursal_id', null, ['id' => 'sucursal_id', 'class' => 'form-control']) !!}
    {!! Form::hidden('sucursal_nombre', null, ['id' => 'sucursal_nombre', 'class' => 'form-control']) !!}
    {!! Form::hidden('producto_id', null, ['id' => 'producto_id', 'class' => 'form-control']) !!}
    {!! Form::hidden('save_add', null, ['id' => 'save_add', 'class' => 'form-control']) !!}

    <div class="form-group">
        {!! Form::label('sucursal', 'Sucursal', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            @if ( isset ($sucursal) )
                {!! Form::text('sucursal', $sucursal->cliente->razon_social . ' - ' . $sucursal->nombre, ['id' => 'sucursal', 'class' => 'form-control']) !!}
            @else
                {!! Form::text('sucursal', null, ['id' => 'sucursal', 'class' => 'form-control']) !!}
            @endif
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('fecha', 'Fecha', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('fecha', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('producto', 'Producto', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            @if ( isset ($producto) )
                {!! Form::text('producto', $producto->nombre, ['id' => 'producto', 'class' => 'form-control']) !!}
            @else
                {!! Form::text('producto', null, ['id' => 'producto', 'class' => 'form-control']) !!}
            @endif
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('contenedores', 'Contenedores', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('contenedores', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('kilos', 'Kilos', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('kilos', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('direccion', 'Dirección', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nro_manifiesto', 'Nro. Manifiesto', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nro_manifiesto', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nro_cr', 'Nro. CR', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nro_cr', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('observaciones', 'Observaciones', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a class="btn" style="color:rgb(0, 112, 210)" href="#" id="save_add_button">Guardar y agregar</a>
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('servicios.index') !!}">Cancelar</a>
    </div>
</div>