@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($servicio, ['route' => ['servicios.update', $servicio->id], 'method' => 'patch']) !!}

        @include('servicios.fields')

    {!! Form::close() !!}
</div>
@endsection
