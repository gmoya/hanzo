@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Servicios</h1>
            <a class="btn btn-primary pull-right" style="margin: 25px 5px 0;" href="{!! route('servicios.create') !!}">Nuevo</a>
        </div>

        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'servicios.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) !!}
                <div class="form-group">
                    {!! Form::text('sucursal_cliente', null, ['class' => 'form-control', 'placeholder' => 'Sucursal, Razón Social']) !!}
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"> </span></button>
            {!! Form::close() !!}
        </div>

        <div class="row">
            @if($servicios->isEmpty())
                <div class="well text-center">No se encotraron servicios.</div>
            @else
                @include('servicios.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $servicios])

        <div class="row">
            <p>Resultados {{ $servicios->total() }} servicios</p>
        </div>

    </div>
@endsection