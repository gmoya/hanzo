<table class="table">
    <thead>
        <th>Id</th>
        <th>Fecha</th>
        <th>Raz&oacute;n</th>
        <th>Producto</th>
        <th>Contenedores</th>
        <th>Kilos</th>
        <th>Estado</th>
        <th width="25px">Action</th>
    </thead>
    <tbody>
    @foreach($servicios as $servicio)
        <tr>
            <td>{!! $servicio->id !!}</td>
            <td>{!! date('d-m-Y', strtotime($servicio->fecha)) !!}</td>
            <td>
                {!! $servicio->cliente ? $servicio->cliente->razon_social : '' !!}<br/>
            </td>
            <td>{!! $servicio->producto->nombre !!}</td>
            <td>{!! $servicio->contenedores !!}</td>
            <td>{!! $servicio->kilos !!}</td>
            <td>{!! $servicio->estado_id == 1 ? 'Pendiente' : ($servicio->estado_id == 2 ? 'Facturado' : 'ERR' ) !!}</td>
            <td class="col-xs-2">
            @if ($servicio->estado_id == 1)
                <a class="action-list" href="{!! route('servicios.edit', [$servicio->id]) !!}"><i class="glyphicon glyphicon-edit"> </i></a>
            @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>