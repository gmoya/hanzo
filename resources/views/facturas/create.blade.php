@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    @include('facturas.fields')

</div>
@endsection
