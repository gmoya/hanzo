@extends('app')

@section('content')

    <div class="container factura-show">

        @include('flash::message')

            @if (!$factura)
            <div class="row">
                <div class="well text-center">No se encontró factura.</div>
            </div>
            @else
                @include('facturas.impresion')
            @endif
    </div>
@endsection