{!! Form::open(['route' => 'facturas.store']) !!}

<div class="row form-horizontal">
    <div class="col-md-12 lead">
        Nueva Factura Tipo <span class="tipo_factura"> </span> <hr>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <!-- Sucursal -->
            <div class="form-group">
                {!! Form::label('sucursal', 'Sucursal', ['class' => 'control-label col-xs-3']) !!}
                <div class="col-xs-9">
                    {!! Form::hidden('sucursal_id', null, ['id' => 'sucursal_id', 'class' => 'form-control']) !!}
                    {!! Form::hidden('sucursal_nombre', null, ['id' => 'sucursal_nombre', 'class' => 'form-control']) !!}
                    {!! Form::text('sucursal', null, ['id' => 'sucursal', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>

            <!-- Desde - Hasta -->
            <div class="form-group">
                {!! Form::label('fecha_desde', 'Desde', ['class' => 'control-label col-xs-3']) !!}
                <div class="col-xs-3">
                   {!! Form::text('fecha_desde', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! Form::label('fecha_hasta', 'Hasta', ['class' => 'control-label col-xs-2']) !!}
                <div class="col-xs-3">
                   {!! Form::text('fecha_hasta', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>

            <!-- Fecha factura -->
            <div class="form-group">
                {!! Form::label('fecha_facturacion', 'Fecha de facturaci&oacute;n', ['class' => 'control-label col-xs-3']) !!}
                <div class="col-xs-9">
                   {!! Form::text('fecha_facturacion', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>

            <!-- Condición Venta -->
            <div class="form-group">
                {!! Form::label('condicion_venta_id', 'Condición de Venta', ['class' => 'control-label col-xs-3']) !!}
                <div class="col-xs-9">
                   {!! Form::select('condicion_venta_id', $options['condiciones_venta'], null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>

            <!-- Observaciones -->
            <div class="form-group">
                {!! Form::label('observaciones', 'Observaciones', ['class' => 'control-label col-xs-3']) !!}
                <div class="col-xs-9">
                   {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <!-- Datos del Cliente -->
        <div class="col-sm-4 datos-cliente" style="display:none">
            <div class="col-md-12">
                <span class="text-muted">Razón social:</span><span class="razon_social"> </span><br>
                <span class="text-muted">Nombre Comercial:</span><span class="nombre_comercial"> </span><br>
                <span class="text-muted">CUIT:</span> <span class="cuit"> </span><br>
                <span class="text-muted">Estado:</span><span class="estado"> </span><br>
                <span class="text-muted">Categoría IVA:</span><span class="categoria_iva"> </span><br>
                <span class="text-muted">Nro Orden Compra:</span><span class="orden_compra"> </span><br>
                <span class="text-muted">Condición de Venta:</span><span class="condicion_venta"> </span><br>
                <span class="text-muted">Domicilio de facturación:</span><span class="dom_facturacion"> </span>    
            </div>
        </div>
    </div>


    <fieldset id="detalle-items-factura">
        <legend>Detalle <a id="add-item" class="btn" style="color:rgb(0, 112, 210)">Agregar item</a></legend>

        <div class="col-sm-12">
            <table id="factura-detalle-items" class="table">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th class="text-right">Cantidad</th>
                        <th>Tipo unidad</th>
                        <th class="text-right">Precio unitario</th>
                        <th class="text-right" colspan="2">IVA</th>
                        <th class="text-right">Monto</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="renglon-0">
                        <!-- Producto -->
                        <td>
                            {!! Form::hidden('items[0][producto_id]', null, ['class' => 'form-control producto_id', 'required' => 'required']) !!}
                            {!! Form::hidden('items[0][producto_nombre]', null, ['class' => 'producto_nombre form-control']) !!}
                            {!! Form::text('items[0][producto]', null, ['class' => 'form-control producto', 'required' => 'required']) !!}
                        </td>

                        <!-- Cantidad -->
                        <td>
                            {!! Form::hidden('items[0][item_id]', null, ['class' => 'item_id']) !!}
                            {!! Form::text('items[0][cantidad]', null, ['class' => 'form-control cantidad text-right', 'required' => 'required']) !!}
                        </td>

                        <!-- Tipo Unidad -->
                        <td>
                           <select class="form-control tipo_unidad" name="items[0][tipo_unidad]">
                               <option value="1">Contenedores</option>
                               <option value="2">Kilos</option>
                           </select>
                        </td>

                        <!-- Precio Unitario -->
                        <td class="text-right">
                            <span class="precio-unitario-label hidden">$ 0</span>
                            {!! Form::text('items[0][precio_unitario]', null, ['class' => 'form-control precio-unitario text-right', 'required' => 'required']) !!}
                        </td>
                        
                        <!-- IVA -->
                        <td class="text-right">[<span class="iva-percent-label">0</span>%]</td>
                        <td class="text-right">$ <span class="iva-label">0</span></td>

                        <!-- Subtotal -->
                        <td class="text-right">
                            <span class="subtotal-label">$ 0</span>
                            {!! Form::text('items[0][subtotal]', null, ['class' => 'form-control subtotal text-right hidden']) !!}</td>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- Totales -->
            <table class="col-md-12 factura-totales">
                <tbody>
                    <tr>  
                        <th><div class="col-md-12 text-right">Neto Gravado:</div></th>
                        <td><div class="col-md-12 text-left">$ <span value="0" class="neto">0</span></div></td>
                        <th><div class="col-md-12 text-right">Impuestos IVA:</div></th>
                        <td><div class="col-md-12 text-left">$ <span value="0" class="iva">0</span></td>
                        <th><div class="col-md-12 text-right">Total:</div></th>
                        <td><div class="col-md-12 text-left">$ <span value="0" class="total">0</span></div></td>
                  </tr>
                </tbody>
            </table>

            <br/><br/><br/>
        </div>
    </fieldset>

    <!-- Submit Field -->
    <div class="form-group text-right">
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('facturas.index') !!}">Cancelar</a>
        {!! Form::submit('Facturar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
{!! Form::close() !!}