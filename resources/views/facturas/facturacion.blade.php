@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Facturas</h1>
        </div>

        <div class="row">
            @if($facturas->isEmpty())
                <div class="well text-center">No se encotraron facturas.</div>
            @else
                @foreach($facturas as $factura)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Id: {!! $factura->id !!}</th>
                            <th>Nro. Cliente: {!! $factura->sucursal->nro_cliente !!}</th>
                            <th>N&uacute;mero: {!! $factura->numero !!}</th>
                            <th>Raz&oacute;n: {!! $factura->razon !!}</th>
                            <th>Direcci&oacute;n: {!! $factura->direccion !!}</th>
                        </tr>
                    </thead>
                </table>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-right">Cantidad</th>
                            <th>Producto</th>
                            <th class="text-right">Precio Unitario</th>
                            <th class="text-right">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($factura->items as $item)
                        <tr>
                            <td class="text-right">{{ $item->cantidad() }}</td>
                            <td>{{ $item->producto->nombre . ($item->kilos > 0 ? ' (toneladas)' : '') }}</td>
                            <td class="text-right">$ {{ number_format($item->precio_unitario, 2, '.', ',') }}</td>
                            <td class="text-right">$ {{ number_format($item->neto, 2, '.', ',') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table">
                    <tfoot>
                        <tr>
                            <th>Neto: $ {{ number_format($factura->neto_gravado, 2, '.', ',') }}</th>
                            <th>IVA: $ {{ number_format($factura->impuestos, 2, '.', ',') }}</th>
                            <th>Total: $ {{ number_format($factura->total, 2, '.', ',') }}</th>
                        </tr>
                    </tfoot>
                </table><br/><br/><br/>
                @endforeach
            @endif
        </div>

    </div>
@endsection