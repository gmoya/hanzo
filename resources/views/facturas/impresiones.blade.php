@extends('app')

@section('content')

    <div class="container {{ (!$facturas->isEmpty()) ? 'factura-show' : '' }}">

        @include('flash::message')

        @if ( !$facturas->isEmpty() )
            @foreach ($facturas as $factura)
                @include('facturas.impresion')
            @endforeach
        @else
        {!! Form::open(['route' => 'facturas.imprimir']) !!}
            <div class="row form-horizontal">
                <div class="col-md-12 lead">
                    Impresión de facturas en lote<hr>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- Tipo Factura -->
                        <div class="form-group">
                            {!! Form::label('letra', 'Tipo Factura', ['class' => 'control-label col-xs-3']) !!}
                            <div class="col-xs-9">
                                {!! Form::select('letra', $options['tipos_factura'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <!-- Desde - Hasta -->
                        <div class="form-group">
                            {!! Form::label('desde', 'Desde', ['class' => 'control-label col-xs-3']) !!}
                            <div class="col-xs-3">
                               {!! Form::text('desde', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                            {!! Form::label('hasta', 'Hasta', ['class' => 'control-label col-xs-2']) !!}
                            <div class="col-xs-3">
                               {!! Form::text('hasta', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <br/><br/><br/>
                    </div>
                </div>

                <!-- Submit Field -->
                <div class="form-group text-right">
                    <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('facturas.index') !!}">Cancelar</a>
                    {!! Form::submit('Facturar', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        {!! Form::close() !!}

        @endif
    </div>
@endsection