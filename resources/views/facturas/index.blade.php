@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row title-module">
            <h1 class="pull-left">Facturas</h1>
            <a class="btn btn-sm btn-default pull-right" style="margin-top: 25px; color:rgb(0, 112, 210)" href="facturas/imprimir">Imprimir</a>
            <a class="btn btn-sm btn-default pull-right" style="margin-top: 25px; color:rgb(0, 112, 210)" href="facturas/facturar">Facturación automática</a>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('facturas.create') !!}">Nuevo</a>
        </div>

        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'facturas.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) !!}
                <div class="form-group">
                    {!! Form::text('producto', null, ['class' => 'form-control', 'placeholder' => 'Código o Nombre']) !!}
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"> </span></button>
            {!! Form::close() !!}
        </div>

        <div class="row">
            @if($facturas->isEmpty())
                <div class="well text-center">No se encontraron facturas.</div>
            @else
                @include('facturas.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $facturas])

        <div class="row">
            <p>Resultados {{ $facturas->total() }} facturas</p>
        </div>

    </div>
@endsection