<table class="table">
    <thead>
        <th>Fecha</th>
        <th>Nro.</th>
        <th>Nro. CAE</th>
        <th>Raz&oacute;n</th>
        <th>Direcci&oacute;n</th>
        <th>Manual</th>
        <th>Total</th>
        <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($facturas as $factura)
        <tr>
            <td>{{ date('d-m-Y', strtotime($factura->fecha)) }}</td>
            <td>{{ $factura->getNumeroCompleto() }}</td>
            <td>{{ $factura->cae ? $factura->cae : '' }}</td>
            <td>{{ $factura->razon }}</td>
            <td>{{ $factura->direccion }}</td>
            <td>{{ $factura->manual ? 'S&iacute;' : '' }}</td>
            <td class="text-right">${{ number_format($factura->total, 2, '.', ',') }}</td>
            <td class="col-xs-2">
                <a class="action-list" href="{!! route('facturas.show', [$factura->id]) !!}"><i class="glyphicon glyphicon-list"> </i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>