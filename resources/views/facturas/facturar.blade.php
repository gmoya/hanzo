@extends('app')
@section('content')
@include('common.errors')

{!! Form::open(['id' => 'facturar_form', 'route' => 'facturas.facturar']) !!}
    <div class="col-sm-6 col-lg-6 form-horizontal">
        <div class="form-group">
            {!! Form::label('ciclo_facturacion_id', 'Grupo', ['class' => 'control-label col-xs-3']) !!}
            <div class="col-xs-9">
               {!! Form::select('ciclo_facturacion_id', $options['ciclos'], null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        
        <div class="form-group">

            {!! Form::label('fecha_desde', 'Desde', ['class' => 'control-label col-xs-3']) !!}
            <div class="col-xs-3">
               {!! Form::text('fecha_desde', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>

            {!! Form::label('fecha_hasta', 'Hasta', ['class' => 'control-label col-xs-2']) !!}
            <div class="col-xs-3">
               {!! Form::text('fecha_hasta', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('fecha_facturacion', 'Fecha de facturaci&oacute;n', ['class' => 'control-label col-xs-3']) !!}
            <div class="col-xs-9">
               {!! Form::text('fecha_facturacion', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>

        <!-- Submit Field -->
        <div class="form-group text-right">
            <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('facturas.index') !!}">Cancelar</a>
            {!! Form::submit('Facturar', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
{!! Form::close() !!}

<div id="dialog-message" title="Facturación"> </div>

<script type="text/javascript">
    var fecha_min = "{!! $options['fecha_min'] !!}";
</script>

@endsection