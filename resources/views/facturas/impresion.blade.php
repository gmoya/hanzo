@foreach ($options['copias'] as $copia)

<!-- {{ strtoupper($copia) }} -->
<div class="{{ strtolower($copia) }}">
    <div class="row">
        <div id="logo_wrapper" class="col-md-6">
            <h1 style="height: 100%; display: table-cell; vertical-align: bottom;">
                <img id="logo" class="standard_logo" src="/assets/images/logo_hugo.jpg" alt="Hugo Contenedores" border="0" name="logo"><br>
            </h1>
            <div class="sub"> 
                <span>de CONTENEDORES HUGO S.A.</span>
                <p>
                    Saraza 5677<br/>
                    (C1439APC) Cap. Fed.<br/>
                    4601-2610 (Líneas Rotativas)<br/>
                </p>
                <p><strong>I.V.A. RESPONSABLE INSCRIPTO</strong></p>
            </div>
        </div>

        <div class="letra">
            <h1>
                {!! $factura->letra !!}
                <span class="sub">{{ strtoupper($copia) }}</span>
            </h1>
        </div>

        <div class="datos-factura">
            <h4 class=""><strong>Factura</strong></h4>
            <h4>
                <strong>Nº {{ str_pad($factura->punto_venta->nro, 4, "0", STR_PAD_LEFT) . ' - ' . str_pad($factura->numero_comprobante, 8, "0", STR_PAD_LEFT) }}</strong>
            </h4>
            <p class="fecha"><strong> Fecha:</strong> {{ $factura->getFechaEspaniol() }}</p>

            <br><br><br>

            <div class="sub">
                <p>
                    CUIT Nº 30-68967475-1<br/>
                    ING. BRUTOS C.M. Nº 901-189977-1<br/>
                    Fecha de Inicio de Actividad: 01/01/1997
                </p>
            </div>
        </div>
    </div>
    <hr>
    <table class="table datos-cliente">
        <thead>
            <tr>
                <th>Cliente: {!! $factura->sucursal->nro_cliente !!} - {!! $factura->razon !!}</th>
                <th>Localidad: {!! $factura->localidad->nombre !!}</th>
            </tr>
                <th>Direcci&oacute;n: {!! $factura->direccion !!}</th>
                <th>Provincia: {!! $factura->provincia->nombre !!}</th>
            <tr>
                <th>Cond. de Venta: {!! $factura->condicion_venta->condicion !!}</th>
                <th>C.U.I.T.: {!! $factura->getCuitFormateado() !!}</th>
            </tr>
            <tr>
                <th>Categ. IVA: {!! $factura->cliente->categoria_iva->categoria !!}</th>
                <th>Nº Orden Comp.: {!! $factura->sucursal->nro_orden_compra !!}</th>
            </tr>
            <tr>
                <th>Período Facturado Desde: {{ date('d-m-Y', strtotime($factura->periodo_facturacion_desde)) }}</th>
                <th>Hasta: {{ date('d-m-Y', strtotime($factura->periodo_facturacion_hasta)) }}</th>
            </tr>
        </thead>
    </table>

    <div class="factura-detalle">
        <table class="table">
            <thead>
                <tr>
                    <th class="text-right">Cantidad</th>
                    <th>Producto</th>
                    <th class="text-right">Precio Unitario</th>
                    <th class="text-right">Monto</th>
                </tr>
            </thead>
            <tbody>
                @foreach($factura->items as $item)
                <tr>
                    <td class="text-right">{{ $item->cantidad() }}</td>
                    <td>{{ $item->producto->nombre }}</td>
                    <td class="text-right">$ {{ number_format($item->precio_unitario, 2, '.', ',') }}</td>
                    
                    @if ($factura->letra == 'B')
                    <td class="text-right">$ {{ number_format(($item->neto * (1 + ($item->alicuota->porcentaje / 100))), 2, '.', ',') }}</td>
                    @else
                    <td class="text-right">$ {{ number_format($item->neto, 2, '.', ',') }}</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="text-center"> {{ $factura->observaciones }} </div>
    </div>

    <table class="table">
        <tfoot>
            <tr>
                @if ($factura->letra != 'B')
                <th>Neto Gravado: $ {{ number_format($factura->neto_gravado, 2, '.', ',') }}</th>
                <th>Neto No Gravado: $ 0.00 </th>
                <th class="text-center">IVA: $ {{ number_format($factura->impuestos, 2, '.', ',') }}</th>
                @endif
                <th class="text-right">Total: $ {{ number_format($factura->total, 2, '.', ',') }}</th>
            </tr>
        </tfoot>
    </table>

    <div class="factura-footer">
        <hr class="clear">
        <p class="leonina">Toda rescisión de servicio será aceptada en los mismos días y horarios de atención al público para ventas, cualquiera sea su modo de rescisión. Ley 3.281</p>
        <div class="pull-left barras">
            <img src="{{ 'data:image/png;base64,' . $factura->getCodigoBarras(true) }}" alt="{{ $factura->getCodigoBarras() }}">
        </div>
        <div class="pull-right leonina text-center jurisdiccion">Por cualquier controversia en esta operación las partes se someterán a la Jurisdicción de la Justicia Nacional en lo Comercial de la Capital Federal, renunciando a cualquier otro Fuero y Jurisdicción</div>

        <hr class="clear">
        
        <div class="text-right">
            <p>
                <strong>CAE Nº: {!! $factura->cae !!}</strong><br>
                <strong>Vencimiento: {{ date('d/m/Y', strtotime($factura->vencimiento_cae)) }}</strong>
            </p>
        </div>
    </div>
</div>
@endforeach