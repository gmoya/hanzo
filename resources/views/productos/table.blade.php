<table class="table">
    <thead>
        <th>Id</th>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>IVA</th>
        <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($productos as $producto)
        <tr>
            <td>{!! $producto->id !!}</td>
            <td>{!! $producto->codigo !!}</td>
            <td>{!! $producto->nombre !!}</td>
            <td>{!! $producto->alicuota->porcentaje !!} %</td>
            <td class="col-xs-2">
                <a class="action-list" href="{!! route('productos.edit', [$producto->id]) !!}"><i class="glyphicon glyphicon-edit"> </i></a>
                <a class="action-list" href="{!! route('productos.delete', [$producto->id]) !!}" onclick="return confirm('¿Está seguro que desea borrar este producto?')"><i class="glyphicon glyphicon-remove"> </i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
