@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Productos</h1>
            <a class="btn btn-default pull-right" style="margin-top: 25px; color:rgb(0, 112, 210)" href="{!! route('productos.create') !!}">Nuevo</a>
        </div>

        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'productos.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) !!}
                <div class="form-group">
                    {!! Form::text('producto', null, ['class' => 'form-control', 'placeholder' => 'Código o Nombre']) !!}
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"> </span></button>
            {!! Form::close() !!}
        </div>

        <div class="row">
            @if($productos->isEmpty())
                <div class="well text-center">No se encotraron productos.</div>
            @else
                @include('productos.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $productos])

        <div class="row">
            <p>Resultados {{ $productos->total() }} productos</p>
        </div>

    </div>
@endsection