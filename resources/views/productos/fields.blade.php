<div class="col-sm-6 col-lg-6 form-horizontal">

    <!-- Codigo -->
    <div class="form-group">
        {!! Form::label('codigo', 'Código', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Producto -->
    <div class="form-group">
        {!! Form::label('nombre', 'Nombre', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Alicuotas -->
    <div class="form-group">
        {!! Form::label('alicuota_id', 'Alícuota', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
           {!! Form::select('alicuota_id', $options['alicuotas'], null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('productos.index') !!}">Cancelar</a>
    </div>
</div>