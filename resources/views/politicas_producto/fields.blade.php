<div class="col-sm-6 col-lg-12 form-horizontal">
    @if (!isset($politica_producto))
        {!! Form::hidden('cliente_id', $options['cliente'], ['class' => 'form-control']) !!}
    @endif
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Producto</h3>
        </div>

        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    {!! Form::label('producto_id', 'Producto', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-5">
                       {!! Form::select('producto_id', $options['productos'], null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                    
                <div class="form-group">
                    {!! Form::label('cantidad_mensual', 'Cantidad Mensual', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-3">
                       {!! Form::text('cantidad_mensual', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('cuota_mensual', 'Cuota Mensual', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-3">
                       {!! Form::text('cuota_mensual', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Política de Precios</h3>
        </div>

        <div class="panel-body">
            <div class="form-inline">
            @if (!isset($politica_producto))
                <div class="form-group">
                    {!! Form::label('tipo_cantidad_id', 'Magnitud', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-5">
                       {!! Form::select('tipo_cantidad_id', $options['productos'], null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                    
                <div class="form-group">
                    {!! Form::label('cantidad_hasta', 'Cantidad M&aacute;xima', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-3">
                       {!! Form::text('cantidad_hasta', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('precio', 'Precio', ['class' => 'control-label col-xs-3']) !!}
                    <div class="col-xs-3">
                       {!! Form::text('precio', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a class="btn" style="color:rgb(0, 112, 210)" href="{!! route('politicas_producto.index') !!}">Cancelar</a>
    </div>
</div>