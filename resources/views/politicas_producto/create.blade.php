@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'politicas_producto.store']) !!}

        @include('politicas_producto.fields')

    {!! Form::close() !!}
</div>
@endsection
