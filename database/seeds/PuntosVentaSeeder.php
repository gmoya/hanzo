<?php

use Illuminate\Database\Seeder;

class PuntosVentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('puntos_venta')->insert([
            'nro'             => '00004',
            'nombre'          => 'ADMINISTRACION',
            'sistema'         => 'RECE para aplicativo y web services',
            'domicilio'       => 'FISCAL - 001SARAZA 5677 CP: 1439 Localidad: CAPITAL FEDERAL',
            'fecha_alta'      => '03/03/2016',
            'created_user_id' => 1,
            'created_at'      => date('d-m-Y H:i:s'),
            'updated_at'      => date('d-m-Y H:i:s')
        ]);

        DB::table('puntos_venta')->insert([
            'nro'             => '00005',
            'nombre'          => 'VENTAS',
            'sistema'         => 'RECE para aplicativo y web services',
            'domicilio'       => 'FISCAL - 001SARAZA 5677 CP: 1439 Localidad: CAPITAL FEDERAL',
            'fecha_alta'      => '03/03/2016',
            'created_user_id' => 1,
            'created_at'      => date('d-m-Y H:i:s'),
            'updated_at'      => date('d-m-Y H:i:s')
        ]);

        DB::table('puntos_venta')->insert([
            'nro'             => '00006',
            'nombre'          => 'OPERACIONES',
            'sistema'         => 'RECE para aplicativo y web services',
            'domicilio'       => 'FISCAL - 001SARAZA 5677 CP: 1439 Localidad: CAPITAL FEDERAL',
            'fecha_alta'      => '03/03/2016',
            'created_user_id' => 1,
            'created_at'      => date('d-m-Y H:i:s'),
            'updated_at'      => date('d-m-Y H:i:s')
        ]);

    }
}
