<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\CondicionVenta;
/*
  0 => "Id"
  1 => "Condicion"
  2 => "Variacion"
  3 => "Dias1"
  4 => "Porc1"
  5 => "Dias2"
  6 => "Porc2"
  7 => "Dias3"
  8 => "Porc3"
  9 => "Dias4"
  10 => "Porc4"
  11 => "Dias5"
  12 => "Porc5"
  13 => "FechaMod"
  14 => "UserMod"
*/

class CondicionesVentaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|Condicion|Variacion|Dias1|Porc1|Dias2|Porc2|Dias3|Porc3|Dias4|Porc4|Dias5|Porc5|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/CondicionesVenta.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $condicion_venta = new CondicionVenta();

                $condicion_venta->baciunas_id = $datos[0];
                $condicion_venta->condicion   = $datos[1];
                $condicion_venta->variacion   = $datos[2];
                $condicion_venta->dias_1      = $datos[3];
                $condicion_venta->porc_1      = $datos[4];
                $condicion_venta->dias_2      = $datos[5];
                $condicion_venta->porc_2      = $datos[6];
                $condicion_venta->dias_3      = $datos[7];
                $condicion_venta->porc_3      = $datos[8];
                $condicion_venta->dias_4      = $datos[9];
                $condicion_venta->porc_4      = $datos[10];
                $condicion_venta->dias_5      = $datos[11];
                $condicion_venta->porc_5      = $datos[12];

                $condicion_venta->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }

/*
        DB::table('condiciones_venta')->insert([
            'condicion' => 'Contado',
            'variacion' => 0,
            'dias_1'    => 0,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '03 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 3,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '05 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 5,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '15 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 15,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '30 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 30,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '45 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 45,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '60 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 60,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);

        DB::table('condiciones_venta')->insert([
            'condicion' => '90 Días F.F.',
            'variacion' => 0,
            'dias_1'    => 90,
            'porc_1'    => 100,
            'dias_2'    => 0,
            'porc_2'    => 0,
            'dias_3'    => 0,
            'porc_3'    => 0,
            'dias_4'    => 0,
            'porc_4'    => 0,
            'dias_5'    => 0,
            'porc_5'    => 0            
        ]);
*/
}
