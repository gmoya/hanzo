<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Producto;
use Hanzo\Models\Alicuota;

class ProductosTableSeeder extends Seeder
{
/*
  0 => "Id"
  1 => "Codigo"
  2 => "Producto"
  3 => "IdAlicuota"
  4 => "FechaMod"
  5 => "UserMod"
*/
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Id|Codigo|Producto|IdAlicuota|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/Productos.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $alicuota = ($datos[3] != 0) ? Alicuota::retrieveByBaciunasId($datos[3]) : Alicuota::where('porcentaje', $datos[3])->first();
                
                # PRODUCTO
                $producto = new Producto();

                $producto->baciunas_id     = $datos[0];
                $producto->codigo          = $datos[1];
                $producto->nombre          = $datos[2];
                $producto->alicuota_id     = $alicuota->id;
                $producto->created_at      = date('Y-m-d H:i:s');
                $producto->created_user_id = 1;

                $producto->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}