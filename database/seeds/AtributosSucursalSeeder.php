<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Sucursal;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AtributosSucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contenido = file_get_contents(storage_path('app/csv/Clientes_Atributos.csv'));
        $lineas    = explode("\n", $contenido);
        $sucursales = 0;

        unset($lineas[0]);

        foreach ($lineas as $key => $datos)
        {
            $this->command->info('Migrando línea: '. $key);
            
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                if ($datos[2] == 189 || $datos[2] == 118 || $datos[2] == 107)
                {
                    $sucursal = Sucursal::retrieveByNroCliente($datos[1]);
                    
                    if (!$sucursal) {
                        $this->logError('['.$key.'] No se encontró sucursal cliente nro. '. $datos[1]);
                    } else {

                        if ($sucursal->ciclo_facturacion_id == 1 || $sucursal->ciclo_facturacion_id == 2 ){
                            $this->logError('['.$key.'] Sucursal cliente nro. '. $datos[1].' ya tenía ciclo '. $sucursal->ciclo_facturacion_id . ' cambia a ' . $datos[2]. ': '. ($datos[2] == 189) ? '1' : '2');
                        }

                        $sucursal->ciclo_facturacion_id = ($datos[2] == 189) ? 1 : ($datos[2] == 118 ? 2 : 3);
                        $sucursal->save();
                        $sucursales++;
                    }
                }
            }
        }

        $this->command->warn('Sucursales: '.$sucursales);
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = trim(mb_convert_encoding($value, "UTF-8", 'CP850'));        
        }

        return $datos;
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Atributos Migration Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/mig_atributos.log', Logger::INFO) );
        $view_log->addInfo($contenido);

        $this->command->warn($contenido);
    }
}
