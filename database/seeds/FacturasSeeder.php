<?php

use Illuminate\Database\Seeder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hanzo\Models\Factura;
use Hanzo\Models\Localidad;
use Hanzo\Models\Provincia;
use Hanzo\Models\Sucursal;
use Hanzo\Models\CondicionVenta;
use Hanzo\Models\Vendedor;
use Hanzo\Models\Talonario;
/*
  0  => "Id"
  1  => "IdTalonario"
  2  => "TipoComp"
  3  => "Letra"
  4  => "PtoVta"
  5  => "Numero"
  6  => "Fecha"
  7  => "IdCliente"
  8  => "Fact_Razon"
  9  => "Fact_Direccion"
  10 => "Fact_CodPos"
  11 => "Fact_cLocalidad"
  12 => "Fact_cProvincia"
  13 => "IdCategoriaIva"
  14 => "CUIT"
  15 => "IdVendedor"
  16 => "IdCondVta"
  17 => "Activa"
  18 => "FecAnulacion"
  19 => "NetoGravado"
  20 => "NetoNOGravado"
  21 => "Impuestos"
  22 => "Total"
  23 => "Manual"
  24 => "IDCtaCte"
*/

class FacturasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //  Id|IdTalonario|TipoComp|Letra|PtoVta|Numero|Fecha|IdCliente|Fact_Razon|Fact_Direccion|Fact_CodPos|Fact_cLocalidad|Fact_cProvincia|IdCategoriaIva|CUIT|IdVendedor|IdCondVta|Activa|FecAnulacion|NetoGravado|NetoNOGravado|Impuestos|Total|Manual|IDCtaCte

        $contenido = file_get_contents(storage_path('app/csv/Facturas.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);
           
            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);
#                $this->command->info('Migrando registro: '. $datos[0]);
                
                $sucursal        = Sucursal::retrieveByNroCliente($datos[7]);
                $localidad       = Localidad::retrieveByBaciunasId($datos[11]);
                $provincia       = Provincia::retrieveByPk($datos[12]);
                $condicion_venta = CondicionVenta::retrieveByBaciunasId($datos[16]);
                $vendedor        = Vendedor::retrieveByBaciunasId($datos[15]);
                $talonario       = Talonario::retrieveByBaciunasId($datos[1]);

                if (!$sucursal)
                    $this->logError('['.$datos[0].'] No se encontró sucursal cliente nro. '. $datos[7]);
                elseif (!$localidad)
                    $this->logError('['.$datos[0].'] No se encontró localidad nro. '. $datos[11]);
                elseif (!$provincia)
                    $this->logError('['.$datos[0].'] No se encontró provincia nro. '. $datos[12]);
                elseif (!$condicion_venta)
                    $this->logError('['.$datos[0].'] No se encontró condicion venta nro. '. $datos[16]);
                elseif (!$vendedor)
                    $this->logError('['.$datos[0].'] No se encontró vendedor nro '. $datos[15]);

                $factura = new Factura();

                $factura->baciunas_id          = $datos[0];
                $factura->talonario_id         = $talonario->id;
                #$factura->talonario_id         = $datos[1];
                $factura->sucursal_id          = $sucursal ? $sucursal->id : $datos[7] + 9900000;
                $factura->cliente_id           = $sucursal ? $sucursal->cliente_id : $datos[7] + 9900000;
                $factura->tipo_comprobante     = $datos[2];
                $factura->letra                = $datos[3];
                $factura->punto_venta          = $datos[4];
                $factura->numero               = $datos[5];
                $factura->fecha                = $datos[6];
                $factura->razon                = $datos[8];
                $factura->direccion            = $datos[9];
                $factura->codigo_postal        = $datos[10];
                $factura->localidad_id         = $localidad ? $localidad->id : $datos[11] + 9900000;
                $factura->provincia_id         = $provincia ? $provincia->id : $datos[12] + 9900000;
                $factura->categoria_iva_id     = $datos[13];
                $factura->cuit                 = $datos[14];
                $factura->vendedor_id          = $vendedor ? $vendedor->id : $datos[15] + 9900000;
                $factura->condicion_venta_id   = $condicion_venta ? $condicion_venta->id : $datos[16] + 9900000;
                $factura->activa               = $datos[17];
                $factura->manual               = $datos[23];
                $factura->fecha_anulacion      = $datos[18];
                $factura->neto_gravado         = $datos[19];
                $factura->neto_no_gravado      = $datos[20];
                $factura->impuestos            = $datos[21];
                $factura->total                = $datos[22];
#                $factura->cuenta_corriente_id  = $cuenta_corriente->id;
                $factura->cuenta_corriente_id  = $datos[24];
                $factura->created_user_id      = 1;
                
                $factura->save();
            }
        }

        $this->command->info('Fin de migración.');
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        $datos[6]  = $datos[6]  ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[6])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');
        $datos[18] = $datos[18] ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[18])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');

        return $datos;
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Facturas Migration Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/mig_facturas.log', Logger::INFO) );
        $view_log->addInfo($contenido);

        $this->command->warn($contenido);
    }
}