<?php

use Illuminate\Database\Seeder;

class TiposDomicilioClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_domicilio_cliente')->insert([
            'nombre'      => 'cbz',
            'descripcion' => 'Cobranza',
        ]);

        DB::table('tipos_domicilio_cliente')->insert([
            'nombre'      => 'ftc',
            'descripcion' => 'Facturación',
        ]);

        DB::table('tipos_domicilio_cliente')->insert([
            'nombre'      => 'srv',
            'descripcion' => 'Servicio',
        ]);
    }
}
