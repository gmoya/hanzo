<?php

use Illuminate\Database\Seeder;

class EstadosClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados_cliente')->insert([
            'nombre' => 'Normal'
        ]);

        DB::table('estados_cliente')->insert([
            'nombre' => 'Suspendido'
        ]);

        DB::table('estados_cliente')->insert([
            'nombre' => 'Inactivo'
        ]);
    }
}
