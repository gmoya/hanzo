<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\RubroEmpresario;
/*
  0 => "Id"
  1 => "RubroEmp"
  2 => "FechaMod"
  3 => "UserMod"
*/

class RubrosEmpresariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Id|RubroEmp|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/RubrosEmpresarios.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $rubro = new RubroEmpresario();

                $rubro->rubro           = $datos[1];
                $rubro->baciunas_id     = $datos[0];
                $rubro->created_user_id = 1;

                $rubro->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}
