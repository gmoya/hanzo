<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Alicuota;

/*
  0 => "Id"
  1 => "Codigo"
  2 => "Alicuota"
  3 => "Abrev"
  4 => "Porcentaje"
  5 => "ImpMinimo"
  6 => "IdProvincia"
  7 => "UserMod"
  8 => "FechaMod"
*/

class AlicuotasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|Codigo|Alicuota|Abrev|Porcentaje|ImpMinimo|IdProvincia|UserMod|FechaMod
        $contenido = file_get_contents(storage_path('app/csv/Alicuotas.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                # PERSONA
                $alicuota = new Alicuota();

                $alicuota->baciunas_id     = $datos[0];
                $alicuota->codigo          = $datos[1];
                $alicuota->alicuota        = $datos[2];
                $alicuota->abreviatura     = $datos[3];
                $alicuota->porcentaje      = $datos[4];
                $alicuota->imp_minimo      = $datos[5];
                $alicuota->provincia_id    = ($datos[6] != 0) ? $datos[6] : null;
                $alicuota->created_user_id = 1;

                $alicuota->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}
