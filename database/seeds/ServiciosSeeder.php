<?php

use Illuminate\Database\Seeder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hanzo\Models\Factura;
use Hanzo\Models\Producto;
use Hanzo\Models\Sucursal;
use Hanzo\Models\Servicio;
/*
  0  => "Id"
  1  => "IdCliente"
  2  => "Fecha"
  3  => "IdProducto"
  4  => "Contenedores"
  5  => "Kilos"
  6  => "Direccion"
  7  => "Observaciones"
  8  => "Monto"
  9  => "Estado"
  10 => "IdFactura"
  11 => "FechaFactura"
  12 => "IdClienteFacturado"
  13 => "FechaMod"
  14 => "UserMod"
  15 => "NumManifiesto"
  16 => "NumCR"
  17 => "FechaIngreso"
  18 => "MuestraObservaciones"
*/

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Id|IdCliente|Fecha|IdProducto|Contenedores|Kilos|Direccion|Observaciones|Monto|Estado|IdFactura|FechaFactura|IdClienteFacturado|FechaMod|UserMod|NumManifiesto|NumCR|FechaIngreso|MuestraObservaciones

        $inicio = date('d/m/Y H:i:s');
        $this->logError('Inicio de migración: '. $inicio);

        $contenido = file_get_contents(storage_path('app/csv/Servicios.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);
            

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);
            
            if ($datos[0] != '')
            #if ($datos[0] != '' && count($datos) == 19)  # QUITAR
            {
                $datos = $this->transformarDatos($datos);
                
                $sucursal = Sucursal::retrieveByNroCliente($datos[1]);
                $producto = Producto::retrieveByBaciunasId($datos[3]);
                $factura  = Factura::retrieveByBaciunasId($datos[10]);
                
                if (!$sucursal)
                    $this->logError('['.$datos[0].'] No se encontró sucursal nro. '. $datos[1]);
                elseif (!$producto)
                    $this->logError('['.$datos[0].'] No se encontró producto nro. '. $datos[3]);
                elseif (!$factura)
                    $this->logError('['.$datos[0].'] No se encontró factura nro. ' . $datos[10]);

                $servicio = new Servicio();

                $servicio->baciunas_id     = $datos[0];
                $servicio->cliente_id      = $datos[1]  != 0 ? ($sucursal ? $sucursal->cliente_id : $datos[1] + 990000000) : null;
                $servicio->sucursal_id     = $datos[1]  != 0 ? ($sucursal ? $sucursal->id : $datos[1] + 990000000) : null;
                $servicio->producto_id     = $datos[3]  != 0 ? ($producto ? $producto->id : $datos[3] + 990000000) : null;
                $servicio->factura_id      = $datos[10] != 0 ? ($factura  ? $factura->id : $datos[10] + 990000000) : null;

                $servicio->direccion       = $datos[6];
                $servicio->contenedores    = $datos[4];
                $servicio->kilos           = $datos[5];
                $servicio->monto           = $datos[8];
                $servicio->nro_manifiesto  = $datos[15];
                $servicio->nro_cr          = $datos[16];
                $servicio->estado_id       = $datos[9];
                $servicio->observaciones   = $datos[7];
                
                $servicio->fecha           = $datos[2];
                $servicio->fecha_factura   = $datos[11];
                $servicio->fecha_ingreso   = $datos[17];
                $servicio->created_user_id = 1;

                $servicio->save();
            }
        }

        $fin = date('d/m/Y H:i:s');
        
        $this->command->info('Inicio de migración: '. $inicio);
        $this->command->info('Fin de migración: ' . $fin);
        
        $this->logError('Fin de migración: ' . $fin);
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        $datos[2]  = str_replace('.000', '', $datos[2]);
        $datos[11] = str_replace('.000', '', $datos[11]);
        $datos[13] = str_replace('.000', '', $datos[13]);
        $datos[17] = str_replace('.000', '', $datos[17]);

        $datos[2]  = $datos[2]  ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[2])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');
        $datos[11] = $datos[11] ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[11])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');
        $datos[13] = $datos[13] ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[13])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');
        $datos[17] = $datos[17] ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[17])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');

        return $datos;
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Servicios Migration Log');

        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/mig_servicios.log', Logger::INFO) );
        $view_log->addInfo($contenido);

        $this->command->warn($contenido);
    }
}