<?php

use Illuminate\Database\Seeder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hanzo\Models\Sucursal;
use Hanzo\Models\AbonoSucursal;
use Hanzo\Models\PoliticaProducto;
use Hanzo\Models\PoliticaPrecio;
use Hanzo\Models\Frecuencia;

/*
  0  => "Id"
  1  => "IdCliente"
  2  => "IdProducto"
  3  => "Cantidad"
  4  => "IdFrecuencia"
  5  => "Lunes"
  6  => "Martes"
  7  => "Miercoles"
  8  => "Jueves"
  9  => "Viernes"
  10 => "Sabado"
  11 => "Domingo"
  12 => "Cant1"
  13 => "Cant1Precio"
  14 => "Cant2"
  15 => "Cant2Precio"
  16 => "Cant3"
  17 => "Cant3Precio"
  18 => "Cant4"
  19 => "Cant4Precio"
  20 => "Cant5"
  21 => "Cant5Precio"
  22 => "Kilos1"
  23 => "Kilos1Precio"
  24 => "Kilos2"
  25 => "Kilos2Precio"
  26 => "Kilos3"
  27 => "Kilos3Precio"
  28 => "FechaUltColocacion"
  29 => "FechaRetiro"
  30 => "FechaMod"
  31 => "UserMod"
  32 => "Abono"
*/

class PoliticasPreciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contenido = file_get_contents(storage_path('app/csv/Clientes_Comerciales.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $key => $datos)
        {
            $this->command->info('Migrando línea: '. $key);
            
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $sucursal   = Sucursal::retrieveByNroCliente($datos[1]);
                $frecuencia = Frecuencia::retrieveByBaciunasId($datos[4]);
                
                if (!$sucursal)
                    $this->logError('['.$key.'] No se encontró sucursal cliente nro. '. $datos[1]);

                $politica_producto = new PoliticaProducto();

                $politica_producto->cantidad_mensual = ($datos[32] && $datos[13] == 0) ? $datos[12] : null;
                $politica_producto->baciunas_id = $datos[0];
                $politica_producto->producto_id = $datos[2];
                $politica_producto->sucursal_id = $sucursal ? $sucursal->id : $datos[1] + 990000000;
                $politica_producto->cliente_id  = $sucursal ? $sucursal->cliente_id : $datos[1] + 990000000;
                $politica_producto->lunes       = $datos[5];
                $politica_producto->martes      = $datos[6];
                $politica_producto->miercoles   = $datos[7];
                $politica_producto->jueves      = $datos[8];
                $politica_producto->viernes     = $datos[9];
                $politica_producto->sabado      = $datos[10];
                $politica_producto->domingo     = $datos[11];
                $politica_producto->created_user_id = 1;

                $politica_producto->save();

                if ($datos[32] && $datos[13] == 0 && !$sucursal->abono)
                {
                   $this->generarAbono($sucursal, 0);
                } elseif ($datos[32] && $datos[13] == 0 && $sucursal->abono) {
                    $sucursal->abono->activo = true;
                    $sucursal->abono->save();
                }

                $cantidades = array(12,14,16,18,20,22,24,26);

                foreach ($cantidades as $cant) 
                {
                    if ( $datos[$cant] > 0 && $datos[$cant+1] != 0 )
                    {
                        $politica_precio = new PoliticaPrecio();

                        $politica_precio->precio = $datos[$cant+1];
                        $politica_precio->cantidad_hasta = ( $datos[$cant] > 2147483647 ) ? 2147483647 : $datos[$cant];
                        $politica_precio->created_user_id = 1;
                        $politica_precio->baciunas_id = $datos[0];
                        $politica_precio->producto_id = $politica_producto->producto_id;
                        $politica_precio->politicas_producto_id = $politica_producto->id;
                        $politica_precio->tipo_cantidad_id = ($cant < 22) ? 1 : 2;
                        $politica_precio->cliente_id = $politica_producto->cliente_id;

                        $politica_precio->save();
                    }
                }
            }
        }
    }

    private function generarAbono($sucursal, $cuota_mensual)
    {
        $abono = new AbonoSucursal();

        $abono->monto           = (float) $cuota_mensual;
        $abono->activo          = true;
        $abono->cliente_id      = $sucursal->cliente_id;
        $abono->sucursal_id     = $sucursal->id;
        $abono->tipo_abono_id   = 1;
        $abono->created_user_id = 1;

        $abono->save();
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = trim(mb_convert_encoding($value, "UTF-8", 'CP850'));        
        }

        return $datos;
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Políticas Precios Migration Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/mig_politicas_precios.log', Logger::INFO) );
        $view_log->addInfo($contenido);

        $this->command->warn($contenido);
    }
}
