<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Cliente;
use Hanzo\Models\Chofer;
use Hanzo\Models\Localidad;
use Hanzo\Models\DomicilioCliente;
use Hanzo\Models\Sucursal;
use Hanzo\Models\AbonoSucursal;
use Hanzo\Models\TipoCliente;
use Hanzo\Models\CondicionVenta;
use Hanzo\Models\RubroEmpresario;
use Hanzo\Models\Vendedor;
use Hanzo\Models\Alicuota;

/*
  0  => "Id"
  1  => "Razon"
  2  => "Comercial"
  3  => "Fact_Direccion"
  4  => "Fact_CodPos"
  5  => "Fact_cLocalidad"
  6  => "Fact_cProvincia"
  7  => "Fact_Telefono1"
  8  => "Fact_Telefono2"
  9  => "Fact_Email"
  10 => "Fact_Horario"
  11 => "Cob_Direccion"
  12 => "Cob_CodPos"
  13 => "Cob_cLocalidad"
  14 => "Cob_cProvincia"
  15 => "Cob_Telefono1"
  16 => "Cob_Telefono2"
  17 => "Cob_Email"
  18 => "Cob_Horario"
  19 => "Cob_PersonaContacto"
  20 => "Prod_Direccion"
  21 => "Prod_EntreCalle1"
  22 => "Prod_EntreCalle2"
  23 => "Prod_CodPos"
  24 => "Prod_cLocalidad"
  25 => "Prod_cProvincia"
  26 => "Prod_Telefono1"
  27 => "Prod_Telefono2"
  28 => "Prod_Email"
  29 => "Prod_Horario"
  30 => "Prod_PersonaContacto"
  31 => "IdVendedor"
  32 => "IdCobrador"
  33 => "IdCondVenta"
  34 => "IdCategIVA"
  35 => "IdPercepIIBB"
  36 => "IdPercepIIBBBsAs"
  37 => "CUIT"
  38 => "HojaFilcar"
  39 => "Cob_HojaFilcar"
  40 => "IdChofer"
  41 => "Web"
  42 => "Estado"
  43 => "CuotaMensual"
  44 => "IdRubroEmpresario"
  45 => "IdClienteFacturacion"
  46 => "IdTipoCliente"
  47 => "NroCEAMSE"
  48 => "Recomendado"
  49 => "FechaAlta"
  50 => "FechaMod"
  51 => "FechaInhab"
  52 => "cUsuario"
  53 => "cUsuario_Mod"
  54 => "Observaciones"
  55 => "NroOrdenCompra"
  56 => "FechaExp"
*/

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sucursales = array();
        $contenido = file_get_contents(storage_path('app/csv/Clientes.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $key => $datos)
        {
            $this->command->info('Migrando línea: '. $key);
            
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);
                $madre = Cliente::where('clientes.cuit', $datos[37])->first();
                
                #   Cuentas madres
                # 45 => "IdClienteFacturacion"
                # 37 => "CUIT"
                if (( ($datos[45] == 0) || ($datos[45] == $datos[0]) ) && !$madre)
                {
                    $cliente = $this->generarCliente($datos);

                    $factura  = $this->generarDomicilioFacturacion($datos, $cliente);
                    $cobro    = $this->generarDomicilioCobranza($datos, $cliente);
                    $sucursal = $this->generarSucursal($datos, $cliente, $factura, $cobro);

                    if ( $cliente->cuota_mensual > 0 ){
                       $this->generarAbono($sucursal, $cliente->cuota_mensual);
                    }
                } else {
                    $sucursales[] = $datos;
                }
            }
        }

        $this->command->info('Migrando sucursales... ');
        
        foreach ($sucursales as $datos)
        {
            $cliente = Cliente::where('clientes.cuit', $datos[37])->first();

            $cliente = $cliente ? $cliente : Cliente::where('clientes.nro_cliente', $datos[45])->first();
            
            $cliente = $cliente ? $cliente : $this->generarCliente($datos);

            $dom_fac = DomicilioCliente::where('cliente_id', $cliente->id)
                                        ->where('calle', $datos[3])
                                        ->where('tipo_domicilio_cliente_id', 2)
                                        ->first();

            $dom_cob = DomicilioCliente::where('cliente_id', $cliente->id)
                                        ->where('calle', $datos[11])
                                        ->where('tipo_domicilio_cliente_id', 1)
                                        ->first();

            $factura  = ($dom_fac) ? $dom_fac : $this->generarDomicilioFacturacion($datos, $cliente);
            $cobro    = ($dom_cob) ? $dom_cob : $this->generarDomicilioCobranza($datos, $cliente);
            
            $sucursal = $this->generarSucursal($datos, $cliente, $factura, $cobro);

            if ( $datos[43] > 0 ) {
                $this->generarAbono($sucursal, $datos[43]);
            }
        }
     
        $this->command->info('Fin de migración.');
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }
        
        $datos[37] = str_replace('-', '', $datos[37]);
        $datos[56] = str_replace('.000', '', $datos[56]);
        $datos[56] = $datos[56] ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[56])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');
    
        return $datos;
    }

    private function generarCliente($datos)
    {
        $tipo_cliente     = TipoCliente::retrieveByBaciunasId($datos[46]);
        $rubro_empresario = RubroEmpresario::retrieveByBaciunasId($datos[44]);
        $alicuota         = Alicuota::retrieveByBaciunasId($datos[35]);
        $alicuota_bsas    = Alicuota::retrieveByBaciunasId($datos[36]);
        
        $cliente = new Cliente();

        $cliente->nro_cliente            = $datos[0];
        $cliente->razon_social           = $datos[1];
        $cliente->nombre_comercial       = $datos[2];
        $cliente->cuit                   = $datos[37];
        $cliente->estado_id              = ($datos[42] == 4) ? 1 : $datos[42];
        $cliente->categoria_iva_id       = $datos[34];
        $cliente->ingreso_brutos_id      = ($datos[35]) ? $alicuota->id : null;
        $cliente->ingreso_brutos_bsas_id = ($datos[36]) ? $alicuota_bsas->id : null;
        $cliente->fecha_expiracion       = $datos[56];
        $cliente->rubro_empresario_id    = $rubro_empresario->id;
        $cliente->web                    = $datos[41];
        $cliente->cuota_mensual          = $datos[43];
        $cliente->observaciones          = $datos[54];
        $cliente->tipo_cliente_id        = $tipo_cliente->id;
        $cliente->created_user_id        = 1;

        $cliente->save();
    
        return $cliente;
    }

    private function generarDomicilioFacturacion($datos, $cliente)
    {
        $localidad =  Localidad::retrieveByBaciunasId($datos[5]);

        $domicilio = new DomicilioCliente();

        $domicilio->calle                     = $datos[3];
        $domicilio->nro                       = 0;
        $domicilio->codigo_postal             = $datos[4];
        $domicilio->provincia_id              = $datos[6];
        $domicilio->localidad_id              = $localidad->id;
        $domicilio->telefono                  = $datos[7] . ' ' . $datos[8];
        $domicilio->email                     = $datos[9];
        $domicilio->horario_atencion          = $datos[10];
        $domicilio->hoja_filcar               = $datos[38];
        $domicilio->tipo_domicilio_cliente_id = 2;
        $domicilio->cliente_id                = $cliente->id;
        $domicilio->created_user_id           = 1;

        $domicilio->save();

        return $domicilio;
    }

    private function generarDomicilioCobranza($datos, $cliente) 
    {
        $localidad =  Localidad::retrieveByBaciunasId($datos[13]);

        $domicilio = new DomicilioCliente();

        $domicilio->calle                     = $datos[11]; # R
        $domicilio->nro                       = 0; # R
        $domicilio->codigo_postal             = $datos[12];
        $domicilio->provincia_id              = $datos[14]; # R
        $domicilio->localidad_id              = $localidad->id; # R
        $domicilio->telefono                  = $datos[15] . ' ' . $datos[16];
        $domicilio->email                     = $datos[17];
        $domicilio->horario_atencion          = $datos[18];
        $domicilio->contacto                  = $datos[19];
        $domicilio->hoja_filcar               = $datos[39];
        $domicilio->tipo_domicilio_cliente_id = 1;
        $domicilio->cliente_id                = $cliente->id; # R
        $domicilio->created_user_id           = 1;

        $domicilio->save();

        return $domicilio;
    }

    private function generarSucursal($datos, $cliente, $factura, $cobro)
    {
        $localidad       = Localidad::retrieveByBaciunasId($datos[24]);
        $vendedor        = Vendedor::retrieveByBaciunasId($datos[31]);
        $cobrador        = Vendedor::retrieveByBaciunasId($datos[32]);
        $chofer          = Chofer::retrieveByBaciunasId($datos[40]);
        $condicion_venta = CondicionVenta::retrieveByBaciunasId($datos[33]);

        $sucursal = new Sucursal();

        $sucursal->nro_cliente            = $datos[0];
        $sucursal->nombre               = ($cliente->razon_social != $datos[1])  ? $datos[1] . ' ' . $datos[2] : $datos[2];
        $sucursal->calle                = $datos[20]; # R
        $sucursal->nro                  = 0; # R
        $sucursal->codigo_postal        = $datos[23];
        $sucursal->provincia_id         = $datos[25]; # R
        $sucursal->localidad_id         = $localidad->id; # R
        $sucursal->telefono             = $datos[26] . ' ' . $datos[27];
        $sucursal->email                = $datos[28];
        $sucursal->horario_atencion     = $datos[29];
        $sucursal->contacto             = $datos[30];
        $sucursal->hoja_filcar          = $datos[38];
        $sucursal->nro_ceamse           = $datos[47];
        $sucursal->entre_calle_a        = $datos[21];
        $sucursal->entre_calle_b        = $datos[22];
        $sucursal->condicion_venta_id   = $condicion_venta->id;
        $sucursal->chofer_id            = $chofer->id;
        $sucursal->domicilio_cobro_id   = $cobro->id;
        $sucursal->domicilio_factura_id = $factura->id;
        $sucursal->estado_id            = ($datos[42] == 4) ? 1 : $datos[42]; # R
        $sucursal->cliente_id           = $cliente->id;
        $sucursal->vendedor_id          = $vendedor->id;
        $sucursal->cobrador_id          = $cobrador->id;
        $sucursal->nro_orden_compra     = $datos[55];
        $sucursal->created_user_id      = 1;

        $sucursal->save();

        return $sucursal;
    }

    private function generarAbono($sucursal, $cuota_mensual)
    {
        $abono = new AbonoSucursal();

        $abono->monto           = (float) $cuota_mensual;
        $abono->activo          = false;
        $abono->cliente_id      = $sucursal->cliente_id;
        $abono->sucursal_id     = $sucursal->id;
        $abono->tipo_abono_id   = 1;
        $abono->created_user_id = 1;

        $abono->save();
    }
}