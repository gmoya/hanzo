<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Talonario;

class TalonariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|Talonario|Fiscal|Autonumera|Debito|Modulos|TipoComp|Letra|PtoVta|Primero|Ultimo|Proximo|FechaVenc|CAI|UserMod|FechaMod

        $contenido = file_get_contents(storage_path('app/csv/Talonarios.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $talonario = new Talonario();

                $talonario->baciunas_id       = $datos[0];
                $talonario->talonario         = $datos[1];
                $talonario->fiscal            = $datos[2];
                $talonario->autonumera        = $datos[3];
                $talonario->debito            = $datos[4];
                $talonario->modulos           = $datos[5];
                $talonario->tipo_comprobante  = $datos[6];
                $talonario->letra             = $datos[7];
                $talonario->punto_venta       = $datos[8];
                $talonario->primero           = $datos[9];
                $talonario->ultimo            = $datos[10];
                $talonario->proximo           = $datos[11];
                $talonario->fecha_vencimiento = $datos[12];
                $talonario->cai               = $datos[13];
                $talonario->created_user_id = 1;

                $talonario->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        $datos[12]  = $datos[12]  ? DateTime::createFromFormat('Y-m-d H:i:s', $datos[12])->format('d-m-Y H:i:s') : date('d-m-Y H:i:s');

        return $datos;
    }
}
