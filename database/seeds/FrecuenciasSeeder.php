<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Frecuencia;

/*
  0 => "Id"
  1 => "Frecuencia"
  2 => "FechaMod"
  3 => "UserMod"
*/

class FrecuenciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //"Id"|"Frecuencia"|"FechaMod"|"UserMod"

        $contenido = file_get_contents(storage_path('app/csv/Frecuencias.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                # FRECUENCIA
                $frecuencia = new Frecuencia();

                $frecuencia->baciunas_id     = $datos[0];
                $frecuencia->nombre          = $datos[1];
                $frecuencia->created_user_id = 1;

                $frecuencia->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}
