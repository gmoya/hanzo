<?php

use Illuminate\Database\Seeder;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hanzo\Models\Factura;
use Hanzo\Models\Producto;
use Hanzo\Models\Alicuota;
use Hanzo\Models\ItemFactura;

/*
  0  => "Id"
  1  => "IdFactura"
  2  => "Renglon"
  3  => "IdProducto"
  4  => "Contenedores"
  5  => "Kilos"
  6  => "NotaFactura"
  7  => "PrecioUnitario"
  8  => "Neto"
  9  => "IdAlicuota"
*/

class ItemsFacturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|IdFactura|Renglon|IdProducto|Contenedores|Kilos|NotaFactura|PrecioUnitario|Neto|IdAlicuota

        $contenido = file_get_contents(storage_path('app/csv/Factura_Items.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);
           
            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);
#                $this->command->info('Migrando registro: '. $datos[0]);

                $factura        = Factura::retrieveByBaciunasId($datos[1]);
                $producto       = Producto::retrieveByBaciunasId($datos[3]);
                $alicuota       = Alicuota::retrieveByBaciunasId($datos[9]);

                if (!$factura)
                    $this->logError('['.$datos[0].'] No se encontró factura nro. '. $datos[1]);
                elseif (!$producto)
                    $this->logError('['.$datos[0].'] No se encontró producto nro. '. $datos[3]);
                elseif (!$alicuota)
                    $this->logError('['.$datos[0].'] No se encontró alicuota nro. '. $datos[9]);

                $item = new ItemFactura();

                $item->baciunas_id     = $datos[0];
                $item->factura_id      = $factura  ? $factura->id : $datos[1] + 990000000;
                $item->producto_id     = $producto ? $producto->id : $datos[3] + 990000000;
                $item->alicuota_id     = $alicuota ? $alicuota->id : $datos[9] + 990000000;
                $item->renglon         = $datos[2];
                $item->contenedores    = $datos[4];
                $item->kilos           = $datos[5];
                $item->precio_unitario = $datos[7];
                $item->neto            = $datos[8];
                $item->nota_factura    = $datos[6];
                $item->created_user_id = 1;

                $item->save();
            }
        }

        $this->command->info('Fin de migración.');
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }

    protected function logError($contenido)
    {
        $view_log = new Logger('Items Factura Migration Log');
        $view_log->pushHandler( new StreamHandler(storage_path() . '/logs/mig_items_factura.log', Logger::INFO) );
        $view_log->addInfo($contenido);

        $this->command->warn($contenido);
    }
}