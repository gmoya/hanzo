<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\TipoComprobante;
use Hanzo\Models\PuntoVenta;
use Hanzo\Models\NumeradoComprobante;

class NumeradoComprobanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = TipoComprobante::get();
        $puntos = PuntoVenta::get();

        foreach ($tipos as $key => $tipo) 
        {
            foreach ($puntos as $key => $punto) 
            {
                $numerado = new NumeradoComprobante();

                $numerado->punto_venta_id      = $punto->id;
                $numerado->tipo_comprobante_id = $tipo->id;
                $numerado->created_user_id     = 1;

                $numerado->save();            
            }
        }
    }
}
