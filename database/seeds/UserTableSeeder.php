<?php

use Illuminate\Database\Seeder;

/**
* 
*/
class UserTableSeeder extends Seeder
{
    
    public function run()
    {
        \DB::table('users')->insert( array(
            'name'          =>  'Gutavo Moya',
            'email'         =>  'gustavo22@hotmail.com',
            'password'      =>  \Hash::make('hanzo816'),
            'punto_venta_id' =>  1,
            'created_at'    =>  date('Y-m-d H:i:s'),
            'updated_at'    =>  date('Y-m-d H:i:s')
        ));

        \DB::table('users')->insert( array(
            'name'           =>  'Tester',
            'email'          =>  'test@hugo.com.ar',
            'password'       =>  \Hash::make('test908'),
            'punto_venta_id' =>  1,
            'created_at'     =>  date('Y-m-d H:i:s'),
            'updated_at'     =>  date('Y-m-d H:i:s')
        ));

        \DB::table('users')->insert( array(
            'name'           =>  'Martin',
            'email'          =>  'martin@contenedoreshugo.com.ar',
            'password'       =>  \Hash::make('martinc'),
            'punto_venta_id' =>  3,
            'created_at'     =>  date('Y-m-d H:i:s'),
            'updated_at'     =>  date('Y-m-d H:i:s')
        ));

        \DB::table('users')->insert( array(
            'name'           =>  'Carlos',
            'email'          =>  'carlos@contenedoreshugo.com.ar',
            'password'       =>  \Hash::make('carlosb'),
            'punto_venta_id' =>  2,
            'created_at'     =>  date('Y-m-d H:i:s'),
            'updated_at'     =>  date('Y-m-d H:i:s')
        ));

        \DB::table('users')->insert( array(
            'name'           =>  'Gabriel',
            'email'          =>  'gabriel@contenedoreshugo.com.ar',
            'password'       =>  \Hash::make('gabrield'),
            'punto_venta_id' =>  1,
            'created_at'     =>  date('Y-m-d H:i:s'),
            'updated_at'     =>  date('Y-m-d H:i:s')
        ));
    }
}