<?php

use Illuminate\Database\Seeder;

class ProvinciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provincias = array(
            'Buenos Aires',
            'Capital Federal',
            'Catamarca',
            'Córdoba',
            'Corrientes',
            'Chaco',
            'Chubut',
            'Entre Ríos',
            'Formosa',
            'Jujuy',
            'La Pampa',
            'La Rioja',
            'Mendoza',
            'Misiones',
            'Neuquén',
            'Río Negro',
            'Salta',
            'San Juan',
            'San Luis',
            'Santa Cruz',
            'Santa Fe',
            'Sgo.del Estero',
            'Tierra del Fuego',
            'Tucumán',
            'Otra'
        );

        foreach ($provincias as $pcia) 
        {
            DB::table('provincias')->insert([
                'nombre' => $pcia,
            ]);
        }
    }
}
