<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Vendedor;
use Hanzo\Models\Persona;
use Hanzo\Models\DomicilioPersona;
/*
  0 => "Id"
  1 => "Vendedor"
  2 => "Iniciales"
  3 => "Direccion"
  4 => "IdLocalidad"
  5 => "IdProvincia"
  6 => "Telefono"
  7 => "Celular"
  8 => "Email"
  9 => "Comision"
  10 => "FechaMod"
  11 => "UserMod"
*/

class VendedoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|Vendedor|Iniciales|Direccion|IdLocalidad|IdProvincia|Telefono|Celular|Email|Comision|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/Vendedores.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                # PERSONA
                $persona = new Persona();

                $persona->nombre          = $datos[1];
                $persona->telefono        = $datos[6];
                $persona->celular         = $datos[7];
                $persona->email           = $datos[8];
                $persona->created_user_id = 1;

                $persona->save();

                # DOMICILIO
                if ( $datos[3] )
                {
                    $domicilio = new DomicilioPersona();

                    $domicilio->calle         = $datos[3];
                    $domicilio->provincia_id  = $datos[5] ? $datos[5] : 1;
                    $domicilio->localidad_id  = $datos[4] ? $datos[4] : 1;
                    $domicilio->persona_id    = $persona->id;
                    
                    $domicilio->save();
                }

                # VENDEDOR
                $vendedor = new Vendedor();

                $vendedor->iniciales       = $datos[2];
                $vendedor->baciunas_id     = $datos[0];
                $vendedor->comision        = (float) $datos[9];
                $vendedor->persona_id      = $persona->id;
                $vendedor->created_user_id = 1;
                
                $vendedor->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}
