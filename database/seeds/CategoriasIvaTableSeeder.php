<?php

use Illuminate\Database\Seeder;

class CategoriasIvaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias_iva')->insert([
            'abreviatura'       => 'RI',
            'categoria'         => 'Responsable Inscripto',
            'tipo_factura'      => 'A',
            'tasa_general'      => 0,
            'desglosa_general'  => 0,
            'tasa_adicional'    => 0,
            'desglosa_tasa'     => 1,
            'cobra_tasa'        => 1
        ]);

        DB::table('categorias_iva')->insert([
            'abreviatura'       => 'CF',
            'categoria'         => 'Consumidor Final',
            'tipo_factura'      => 'B',
            'tasa_general'      => 0,
            'desglosa_general'  => 0,
            'tasa_adicional'    => 0,
            'desglosa_tasa'     => 0,
            'cobra_tasa'        => 1
        ]);

        DB::table('categorias_iva')->insert([
            'abreviatura'       => 'MON',
            'categoria'         => 'Monotributo',
            'tipo_factura'      => 'B',
            'tasa_general'      => 0,
            'desglosa_general'  => 0,
            'tasa_adicional'    => 0,
            'desglosa_tasa'     => 0,
            'cobra_tasa'        => 1
        ]);

        DB::table('categorias_iva')->insert([
            'abreviatura'       => 'EXE',
            'categoria'         => 'Exento',
            'tipo_factura'      => 'B',
            'tasa_general'      => 0,
            'desglosa_general'  => 0,
            'tasa_adicional'    => 0,
            'desglosa_tasa'     => 0,
            'cobra_tasa'        => 1
        ]);
    }
}
