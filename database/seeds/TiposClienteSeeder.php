<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\TipoCliente;
/*
  0 => "Id"
  1 => "TipoCliente"
  2 => "FechaMod"
  3 => "UserMod"
*/
class TiposClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Id|TipoCliente|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/TiposCliente.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                $tipo_cliente = new TipoCliente();

                $tipo_cliente->nombre = $datos[1];
                $tipo_cliente->baciunas_id = $datos[0];
                $tipo_cliente->created_user_id = 1;

                $tipo_cliente->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}
