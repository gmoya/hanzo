<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\Chofer;

class ChoferesSeeder extends Seeder
{
/*
  0 => "Id"
  1 => "Chofer"
  2 => "FechaMod"
  3 => "UserMod"
*/
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Id|Chofer|FechaMod|UserMod
        $contenido = file_get_contents(storage_path('app/csv/Choferes.csv'));
        $lineas    = explode("\n", $contenido);

        unset($lineas[0]);

        foreach ($lineas as $datos)
        {
            $datos = explode('|', $datos);

            if ($datos[0] != '')
            {
                $datos = $this->transformarDatos($datos);

                # CHOFER
                $chofer = new Chofer();

                $chofer->baciunas_id     = $datos[0];
                $chofer->nombre          = $datos[1];
                $chofer->created_at      = date('Y-m-d H:i:s');
                $chofer->created_user_id = 1;

                $chofer->save();
            }
        }
    }

    private function transformarDatos($datos)
    {
        foreach ($datos as $key => $value)
        {
            $datos[$key] = mb_convert_encoding($value, "UTF-8", 'CP850');
            $datos[$key] = str_replace('"', '', $datos[$key]);
            $datos[$key] = str_replace('\'', '', $datos[$key]);
            $datos[$key] = trim($datos[$key]);
        }

        return $datos;
    }
}