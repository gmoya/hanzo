<?php

use Illuminate\Database\Seeder;
use Hanzo\Models\TipoComprobante;

class TiposComprobanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = array(
                        array('nombre'         => 'Factura A',
                             'codigo_afip'     => '1',
                             'codigo'          => 'FAC',
                             'letra'           => 'A',
                             'created_user_id' => 1
                        ),
                        array('nombre'         => 'Nota de Débito A',
                             'codigo_afip'     => '2',
                             'codigo'          => 'NDEB',
                             'letra'           => 'A',
                             'created_user_id' => 1
                        ),
                        array('nombre'         => 'Nota de Crédito A',
                             'codigo_afip'     => '3',
                             'codigo'          => 'NCRE',
                             'letra'           => 'A',
                             'created_user_id' => 1
                        ),
                        array('nombre'         => 'Factura B',
                             'codigo_afip'     => '6',
                             'codigo'          => 'FAC',
                             'letra'           => 'B',
                             'created_user_id' => 1
                        ),
                        array('nombre'         => 'Nota de Débito B',
                             'codigo_afip'     => '7',
                             'codigo'          => 'NDEB',
                             'letra'           => 'B',
                             'created_user_id' => 1
                        ),
                        array('nombre'         => 'Nota de Crédito B',
                             'codigo_afip'     => '8',
                             'codigo'          => 'NCRE',
                             'letra'           => 'B',
                             'created_user_id' => 1
                        )
                );
        
        foreach ($tipos as $key => $tipo) 
        {
            TipoComprobante::create($tipo);
        }
    }
}
