<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsFacturaFallidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_factura_fallida', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('factura_id')->nullable();
            $table->integer('alicuota_id')->nullable();

            $table->integer('renglon')->nullable();
            $table->double('contenedores')->nullable();
            $table->double('kilos')->nullable();
            $table->double('precio_unitario')->nullable();
            $table->double('impuestos')->nullable();
            $table->double('neto')->nullable();
            $table->string('nota_factura', 60)->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
