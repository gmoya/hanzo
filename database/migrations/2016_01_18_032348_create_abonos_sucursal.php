<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbonosSucursal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonos_sucursal', function(Blueprint $table)
        {
            $table->increments('id');
            $table->double('monto')->required();
            $table->boolean('activo');
            $table->integer('cliente_id')->required();
            $table->integer('sucursal_id')->required();
            $table->integer('tipo_abono_id')->required();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
