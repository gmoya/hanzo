<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetSucursales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('sucursales');
        Schema::create('sucursales', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('nro_cliente')->nullable();
            $table->string('nombre', 120)->required();
            $table->string('calle', 70)->required();
            $table->string('nro', 10)->required();
            $table->string('codigo_postal', 10)->nullable();
            $table->integer('provincia_id')->required();
            $table->integer('localidad_id')->required();
            $table->string('telefono', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('horario_atencion', 30)->nullable();
            $table->string('contacto', 70)->nullable();
            $table->string('hoja_filcar', 10)->nullable();
            $table->string('nro_ceamse', 30)->nullable();
            $table->string('entre_calle_a', 70)->nullable();
            $table->string('entre_calle_b', 70)->nullable();

            $table->integer('chofer_id')->nullable();
            $table->integer('recorrido_id')->nullable();
            $table->integer('cliente_id')->required();
            $table->integer('domicilio_cobro_id')->nullable();
            $table->integer('domicilio_factura_id')->nullable();
            $table->integer('estado_id')->required();
            $table->boolean('ciclo_facturacion')->nullable();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
