<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondicionesVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condiciones_venta', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('condicion', 30)->required();
            $table->decimal('variacion', 5, 4);
            $table->smallInteger('dias_1');
            $table->decimal('porc_1', 7, 4);
            $table->smallInteger('dias_2');
            $table->decimal('porc_2', 7, 4);            
            $table->smallInteger('dias_3');
            $table->decimal('porc_3', 7, 4);
            $table->smallInteger('dias_4');
            $table->decimal('porc_4', 7, 4);
            $table->smallInteger('dias_5');
            $table->decimal('porc_5', 7, 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('condiciones_venta');
    }
}
