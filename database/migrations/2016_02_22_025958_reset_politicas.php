<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetPoliticas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('politicas_producto');
        Schema::create('politicas_producto', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('frecuencia_id')->nullable();
            $table->integer('sucursal_id')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->integer('cantidad_mensual')->nullable();
            
            $table->boolean('lunes')->default(false);
            $table->boolean('martes')->default(false);
            $table->boolean('miercoles')->default(false);
            $table->boolean('jueves')->default(false);
            $table->boolean('viernes')->default(false);
            $table->boolean('sabado')->default(false);
            $table->boolean('domingo')->default(false);
            
            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
