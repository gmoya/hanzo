<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetSucursalesClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('sucursales');
        Schema::create('sucursales', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('nro_cliente')->nullable();
            $table->string('nombre', 120)->required();
            $table->string('calle', 70)->required();
            $table->string('nro', 10)->required();
            $table->string('codigo_postal', 10)->nullable();
            $table->integer('provincia_id')->required();
            $table->integer('localidad_id')->required();
            $table->string('telefono', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('horario_atencion', 30)->nullable();
            $table->string('contacto', 70)->nullable();
            $table->string('hoja_filcar', 10)->nullable();
            $table->string('nro_ceamse', 30)->nullable();
            $table->string('entre_calle_a', 70)->nullable();
            $table->string('entre_calle_b', 70)->nullable();

            $table->integer('chofer_id')->nullable();
            $table->integer('recorrido_id')->nullable();
            $table->integer('cliente_id')->required();
            $table->integer('vendedor_id')->required();
            $table->integer('cobrador_id')->required();
            $table->integer('condicion_venta_id')->nullable();
            $table->integer('domicilio_cobro_id')->nullable();
            $table->integer('domicilio_factura_id')->nullable();
            $table->integer('estado_id')->required();
            $table->integer('ciclo_facturacion_id')->nullable();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });


        Schema::drop('clientes');

        Schema::create('clientes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('nro_cliente')->nullable();
            $table->string('razon_social', 60);
            $table->string('nombre_comercial', 50)->nullable();
            $table->string('cuit', 15)->nullable();
            $table->string('nro_orden_compra', 20)->nullable();
            $table->string('web', 100)->nullable();
            $table->double('cuota_mensual')->nullable();

            $table->integer('estado_id')->required();
            $table->integer('categoria_iva_id')->nullable();
            $table->integer('rubro_empresario_id')->nullable();
            $table->integer('ingreso_brutos_id')->nullable();
            $table->integer('ingreso_brutos_bsas_id')->nullable();
            $table->integer('tipo_cliente_id')->nullable();
            
            $table->text('observaciones')->nullable();            
            $table->datetime('fecha_expiracion')->nullable();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();

            $table->timestamps();
        });

        Schema::drop('domicilios_cliente');
        Schema::create('domicilios_cliente', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('calle', 70)->required();
            $table->string('nro', 10)->required();
            $table->string('codigo_postal', 10)->nullable();
            $table->integer('provincia_id')->required();
            $table->integer('localidad_id')->required();
            $table->string('telefono', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('horario_atencion', 30)->nullable();
            $table->string('contacto', 70)->nullable();
            $table->string('hoja_filcar', 10)->nullable();
            $table->string('entre_calle_a', 70)->nullable();
            $table->string('entre_calle_b', 70)->nullable();
            $table->integer('tipo_domicilio_cliente_id')->required();
            $table->integer('cliente_id')->required();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
