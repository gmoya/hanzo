<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendientes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('cliente_id')->nullable();
            $table->string('razon_social', 70)->nullable();
            $table->string('cuit', 15)->nullable();
            $table->integer('comprobante_id')->nullable();
            $table->datetime('fecha')->nullable();
            $table->datetime('vencimiento')->nullable();
            $table->string('letra', 1)->nullable();
            $table->bigInteger('numero_comprobante')->nullable();
            $table->integer('punto_venta_id')->nullable();
            $table->integer('tipo_comprobante_id')->nullable();
            $table->double('importe')->nullable();
        
            $table->integer('estado_id')->default(1);

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
