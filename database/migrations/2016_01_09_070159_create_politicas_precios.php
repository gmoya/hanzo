<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticasPrecios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('politicas_precios', function(Blueprint $table)
        {
            $table->increments('id');
            $table->double('precio')->nullable();
            $table->integer('cantidad_hasta')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->integer('baciunas_id')->nullable();
            $table->integer('producto_id')->nullable();
            $table->integer('politicas_producto_id')->nullable();
            $table->integer('tipo_cantidad_id')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('politicas_precios');
    }
}
