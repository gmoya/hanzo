<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumeradoComprobantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numerado_comprobantes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('nro_comprobante_siguiente')->default(1);
            $table->integer('tipo_comprobante_id')->nullable();
            $table->integer('punto_venta_id')->nullable();
            $table->date('fecha_ultima_factura')->nullable();            

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
