<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->integer('sucursal_id')->required();
            $table->integer('producto_id')->required();
            $table->integer('factura_id')->nullable();

            $table->string('direccion', 150)->nullable();
            $table->double('contenedores')->nullable();
            $table->double('kilos')->nullable();
            $table->double('monto')->nullable();
            $table->string('nro_manifiesto', 20)->nullable();
            $table->string('nro_cr', 20)->nullable();
            $table->integer('estado_id')->nullable();

            $table->text('observaciones')->nullable();

            $table->datetime('fecha')->nullable();
            $table->datetime('fecha_factura')->nullable();
            $table->datetime('fecha_ingreso')->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
