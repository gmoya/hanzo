<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntosVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puntos_venta', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('nro')->nullable();
            $table->string('nombre', 50)->nullable();
            $table->string('sistema', 100)->nullable();
            $table->string('domicilio', 200)->nullable();
            $table->date('fecha_alta')->nullable();            

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
