<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('talonario_id')->nullable();
            $table->integer('sucursal_id')->nullable();
            $table->integer('cliente_id')->nullable();

            $table->string('tipo_comprobante', 5)->nullable();
            $table->string('letra', 1)->nullable();
            $table->integer('punto_venta')->nullable();
            $table->integer('numero')->nullable();
            $table->datetime('fecha')->nullable();

            $table->string('razon', 70)->nullable();
            $table->string('direccion', 90)->nullable();
            $table->string('codigo_postal', 10)->nullable();

            $table->integer('localidad_id')->nullable();
            $table->integer('provincia_id')->nullable();
            $table->integer('categoria_iva_id')->nullable();
            $table->string('cuit', 15)->nullable();

            $table->integer('vendedor_id')->nullable();
            $table->integer('condicion_venta_id')->nullable();
            $table->boolean('activa')->required();
            $table->boolean('manual')->required();

            $table->datetime('fecha_anulacion')->nullable();
            $table->double('neto_gravado')->nullable();
            $table->double('neto_no_gravado')->nullable();
            $table->double('impuestos')->nullable();
            $table->double('total')->nullable();

            $table->integer('cuenta_corriente_id')->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}