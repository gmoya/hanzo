<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->string('nombre', 60)->nullable();
            $table->string('apellido', 60)->nullable();
            $table->string('telefono', 50)->nullable();
            $table->string('celular', 50)->nullable();
            $table->string('email', 100)->nullable();

            $table->integer('sexo_id')->nullable();
            $table->integer('tipo_documento_id')->nullable();
            $table->string('nro_documento', 30)->nullable();
            $table->integer('nacionalidad_id')->nullable();
            $table->integer('estado_civil_id')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('lugar_nacimiento', 50)->nullable();
            $table->text('observaciones')->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
