<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetTiposDomicilioCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('tipos_domicilio_cliente');

        Schema::create('tipos_domicilio_cliente', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nombre', 10)->required();
            $table->string('descripcion', 100)->required();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipos_domicilio_cliente');
    }
}

