<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomiciliosCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios_cliente', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('calle', 70)->required();
            $table->string('nro', 10)->required();
            $table->string('codigo_postal', 10)->nullable();
            $table->integer('provincia_id')->required();
            $table->integer('localidad_id')->required();
            $table->string('telefono', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('horario_atencion', 30)->nullable();
            $table->string('contacto', 70)->nullable();
            $table->string('hoja_filcar', 10)->nullable();
            $table->string('nro_ceamse', 30)->nullable();
            $table->string('entre_calle_a', 70)->nullable();
            $table->string('entre_calle_b', 70)->nullable();
            $table->integer('cobrador_id')->nullable();
            $table->integer('chofer_id')->nullable();
            $table->integer('recorrido_id')->nullable();
            $table->integer('tipo_domicilio_cliente_id')->required();
            $table->integer('cliente_id')->required();

            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });
    
        #    [IdRubroEmpresario] [int] NULL ,
        #    [IdClienteFacturacion] [int] NULL ,
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domicilios_cliente');
    }
}
