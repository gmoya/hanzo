<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalonarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talonarios', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->string('talonario', 30)->nullable();
            $table->boolean('fiscal')->nullable();
            $table->boolean('autonumera')->nullable();
            $table->boolean('debito')->nullable();
            $table->integer('modulos')->nullable();
            $table->string('tipo_comprobante', 5)->nullable();
            $table->string('letra', 1)->nullable();

            $table->integer('punto_venta')->nullable();
            $table->integer('primero')->nullable();
            $table->integer('ultimo')->nullable();
            $table->integer('proximo')->nullable();

            $table->datetime('fecha_vencimiento')->nullable();
            $table->string('cai', 15)->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
