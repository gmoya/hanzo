<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomiciliosPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios_persona', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->string('calle', 70)->required();
            $table->string('nro', 10)->nullable();
            $table->string('piso', 5)->nullable();
            $table->string('dpto', 5)->nullable();
            $table->string('entre_calle_a', 70)->nullable();
            $table->string('entre_calle_b', 70)->nullable();
            $table->string('codigo_postal', 10)->nullable();
            $table->integer('provincia_id')->required();
            $table->integer('localidad_id')->required();
            $table->integer('persona_id')->required();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
