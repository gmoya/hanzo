<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriasIvaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias_iva', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('abreviatura', 5)->required();
            $table->string('categoria', 30)->required();;
            $table->string('tipo_factura', 5);
            $table->decimal('tasa_general', 5, 4);
            $table->boolean('desglosa_general');
            $table->decimal('tasa_adicional', 5, 4);
            $table->boolean('desglosa_tasa');
            $table->boolean('cobra_tasa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categorias_iva');
    }
}
