<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('facturas');
        Schema::create('facturas', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('talonario_id')->nullable();
            $table->integer('sucursal_id')->nullable();
            $table->integer('cliente_id')->nullable();

            $table->string('tipo_comprobante_baciunas', 5)->nullable();
            $table->string('letra', 1)->nullable();
            $table->integer('punto_venta_baciunas')->nullable();
            $table->integer('numero')->nullable();
            $table->datetime('fecha')->nullable();

            $table->string('razon', 70)->nullable();
            $table->string('direccion', 90)->nullable();
            $table->string('codigo_postal', 10)->nullable();

            $table->integer('localidad_id')->nullable();
            $table->integer('provincia_id')->nullable();
            $table->integer('categoria_iva_id')->nullable();
            $table->string('cuit', 15)->nullable();

            $table->integer('vendedor_id')->nullable();
            $table->integer('condicion_venta_id')->nullable();
            $table->boolean('activa')->required();
            $table->boolean('manual')->required();

            $table->integer('punto_venta_id')->nullable();
            $table->integer('tipo_comprobante_id')->nullable();
            $table->date('periodo_facturacion_desde')->nullable();
            $table->date('periodo_facturacion_hasta')->nullable();
            $table->text('mtxca_observaciones')->nullable();
            $table->text('mtxca_errores')->nullable();
            $table->bigInteger('cae')->nullable();
            $table->date('vencimiento_cae')->nullable();
            $table->integer('estado_id')->default(1);

            $table->datetime('fecha_anulacion')->nullable();
            $table->double('neto_gravado')->nullable();
            $table->double('neto_no_gravado')->nullable();
            $table->double('impuestos')->nullable();
            $table->double('total')->nullable();

            $table->integer('cuenta_corriente_id')->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
