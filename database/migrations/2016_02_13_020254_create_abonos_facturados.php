<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbonosFacturados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonos_facturados', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('sucursal_id')->nullable();
            $table->integer('abono_sucursal_id')->nullable();
            $table->integer('factura_id')->nullable();
            $table->double('monto')->nullable();
            
            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
