<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('clientes');

        Schema::create('clientes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('nro_cliente')->nullable();
            $table->string('razon_social', 60);
            $table->string('nombre_comercial', 50)->nullable();
            $table->string('cuit', 15)->nullable();
            $table->integer('estado_id')->required();
            $table->integer('vendedor_id')->nullable();
            $table->integer('categoria_iva_id')->nullable();
            $table->integer('condicion_venta_id')->nullable();
            $table->integer('ingreso_brutos_id')->nullable();
            $table->integer('ingreso_brutos_bsas_id')->nullable();
            $table->datetime('fecha_expiracion')->nullable();
            $table->string('nro_orden_compra', 20)->nullable();
            $table->integer('grupo_empresario_id')->nullable();
            $table->string('web', 100)->nullable();
            $table->double('cuota_mensual')->nullable();
            $table->integer('tipo_cliente_id')->nullable();
            $table->integer('created_user_id');
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
