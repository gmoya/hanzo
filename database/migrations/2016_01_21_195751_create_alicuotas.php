<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlicuotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alicuotas', function(Blueprint $table)
        {        
            $table->increments('id');
            $table->integer('baciunas_id')->nullable();
            $table->integer('codigo')->required();
            $table->string('alicuota', 45)->nullable();
            $table->string('abreviatura', 20)->nullable();
            $table->double('porcentaje')->nullable();
            $table->double('imp_minimo')->nullable();
            $table->integer('provincia_id')->nullable();

            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('deleted_user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
