$(function() {
    $( "#sucursal" ).autocomplete({
        change: function( event, ui ) 
        {
            if ( $("#sucursal_id").val() != '' && $("#sucursal_nombre").val() !=  $( "#sucursal" ).val()  )
            {
                $( "#sucursal" ).val( $("#sucursal_nombre").val() );
            } else if ($("#sucursal_id").val() == '' && $( "#sucursal" ).val() != '') {
                $( "#sucursal" ).val('');
            }
        },
        source: function(request, response, url)
        {
            var searchParam  = request.term;

            $.ajax({
                url: '/sucursales/getSucursalesActivasAutocomplete?term=' + searchParam,
                data : '',
                dataType: "json",
                type: "GET",
                success: function (data) {
                    response($.map(data, function(item) {
                        return {
                            sucursal_id: item.id,
                            value: item.nombre,
                            label: item.nombre
                        };
                    }));
                },
            });
        },
        select: function( event, ui ) 
        {
            //getProductosSucursal(ui.item.sucursal_id);

            $( "#sucursal_id" ).val(ui.item.sucursal_id);
            $( "#sucursal_nombre" ).val(ui.item.label);
            
            getDatosClienteFactura(ui.item.sucursal_id);
        },
        minLength: 2
    });

    $('#fecha_desde').datepicker({
        changeMonth: true,
        dateFormat: 'dd/mm/yy',
        defaultDate: "+1w",
        maxDate: "+0D",
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $('#fecha_hasta').datepicker( "option", "minDate", selectedDate );
        }
    });

    $('#fecha_hasta').datepicker({ 
        changeMonth: true,
        dateFormat: 'dd/mm/yy', 
        maxDate: "+0D", 
        numberOfMonths: 2
    });

    $('#fecha_facturacion').datepicker({ 
        dateFormat: 'dd/mm/yy',
        maxDate: "+5D",
        minDate: -5,
        numberOfMonths: 2
    });

    $.datepicker.regional[ 'es_AR' ]

    $('#facturar_form').validate({
        submitHandler: function(form) {
            console-log(form.serialize());
        }
    });

    /** AGREGAR ITEMS **/
    iniciarInputs();

    $( "#add-item" ).button().on( "click", function() {
        addItem();
    });
});

function getProductosSucursal(sucursal_id)
{
    $.ajax({
        url: '/politicas_producto/getProductosBySucursal?sucursal=' + sucursal_id,
        data : '',
        dataType: "json",
        type: "GET",
        success: function (data) {
            $( ".producto_id" ).each(function(){ 
                select = $(this).html('');

                $.each(data, function( key, producto ){
                    select.append('<option value="' + producto.id + '">' + producto.nombre + '</option>')
                });
            });
        },
    });    
}

function getDatosClienteFactura(sucursal_id)
{
    $.ajax({
        url: '/sucursales/getDatosClienteFactura?sucursal=' + sucursal_id,
        data : '',
        dataType: "json",
        type: "GET",
        success: function (data) {
            $(".datos-cliente").slideUp('fast', function(){
                $('.tipo_factura').html(data.tipo_factura);
                $('.razon_social').html(data.razon_social);
                $('.nombre_comercial').html(data.nombre_comercial);
                $('.cuit').html(data.cuit);
                $('.estado').html(data.estado);
                $('.categoria_iva').html(data.categoria_iva);
                $('.orden_compra').html(data.nro_orden_compra);
                $('.condicion_venta').html(data.condicion_venta);
                $('.dom_facturacion').html(data.domicilios_facturacion);
            });

            verificarInputsTipoFactura(data.tipo_factura);

            $(".datos-cliente").slideDown();
        }
    });
}

function verificarInputsTipoFactura(tipo_factura)
{
    if (tipo_factura == 'B')
    {
        $('.precio-unitario-label').removeClass('hidden');
        $('.precio-unitario').addClass('hidden');

        $('.subtotal').removeClass('hidden');
        $('.subtotal-label').addClass('hidden');
    } else {
        $('.precio-unitario').removeClass('hidden');
        $('.precio-unitario-label').addClass('hidden');

        $('.subtotal-label').removeClass('hidden');
        $('.subtotal').addClass('hidden');
    }
}

function iniciarInputs()
{
    $( ".producto" ).autocomplete({
        change: function( event, ui ) 
        {
            producto_id = $( this ).parent().find( '.producto_id' );
            producto_nombre = $( this ).parent().find( '.producto_nombre' );
            
            if ( producto_id.val() != '' && producto_nombre.val() !=  $( this ).val() )
            {
                $( this ).val( producto_nombre.val() );
            } else if (producto_id.val() == '' && $( this ).val() != '') {
                $( this ).val('');
            }
        },
        source: function(request, response, url){
            var searchParam = request.term;

            $.ajax({
                url: '/productos/getProductosAutocomplete?term=' + searchParam,
                data : '',
                dataType: "json",
                type: "GET",
                success: function (data) {
                    response($.map(data, function(item) {
                        return {
                            producto_id: item.id,
                            value: item.nombre,
                            label: item.nombre,
                            porcentaje: item.porcentaje
                        };
                    }));
                },
            });
        },
        select: function( event, ui ) {

            $( this ).parent().parent().find('.iva-percent-label').html(ui.item.porcentaje);
            $( this ).parent().find( '.producto_id' ).val( ui.item.producto_id );
            $( this ).parent().find( '.producto_nombre' ).val( ui.item.label );
          
            ($('.tipo_factura').html() == 'B') ? calcularPrecioUnitarioIva( $(this) ) : calcularSubtotal( $(this) );
        },
        minLength: 2
    });

    $('.cantidad')   
        .on('input', function(){
            ($('.tipo_factura').html() == 'B') ? calcularPrecioUnitarioIva( $(this) ) : calcularSubtotal( $(this) );
        })
        .on('keypress', function(tecla){ if (tecla.charCode < 48 || tecla.charCode > 57) return false; });
    
    $('.tipo_unidad').on('change', function(){ 
        ($('.tipo_factura').html() == 'B') ? calcularPrecioUnitarioIva( $(this) ) : calcularSubtotal( $(this) );
    });
    
    $('.precio-unitario')
        .on('input', function(){ calcularSubtotal( $(this) ); })
        .on('keypress', function(tecla){ if ((tecla.charCode < 48 || tecla.charCode > 57 ) && tecla.charCode != 46 && tecla.charCode != 45) return false; });
    
    $('.subtotal')
        .on('input', function(){ calcularPrecioUnitarioIva( $(this) ); })
        .on('keypress', function(tecla){ if ((tecla.charCode < 48 || tecla.charCode > 57 ) && tecla.charCode != 46 && tecla.charCode != 45) return false; });

    $('.remove-item').on('click', function(){
        $( this ).parent().parent().remove();
        calcularTotales();
    });

    verificarInputsTipoFactura($('.tipo_factura').html());
}

function calcularPrecioUnitarioIva(field)
{
    tr = $( field ).parent().parent();

    cantidad    = Number($.trim( tr.find('.cantidad').val() ));
    tipo_unidad = $.trim( tr.find('.tipo_unidad').val() );
    subtotal    = Number($.trim( tr.find('.subtotal').val() ));
    iva_percent = Number($.trim( tr.find('.iva-percent-label').html() ));

    if (cantidad > 0 && subtotal >= 0)
    {
        if ( tipo_unidad == 2 )
            cantidad = cantidad / 1000;

        iva           = (iva_percent * subtotal) / (100 + iva_percent);
        precio_u      = (subtotal - iva) / cantidad;

        tr.find('.iva-label')            .html(        Number(iva)     .toFixed(2) );
        tr.find('.subtotal-label')       .html( '$ ' + Number(subtotal).toFixed(2) );
        tr.find('.precio-unitario-label').html( '$ ' + Number(precio_u).toFixed(6) );
        
        tr.find('.precio-unitario').val( precio_u );
    }

    calcularTotales();
}

function calcularSubtotal(field)
{
    tr = $( field ).parent().parent();

    cantidad = Number($.trim( tr.find('.cantidad').val() ));
    precio_u = Number($.trim( tr.find('.precio-unitario').val() ));
    tipo_unidad = $.trim( tr.find('.tipo_unidad').val() );
    iva_percent = Number($.trim( tr.find('.iva-percent-label').html() )) / 100;

    if (cantidad && precio_u)
    {
        if ( tipo_unidad == 2 )
            cantidad = cantidad / 1000;


        subtotal_neto = (Number(cantidad) * Number(precio_u));
        iva           = subtotal_neto * iva_percent;
        subtotal      = subtotal_neto + iva;
        

        tr.find('.iva-label')            .html(        Number(iva)     .toFixed(2) );
        tr.find('.subtotal-label')       .html( '$ ' + Number(subtotal).toFixed(2) );
        tr.find('.precio-unitario-label').html( '$ ' + precio_u );
        
        tr.find('.subtotal')      .val( subtotal );
    }

    calcularTotales();
}

function calcularTotales()
{
    total = 0;
    iva  = 0;

    $('.subtotal').each(function(index){
        total+= Number( $(this).val() );
    });

    $('.iva-label').each(function(index){
        iva+= Number( $(this).html() );
    });

    neto  = total - iva;

    neto  = Number(neto).toFixed(2);
    iva   = Number(iva).toFixed(2);
    total = Number(total).toFixed(2);

    $('.neto') .attr('value', neto ).html( neto );
    $('.iva')  .attr('value', iva ).html( iva );
    $('.total').attr('value', total ).html( total );
}

function addItem()
{
    renglon = $('#factura-detalle-items tbody tr').length;
    
    while( $("#renglon-" + renglon).length )
        renglon++;

    td_producto = $('<td>'
                    //+ '<select class="form-control producto_id" name="items['+renglon+'][producto_id]">'+ $('#renglon-0 .producto_id').html() +'</select></td>');
                    + '<input type="hidden" name="items['+renglon+'][producto_id]" required="required" class="form-control producto_id">'
                    + '<input type="hidden" name="items['+renglon+'][producto_nombre]" class="producto_nombre form-control">'
                    + '<input type="text" name="items['+renglon+'][producto]" required="required" class="form-control producto"></td>');

    td_cantidad = $('<td>'
                    +'<input type="hidden" name="items['+renglon+'][item_id]" class="item_id">'
                    +'<input type="text" name="items['+renglon+'][cantidad]" required="required" class="form-control cantidad text-right"></td>');

    td_tipo_cantidad = $('<td>'
                         +'<select name="items['+renglon+'][tipo_unidad]" class="form-control tipo_unidad">'
                         +'<option value="1">Contenedores</option>'
                         +'<option value="2">Kilos</option>'
                         +'</select></td>');
                        
    td_precio_unitario = $('<td class="text-right">'
                            +'<span class="precio-unitario-label hidden">$ 0</span>'
                            +'<input type="text" name="items['+renglon+'][precio_unitario]" required="required" class="form-control precio-unitario text-right"></td>');
    
    td_iva = $('<td class="text-right">[<span class="iva-percent-label">0</span>%]</td>'
               +'<td class="text-right">$ <span class="iva-label">0</span></td>');

    td_subtotal = $('<td class="text-right">'
                    +'<span class="subtotal-label">$ 0</span>'
                    +'<input class="form-control subtotal text-right hidden" name="items[' +renglon+ '][subtotal]" type="text"></td>');

    td_acciones = $('<td><a class="remove-item"><span class="glyphicon glyphicon-remove-circle"></span></a></td>');
                        
    tr = $('<tr id="renglon-' + renglon + '">')
            .append(td_producto)
            .append(td_cantidad)
            .append(td_tipo_cantidad)
            .append(td_precio_unitario)
            .append(td_iva)
            .append(td_subtotal)
            .append(td_acciones);
                
    $('#factura-detalle-items tbody').append(tr);

    iniciarInputs();
}