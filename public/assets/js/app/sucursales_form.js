jQuery(function(){
    $( "#localidad" ).autocomplete({
        source: function(request, response, url){
            var searchParam  = request.term;

            $.ajax({
                url: '/localidad/getLocalidadesAutocomplete?term=' + searchParam,
                data : '',
                dataType: "json",
                type: "GET",
                success: function (data) {
                    response($.map(data, function(item) {
                        return {
                            localidad_id: item.id,
                            value: item.nombre,
                            label: item.nombre
                        };
                    }));
                },
            });
        },
        select: function( event, ui ) {
            $( "#localidad_id" ).val(ui.item.localidad_id);
        },
        minLength: 3
    });
});