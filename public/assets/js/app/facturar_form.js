$(function() {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $('#fecha_desde').datepicker({
        changeMonth: true,
        dateFormat: 'dd/mm/yy', 
        defaultDate: "+1w",
        maxDate: "+0D", 
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $('#fecha_hasta').datepicker( "option", "minDate", selectedDate );
        }
    });

    $('#fecha_hasta').datepicker({ 
        changeMonth: true,
        dateFormat: 'dd/mm/yy', 
        maxDate: "+0D", 
        numberOfMonths: 2
    });

    $('#fecha_facturacion').datepicker({ 
        dateFormat: 'dd/mm/yy',
        maxDate: "+5D",
        //minDate: -5,
        minDate: fecha_min,
        numberOfMonths: 2
    });

    $.datepicker.regional[ 'es_AR' ]

    $('#facturar_form').validate({
        submitHandler: function(form) {
            $('#dialog-message').dialog({ 
                closeOnEscape: false,
                dialogClass: 'no-close',
                modal: true
            }).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Obteniendo Servicios y Abonos a facturar.');
            
            var form = $(form);

            $.post( '/facturas/getPendientesFacturacion', form.serialize(), function( data ) {
                if ((data.totales.servicios + data.totales.abonos) > 0) {
                    leyenda = 'Se van a facturar ' + data.totales.servicios + ' servicios y ' + data.totales.abonos + ' abonos.<br/>¿Desea continuar?';
                    botones = { "Facturar": function() { facturarBySucursal(data, form) }, 'Cancelar': function() { $( this ).dialog( "close" ) }}
                } else {
                    leyenda = 'No se encontraron servicios / abonos pendientes de facturación en la fecha seleccionada.';
                    botones = { 'Cerrar': function() { $( this ).dialog( "close" ) }}
                }

                $('#dialog-message').dialog({ 
                    buttons: botones 
                }).html(leyenda);
            }, 'json');
        }
    });
});

function facturarBySucursal(data, form)
{
    i = 0;
    facturado = 0;
    total = data.totales.abonos + data.totales.servicios;
    abonos_facturar = data.abonos;
    servicios_facturar = data.servicios;

    $('#dialog-message').dialog({ buttons: {} }).html('<div id="progressbar"><div class="progress-label">Cargando...</div></div><div id="progressbar-action"></div>');

    var progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" );
    $( "#progressbar" ).progressbar({ 
        value: 0, 
        change: function() {
            progressLabel.text( progressbar.progressbar( "value" ).toFixed(0) + "%" );
        },
        complete: function() {
            progressLabel.text( "Completado!" );
            $('#progressbar-action').html('<a class="btn" style="color:rgb(0, 112, 210)" href="/facturas">Cerrar</a>');

        }
    });

    var datos = form.serializeFormJSON();
    
    setTimeout(function(){

        $.each(servicios_facturar, function( sucursal_id, servicios ) {
                while ( abonos_facturar.length > 0 && abonos_facturar[i] != undefined && abonos_facturar[i].sucursal_id < sucursal_id ) 
                {
                    datos.servicios = [];
                    datos.sucursal_id = abonos_facturar[i].sucursal_id;

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: '/facturas/facturarBySucursal',
                        data: datos,
                        complete: function(){
                            i++;
                            facturado++;
                        
                            $( "#progressbar" ).progressbar({ value: ((facturado * 100) / total) });
                        }
                    });

                }

                datos.servicios = servicios;
                datos.sucursal_id = sucursal_id;
            
                $.ajax({
                    async: false,
                    type: "POST",
                    url: '/facturas/facturarBySucursal',
                    data: datos,
                    complete: function(){
                        facturado += servicios.length;
                        $( "#progressbar" ).progressbar({ value: ((facturado * 100) / total) });
                    }
                });
        });

        while ( i < abonos_facturar.length ) 
        {
                datos.servicios = [];
                datos.sucursal_id = abonos_facturar[i].sucursal_id;

                $.ajax({
                    async: false,
                    type: "POST",
                    url: '/facturas/facturarBySucursal',
                    data: datos,
                    complete: function(){
                        i++;
                        facturado++;
                        $( "#progressbar" ).progressbar({ value: ((facturado * 100) / total) });                      
                    }
                });
        }

    }, 1000);
}