jQuery(function(){
    $('#sucursal').focus();

    $( "#sucursal" ).autocomplete({
        change: function( event, ui ) 
        {
            if ( $("#sucursal_id").val() != '' && $("#sucursal_nombre").val() !=  $( "#sucursal" ).val()  )
            {
                $( "#sucursal" ).val( $("#sucursal_nombre").val() );
            }
        },
        source: function(request, response, url){
            var searchParam  = request.term;

            $.ajax({
                url: '/sucursales/getSucursalesActivasByNroClienteAutocomplete?term=' + searchParam,
                data : '',
                dataType: "json",
                type: "GET",
                success: function (data) {
                    response($.map(data, function(item) {
                        return {
                            sucursal_id: item.id,
                            value: item.nombre,
                            label: item.nombre
                        };
                    }));
                },
            });
        },
        select: function( event, ui ) {
            $( "#sucursal_id" ).val(ui.item.sucursal_id);
            $( "#sucursal_nombre" ).val(ui.item.label);
            $('#producto').val('');
            $('#producto_id').val('');
        },
        minLength: 1
    });

    $('#fecha').datepicker({
        changeMonth: true,
        dateFormat: 'dd/mm/yy', 
        defaultDate: "0",
        maxDate: "0",
        numberOfMonths: 2
    });

    $( "#producto" ).autocomplete({
        source: function(request, response, url){
            var searchParam  = request.term;
            if (sucursal = $( "#sucursal_id" ).val())
            {
                $.ajax({
                    url: '/politicas_producto/getProductosBySucursalAutocomplete?term=' + searchParam + '&sucursal=' + sucursal,
                    data : '',
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response($.map(data, function(item) {
                            return {
                                producto_id: item.id,
                                value: item.nombre,
                                label: item.nombre
                            };
                        }));
                    },
                });
            } else {
                alert('Seleccione una sucursal');
                $('#producto').val('');
                $('#sucursal').focus();
            }
        },
        select: function( event, ui ) {
            $( "#producto_id" ).val(ui.item.producto_id);
        },
        minLength: 1
    });

    $('#save_add_button').click(function(){
        $('#save_add').val(true);

        $('form').submit();

        return false;
    });
});